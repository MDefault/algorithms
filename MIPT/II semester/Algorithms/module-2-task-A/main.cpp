#include <algorithm>
#include <iostream>
#include <map>
#include <set>
#include <vector>

void Deykstra(int vertex, std::vector<long long>& distances, std::set< std::pair<long long, long long> >& q, const std::vector< std::vector< std::pair<long long, long long> > >& Graph) 
{
    q.erase(*q.begin());

    for (size_t i = 0; i < Graph[vertex].size(); i++) {
        long long vertex2 = Graph[vertex][i].first;
        long long weight = Graph[vertex][i].second;

        if (distances[vertex2] > distances[vertex] + weight) {
            q.erase(std::make_pair(distances[vertex2], vertex2));
            distances[vertex2] = distances[vertex] + weight;
            q.insert(std::make_pair(distances[vertex2], vertex2));
        }
    }

    if (!q.empty()) {
        Deykstra((*q.begin()).second, distances, q, Graph);
    }
}

int main() {
    const long long Inf = 1e18;
    int FirstCost;
    int SecondCost;
    int NumOfUniverse;
    int StartVertex;
    int EndVertex;
    std::vector< std::vector< std::pair<long long, long long> > > Graph;
    std::vector<long long> distances;
    std::set< std::pair<long long, long long> > q;

    std::cin >> FirstCost >> SecondCost >> NumOfUniverse >> StartVertex >> EndVertex;

    if (NumOfUniverse == 1) {
        std::cout << 0 << std::endl;
        return 0;
    }

    Graph.resize(NumOfUniverse);
    distances.resize(NumOfUniverse, Inf);

    for (long long i = 0; i < NumOfUniverse; i++) {
        long long vertex1 = i;
        long long vertex2 = (i + 1) % NumOfUniverse;
        long long vertex3 = (i * i + 1) % NumOfUniverse;

        if (vertex2 == vertex3) {
            Graph[vertex1].push_back(std::make_pair(vertex2, std::min(FirstCost, SecondCost)));
        } else {
            Graph[vertex1].push_back(std::make_pair(vertex2, FirstCost));
            Graph[vertex1].push_back(std::make_pair(vertex3, SecondCost));
        }
    }

    distances[StartVertex] = 0;
    q.insert(std::make_pair(distances[StartVertex], StartVertex));

    Deykstra(StartVertex, distances, q, Graph);

    std::cout << distances[EndVertex] << std::endl;

    return 0;
}

