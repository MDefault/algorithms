#define CATCH_CONFIG_MAIN

#include <iostream>
#include <sstream>
#include <string>
#include <catch2/catch.hpp>
#include "../include/solution.h"

TEST_CASE("ANSWER_HOLDER", "TEST1") {
    std::ostringstream ostr;
    std::istringstream istr("3 2\n0 1\n2 1\n1 1 0");
    SolveTask(ostr, istr);
    
    REQUIRE(ostr.str() == "2");
}

TEST_CASE("ANSWER_HOLDER2", "TEST2") {
	std::ostringstream ostr;
    std::istringstream istr("5 4\n0 1 2 3\n2 3\n4 5 6");
    SolveTask(ostr, istr);
    
    REQUIRE(ostr.str() == "8");
}

TEST_CASE("ANSWER_HOLDER3", "TEST3") {
	std::ostringstream ostr;
    std::istringstream istr("6 8\n0 1 1 3 4\n3 5\n9 10 93");
    SolveTask(ostr, istr);
    
    REQUIRE(ostr.str() == "10");
}
