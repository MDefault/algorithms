#include <iostream>
#include <vector>

void ProcessLCA(std::vector<long long>& intime, std::vector<long long>& outtime, const std::vector<std::vector<long long>>& graph,
                std::vector<std::vector<long long>>& lcaAns, long long maxH, long long& timer, long long vertex, long long parent = 0)
{
    intime[vertex] = timer++;

    lcaAns[vertex][0] = parent;
    for(long long i = 1; i <= maxH; i++) {
        lcaAns[vertex][i] = lcaAns[lcaAns[vertex][i - 1]][i - 1];
    }

    for(long long i = 0; i < graph[vertex].size(); i++) {
        long long vertex2 = graph[vertex][i];

        if(vertex2 != parent) {
            ProcessLCA(intime, outtime, graph, lcaAns, maxH, timer, vertex2, vertex);
        }
    }

    outtime[vertex] = timer++;
}

bool Upper(const std::vector<long long>& intime, const std::vector<long long>& outtime, long long vertex1, long long vertex2) {
    return intime[vertex1] <= intime[vertex2] && outtime[vertex1] >= outtime[vertex2];
}

long long LCA(const std::vector<long long>& intime, const std::vector<long long>& outtime,
        const std::vector<std::vector<long long>>& lcaAns, long long maxH, long long v1, long long v2)
{
    if(Upper(intime, outtime, v1, v2)) {
        return v1;
    } else if(Upper(intime, outtime, v2, v1)) {
        return v2;
    }

    for(long long i = maxH; i >= 0; i--) {
        if(!Upper(intime, outtime, lcaAns[v1][i], v2)) {
            v1 = lcaAns[v1][i];
        }
    }

    return lcaAns[v1][0];
}

void InitData(std::vector<long long>& intime, std::vector<long long>& outtime, std::vector<std::vector<long long>>& lcaAns,
              long long& maxH, long long& numOfVertices, long long& numOfQueries,
              long long& a1, long long& a2, long long& x, long long& y, long long& z, std::istream& istr)
{
    std::vector<std::vector<long long>> graph;

    long long timer = 0;
    maxH = 0;

    istr >> numOfVertices >> numOfQueries;

    graph.resize(numOfVertices);
    intime.resize(numOfVertices);
    outtime.resize(numOfVertices);
    lcaAns.resize(numOfVertices);

    while((1 << maxH) <= numOfVertices) {
        ++maxH;
    }

    for(long long i = 0; i < numOfVertices; i++) {
        lcaAns[i].resize(maxH + 1);
    }

    for(long long i = 0; i < numOfVertices - 1; i++) {
        long long v;
        istr >> v;
        graph[v].push_back(i + 1);
        graph[i + 1].push_back(v);
    }

    istr >> a1 >> a2;
    istr >> x >> y >> z;

    ProcessLCA(intime, outtime, graph, lcaAns, maxH, timer, 0, 0);
}

void SolveTask(std::ostream& ostr, std::istream& istr) {
    std::vector<long long> intime;
    std::vector<long long> outtime;
    std::vector<std::vector<long long>> lcaAns;
    long long maxH = 0;
    long long numOfVertices;
    long long numOfQueries;
    long long a1;
    long long a2;
    long long x;
    long long y;
    long long z;

    InitData(intime, outtime, lcaAns, maxH, numOfVertices, numOfQueries, a1, a2, x, y, z, istr);

    long long lastQuery = LCA(intime, outtime, lcaAns, maxH, a1, a2);

    long long result = lastQuery;

    for(long long i = 2; i <= numOfQueries; i++) {
        long long temp = (x * a1 + y * a2 + z) % numOfVertices;
        a2 = (a2 * x + y * temp + z) % numOfVertices;
        a1 = temp;
        lastQuery = LCA(intime, outtime, lcaAns, maxH, (a1 + lastQuery) % numOfVertices, a2);
        result += lastQuery;
    }

    ostr << result;
}
