#define CATCH_CONFIG_MAIN

#include <iostream>
#include <sstream>
#include <string>
#include <catch2/catch.hpp>
#include "../include/solution.h"

TEST_CASE("ANSWER_HOLDER", "TEST1") {
    std::ostringstream ostr;
    std::istringstream istr("5\n00101\n00111\n11000\n01001\n11010");
    SolveTask(ostr, istr);
    
    REQUIRE(ostr.str() == "4 \n1 2 3 5 ");
}

TEST_CASE("ANSWER_HOLDER2", "TEST2") {
	std::ostringstream ostr;
    std::istringstream istr("3\n011\n101\n110");
    SolveTask(ostr, istr);
    
    REQUIRE(ostr.str() == "3 \n1 2 ");
}

TEST_CASE("ANSWER_HOLDER3", "TEST3") {
	std::ostringstream ostr;
    std::istringstream istr("4\n0111\n1001\n1001\n1110");
    SolveTask(ostr, istr);
    
    REQUIRE(ostr.str() == "3 \n1 2 4 ");
}
