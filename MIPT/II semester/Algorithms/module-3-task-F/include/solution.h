#pragma once

#include <iostream>
#include <bits/stdc++.h>

void MinCut(std::vector<int>& bestCut, std::vector< std::vector<int> >& graph, int n, int maxN, int& bestCost) {
    std::vector<int> vertices[maxN];
    for(int i = 0; i < n; i++) {
        vertices[i].assign (1, i);
    }
        
    std::vector<bool> exist(maxN, true);
    std::vector<bool> AVertices(maxN);
    std::vector<int> vertices2(maxN);
        
    for(int i = 0; i + 1 < n; i++) {
        AVertices.assign(maxN, false);
       	vertices2.assign(maxN, 0);

        for(int it = 0, prev; it + i < n; it++) {
            int selected = -1;
                
            for(int j = 0; j < n; j++) {
                if(exist[j] && !AVertices[j] && (selected == -1 || vertices2[j] > vertices2[selected])) {
                    selected = j;
                }
            }
                
            if (it == n - i - 1) {
                if (vertices2[selected] < bestCost) {
                    bestCost = vertices2[selected];
                    bestCut = vertices[selected];
                }
                    
                vertices[prev].insert(vertices[prev].end(), vertices[selected].begin(), vertices[selected].end());
                    
                for(int j = 0; j < n; j++) {
                    graph[prev][j] = graph[j][prev] += graph[selected][j];
                }
                    
                exist[selected] = false;
            } else {
                AVertices[selected] = true;
                    
                for(int j = 0; j < n; j++) {
                    vertices2[j] += graph[selected][j];
                }
                   
                prev = selected;
            }
        }
    }
}

void SolveTask(std::ostream& ostr, std::istream& istr) {
	int bestCost = 1e9;
	int maxN = 50;
	
	int NumOfVertices;
	std::vector< std::vector<int> > Graph(maxN, std::vector<int>(maxN));
	std::vector<int> BestCut;
	
    istr >> NumOfVertices;

    std::string str;
    for(int i = 0; i < NumOfVertices; i++) {
        istr >> str;
        
        for(int j = 0; j < str.size(); j++) {
            Graph[i][j] = str[j] - '0';
        }
    }

    MinCut(BestCut, Graph, NumOfVertices, maxN, bestCost);

    std::vector<int> Second;
    for(int j = 0; j < NumOfVertices; j++) {
        bool ex = false;
        
        for(int i = 0; i < BestCut.size(); i++) {
            if(BestCut[i] == j) {
                ex = true;
                break;
            }
        }

        if(!ex) {
            Second.push_back(j);
        }
    }

    for(int i = 0; i < BestCut.size(); i++) {
        ostr << BestCut[i] + 1 << ' ';
    }
    ostr << '\n';

    for(int i = 0; i < Second.size(); i++) {
        ostr << Second[i] + 1 << ' ';
    }
}
