#include <algorithm>
#include <iostream>
#include <vector>

// первый шаг topsort, нумерация вершин
void TopSortNumerateVertices( int vertex, const std::vector< std::vector<int> >& graph, std::vector<int>& order, std::vector<bool>& used ) {
    used[vertex] = true;

    for( int i = 0; i < graph[vertex].size(); i++ ) {
        int vertex2 = graph[vertex][i];
        if( !used[vertex2] ) {
            TopSortNumerateVertices(vertex2, graph, order, used);
	}
    }

    order.push_back(vertex);
}

// завершающий шаг topsort
void TopSortReachVertices( int vertex, const std::vector< std::vector<int> >& reverseGraph, std::vector<bool>& used, std::vector<int>& component ) {
    used[vertex] = true;
    component.push_back(vertex);

    for( int i = 0; i < reverseGraph[vertex].size(); i++ ) {
        int vertex2 = reverseGraph[vertex][i];
        if( !used[vertex2] ) {
            TopSortReachVertices(vertex2, reverseGraph, used, component);
	}
    }
}

// поиск компонент сильной связности
void FindStrongConnections( const std::vector< std::vector<int> >& graph, const std::vector< std::vector<int> >& reverseGraph, std::vector< std::vector<int> >& components ) {
	int numOfVertices = graph.size();
	std::vector<int> order;
	std::vector<bool> used(numOfVertices, false);

	for( int i = 0; i < numOfVertices; i++) {
		if( !used[i] ) {
			TopSortNumerateVertices(i, graph, order, used);
		}
	}

	used.assign(numOfVertices, false);

	for( int i = 0; i < numOfVertices; i++ ) {
        	int vertex = order[numOfVertices - i - 1];

        	if( !used[vertex] ) {
            		components.push_back(std::vector<int>());
            		TopSortReachVertices(vertex, reverseGraph, used, components.back());
        	}
	}

}

// построение конденсации графа
void BuildGraphCond( const std::vector< std::vector<int> >& graph, const std::vector< std::vector<int> >& reverseGraph, std::vector< std::vector<int> >& condensation ) {
    int numOfVertices = graph.size();
    std::vector< std::vector<int> > component;
    std::vector<int> table(numOfVertices, -1);

    FindStrongConnections(graph, reverseGraph, component);

    for( int i = 0; i < component.size(); i++) {
	for( int j = 0; j < component[i].size(); j++) {
	    int vertex = component[i][j];
	    table[vertex] = i;
	}
    }

    condensation.resize(component.size());

    for( int i = 0; i < numOfVertices; i++ ) {
        int vertex = i;

        for( int j = 0; j < graph[vertex].size(); j++ ) {
            int vertex2 = graph[vertex][j];
            int c1 = table[vertex], c2 = table[vertex2];
            if(c1 == c2) {
                continue;
	    }
            if( std::find(condensation[c1].begin(), condensation[c1].end(), c2) == condensation[c1].end() ) {
                condensation[c1].push_back(c2);
            }
        }
    }
}

// находим количество стоков, истоков и изолированных вершин
void ProcessVertex( std::vector< std::vector<int> >& cond, int& source, int& sink, int& x ) {
    std::vector< std::vector<int> > revCond(cond.size());

    for( int i = 0; i < cond.size(); i++ ) {
        for( int j = 0; j < cond[i].size(); j++ ) {
            int vertex = cond[i][j];
            revCond[vertex].push_back(i);
        }
    }

    source = 0;
    sink = 0;
    x = 0;
    
    for( int i = 0; i < cond.size(); i++ ) {
        if( !cond[i].empty() && revCond[i].empty() ) {
            ++source;
        } else if( cond[i].empty() && !revCond[i].empty() ) {
            ++sink;
        } else if( cond[i].empty() && revCond[i].empty() ) {
            ++x;
        }
    }
}

int main() {
    int numOfVertices;
    int numOfEdges;
    std::cin >> numOfVertices >> numOfEdges;
    std::vector< std::vector<int> > Graph(numOfVertices), revGraph(numOfVertices);

    for( int i = 0; i < numOfEdges; i++ ) {
        int vertex1;
	int vertex2;
        std::cin >> vertex1 >> vertex2;
        if( vertex1 == vertex2 )
            continue;
        --vertex1;
	--vertex2;
        Graph[vertex1].push_back(vertex2);
        revGraph[vertex2].push_back(vertex1);
    }

    std::vector< std::vector<int> > condensation;
    BuildGraphCond(Graph, revGraph, condensation);

    int source, sink, x;
    ProcessVertex(condensation, source, sink, x);
    
    std::cout << (( x == 1 && source == 0 ) ? 0 : std::max(source, sink) + x) << std::endl;

    return 0;
}
