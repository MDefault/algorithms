#ifndef CGRAPH_H
#define CGRAPH_H

#include <algorithm>
#include <map>
#include <queue>
#include <set>
#include <vector>

#include "CGraphFuncs.h"

class CEdge 
{
public:
    CEdge( int _v = -1, int _w = -1);

    int First() const;
    int Second() const;

    // возвращает другую вершину
    int Get( int ) const;

private:
    int v;
    int w;
};

class CGraph
{
public:
    CGraph();

    bool Empty() const;

    void Clear();

    int NumOfVertices() const;
    int NumOfEdges() const;

    void InsertEdge( int, int );
    void InsertEdge( CEdge );
    void InsertVertex( int );

    void RemoveEdge( int, int );
    void RemoveEdge( CEdge );
    void RemoveVertex( int );

    bool Contains( int ) const;
    bool Contains( int, int ) const;
    bool Contains( CEdge ) const;

    std::set<int>::iterator BeginVertices();
    std::set<int>::const_iterator BeginVertices() const;
    std::set<int>::iterator EndVertices();
    std::set<int>::const_iterator EndVertices() const;

    std::set< CEdge >::iterator BeginEdges();
    std::set< CEdge >::const_iterator BeginEdges() const;
    std::set< CEdge >::iterator EndEdges();
    std::set< CEdge >::const_iterator EndEdges() const;

    std::set<int>::iterator BeginVertices( int );
    std::set<int>::iterator EndVertices( int );
    std::set<int>& operator[]( int );

    friend CGraph operator-( const CGraph&, const CGraph& );
    friend CGraph operator+( const CGraph&, const CGraph& );

protected:
    std::set<int> Vertices;
    std::set< CEdge > Edges;
    std::map< int, std::set<int> > table;
};

class CGraphPlanar : public CGraph
{
public:
    CGraphPlanar();

    bool IsPlanar();

private:
    static bool CheckPlanarity( CGraph& );
    static void ConvertToVector( CGraph&, std::vector<int>&, int, int );
    static void CreateNewBorders( CGraph&, std::vector<int>&, std::vector<int>&, std::vector<int>&, bool );
    static void FindContactVertices( CGraph&, CGraph&, std::set<int>& );
    static void FindSegments( CGraph&, CGraph&, std::vector<CGraph>& );
    static void FindPathBetweenContactVertices( CGraph&, CGraph&, CGraph& );
    static bool MainAlgProcess( CGraph&, std::vector<CGraph>&, std::vector< std::vector<int> >&, std::vector<int>& );
    static bool CheckBorderContaining( std::vector<int>&, CGraph&, CGraph& );
    static void MakeBasicBorders( CGraph&, std::vector< std::vector<int> >& );
};

bool operator<( const CEdge&, const CEdge& );

CGraph operator-( const CGraph&, const CGraph& );
CGraph operator+( const CGraph&, const CGraph& );

bool operator==( const CGraph&, const CGraph& );
bool operator!=( const CGraph&, const CGraph& );

#endif // CGRAPH_H
