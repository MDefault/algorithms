#include "cgraph.h"

CEdge::CEdge( int _v, int _w ) 
	: v(std::min(_v, _w)), w(std::max(_v, _w)) 
{
}

int CEdge::First() const 
{
    return v;
}

int CEdge::Second() const 
{
    return w;
}

int CEdge::Get( int a ) const 
{
    if( v == a ) {
        return w;
    } else if( w == a ){
        return v;
    }
    return -1;
}

bool operator<( const CEdge& e1, const CEdge& e2 ) 
{
    return e1.First() < e2.First() || (e1.First() == e2.First() && e1.Second() < e2.Second());
}

CGraph::CGraph()
{
}

bool CGraph::Empty() const 
{
    return Vertices.empty();
}

void CGraph::Clear() 
{
    table.clear();
    Edges.clear();
    Vertices.clear();
}

int CGraph::NumOfVertices() const 
{
    return Vertices.size();
}

int CGraph::NumOfEdges() const 
{
    return Edges.size();
}

void CGraph::InsertEdge( int v, int w ) 
{
    Vertices.insert(v);
    Vertices.insert(w);
    Edges.insert( CEdge( v, w ) );
    table[v].insert(w);
    table[w].insert(v);
}

void CGraph::InsertEdge( CEdge e ) 
{
    Vertices.insert(e.First());
    Vertices.insert(e.Second());
    Edges.insert( e );
    table[e.First()].insert(e.Second());
    table[e.Second()].insert(e.First());
}

void CGraph::InsertVertex( int v ) 
{
    Vertices.insert(v);
}

void CGraph::RemoveEdge( int v, int w ) 
{
    Edges.erase( CEdge(v, w) );
    table[v].erase(w);
    table[w].erase(v);
}

void CGraph::RemoveEdge( CEdge e ) 
{
    Edges.erase( e );
    table[e.First()].erase(e.Second());
    table[e.Second()].erase(e.First());
}

void CGraph::RemoveVertex( int v ) 
{
    Vertices.erase(v);

    std::set< CEdge >::iterator it = Edges.begin(), j;

    while(it != Edges.end()) {
        if(it->First() == v || it->Second() == v) {
            j = it;
            ++j;
            Edges.erase(it);
            it = j;
        } else {
            ++it;
        }
    }

    table.erase(v);

    for(std::map< int, std::set<int> >::iterator i = table.begin(); i != table.end(); i++) {
        i->second.erase(v);
    }
}

bool CGraph::Contains( int v ) const 
{
    return Vertices.find(v) != Vertices.end();
}

bool CGraph::Contains( int v, int w ) const 
{
    return Edges.find( CEdge(v, w) ) != Edges.end();
}

bool CGraph::Contains( CEdge v ) const 
{
    return Edges.find(v) != Edges.end();
}

std::set<int>::iterator CGraph::BeginVertices() 
{
    return Vertices.begin();
}

std::set<int>::const_iterator CGraph::BeginVertices() const 
{
    return Vertices.begin();
}

std::set<int>::iterator CGraph::EndVertices() 
{
    return Vertices.end();
}

std::set<int>::const_iterator CGraph::EndVertices() const 
{
    return Vertices.end();
}

std::set< CEdge >::iterator CGraph::BeginEdges() 
{
    return Edges.begin();
}

std::set< CEdge >::const_iterator CGraph::BeginEdges() const 
{
    return Edges.begin();
}

std::set< CEdge >::iterator CGraph::EndEdges() 
{
    return Edges.end();
}

std::set< CEdge >::const_iterator CGraph::EndEdges() const 
{
    return Edges.end();
}

std::set<int>::iterator CGraph::BeginVertices( int v ) 
{
    return table[v].begin();
}

std::set<int>::iterator CGraph::EndVertices( int v ) 
{
    return table[v].end();
}

std::set<int>& CGraph::operator[]( int v ) 
{
    return table.operator[](v);
}

CGraphPlanar::CGraphPlanar() 
{

}

bool CGraphPlanar::IsPlanar() 
{
    std::set<int> ConnectionPoints;
    std::set<CEdge> Bridges;
    CGraph G = *this;

    FindBridges(G, Bridges);
    for( auto i = Bridges.begin(); i != Bridges.end(); i++ ) {
        G.RemoveEdge( *i );
    }

    FindConnectionPoints(G, ConnectionPoints);
    for( auto i = ConnectionPoints.begin(); i != ConnectionPoints.end(); i++ ) {
        G.RemoveVertex( *i );
    }

    std::vector<CGraph> SplittedGraph;
    SplitGraph(G, SplittedGraph);

    for( int i = 0; i < SplittedGraph.size(); i++ ) {
        if( !CheckPlanarity(SplittedGraph[i]) ) {
            return false;
        }
    }

    return true;
}

bool CGraphPlanar::CheckPlanarity( CGraph &G ) 
{
    std::vector<CGraph> Segments;
    std::vector< std::vector<int> > Borders;
    std::vector<int> OutBorder;
    CGraph GPlain;

    if( FindCycle(G, GPlain) ) {
        return true;
    }

    MakeBasicBorders(GPlain, Borders);
    OutBorder = Borders[0];
    std::reverse(OutBorder.begin(), OutBorder.end());

    while( GPlain != G ) {
        Segments.clear();
        FindSegments(G, GPlain, Segments);
        if( !MainAlgProcess(GPlain, Segments, Borders, OutBorder) ) {
            return false;
        }
    }

    return true;
}

void CGraphPlanar::FindContactVertices( CGraph& segment, CGraph& GPlain, std::set<int>& result ) 
{
    for( auto i = segment.BeginVertices(); i != segment.EndVertices(); i++ ) {
        if( GPlain.Contains( *i ) ) {
            result.insert( *i );
        }
    }
}

void CGraphPlanar::FindSegments( CGraph& G, CGraph& gPlain, std::vector<CGraph>& result ) 
{
    for( auto i = G.BeginEdges(); i != G.EndEdges(); i++ ) {
        CGraph type1;

        if( gPlain.Contains( i->First() ) && gPlain.Contains( i->Second() ) && !gPlain.Contains( *i ) ) {
            type1.InsertEdge( i->First(), i->Second() );
        }

        if( !type1.Empty() ) {
            result.push_back( type1 );
        }
    }

    std::vector<CGraph> SplittedGraph;
    CGraph GWithoutGPlane = G - gPlain;

    SplitGraph(GWithoutGPlane, SplittedGraph);

    for( int i = 0; i < SplittedGraph.size(); i++ ) {
        CGraph type2 = SplittedGraph[i];

        for( auto j = G.BeginEdges(); j != G.EndEdges(); j++ ) {
            if( (SplittedGraph[i].Contains(j->First()) && gPlain.Contains(j->Second()))
                    || (SplittedGraph[i].Contains(j->Second()) && gPlain.Contains(j->First())) )
            {
                type2.InsertEdge( *j );
            }
        }

        if( !type2.Empty() ) {
            result.push_back( type2 );
        }
    }
}

void CGraphPlanar::FindPathBetweenContactVertices( CGraph& segment, CGraph& GPlain, CGraph& result ) 
{
    std::set<int> ContactPoints;

    FindContactVertices(segment, GPlain, ContactPoints);

    std::set<int> used;
    std::map<int, int> parents;
    std::queue<int> q;

    int vertex1 = *ContactPoints.begin();
    int vertex2 = *ContactPoints.rbegin();

    q.push(vertex1);
    parents[vertex1] = -1;

    while( !q.empty() ) {
        int v = q.front();
        q.pop();

        used.insert(v);

        if( v == vertex2 ) {
            break;
        } else if( std::find(ContactPoints.begin(), ContactPoints.end(), v) != ContactPoints.end() && v != vertex1 ) {
            continue;
        }

        for( auto i = segment.BeginVertices(v); i != segment.EndVertices(v); i++ ) {
            if( used.find(*i) == used.end() ) {
                parents[*i] = v;
                q.push(*i);
            }
        }
    }

    result.Clear();

    for( int i = vertex2; parents[i] != -1; i = parents[i] ) {
        result.InsertEdge(i, parents[i]);
    }
}

void CGraphPlanar::ConvertToVector( CGraph& Path, std::vector<int>& result, int from, int to ) 
{
    std::set<int> used;

    used.insert(from);
    result.clear();

    int CurrentVertex = from;

    while( result.size() != Path.NumOfVertices() ) {
        result.push_back(CurrentVertex);

        auto it = Path.BeginVertices(CurrentVertex);
        if( used.find(*it) != used.end() ) {
            ++it;
        }

        used.insert( *it );
        CurrentVertex = *it;
    }
}

void CGraphPlanar::CreateNewBorders( CGraph& Path, std::vector<int>& OldBorder, std::vector<int>& NewBorder1, std::vector<int>& NewBorderOutBorder2, bool IsOutBorder ) 
{
    // находим контактные вершины (степень 1)
    int vertex1 = -1;
    int vertex2 = -1;

    for( auto i = Path.BeginVertices(); i != Path.EndVertices(); i++ ) {
        if( Path[*i].size() == 1 ) {
            if( vertex1 == -1 ) {
                vertex1 = *i;
            } else {
                vertex2 = *i;
                break;
            }
        }
    }

    std::vector<int>::iterator FirstIter = std::find( OldBorder.begin(), OldBorder.end(), vertex1 );
    std::vector<int>::iterator SecondIter = std::find( OldBorder.begin(), OldBorder.end(), vertex2 );

    if(FirstIter > SecondIter) {
        std::swap(FirstIter, SecondIter);
        std::swap(vertex1, vertex2);
    }

    std::vector<int> VectorPath;
    ConvertToVector(Path, VectorPath, vertex1, vertex2);

    NewBorder1.clear();

    if(IsOutBorder) {
        std::copy( VectorPath.begin(), VectorPath.end(), std::back_inserter(NewBorder1) );
        std::copy( SecondIter + 1, OldBorder.end(), std::back_inserter(NewBorder1) );
        std::copy( OldBorder.begin(), FirstIter, std::back_inserter(NewBorder1) );
    } else {
        std::copy( OldBorder.begin(), FirstIter, std::back_inserter(NewBorder1) );
        std::copy( VectorPath.begin(), VectorPath.end(), std::back_inserter(NewBorder1) );
        std::copy( SecondIter + 1, OldBorder.end(), std::back_inserter(NewBorder1) );
    }

    NewBorderOutBorder2.clear();
    std::copy( FirstIter, SecondIter, std::back_inserter(NewBorderOutBorder2) );
    std::copy( VectorPath.rbegin(), VectorPath.rend(), std::back_inserter(NewBorderOutBorder2) );
    NewBorderOutBorder2.pop_back();
}

bool CGraphPlanar::MainAlgProcess( CGraph& GPlain, std::vector<CGraph>& Segments, std::vector<std::vector<int> >& Borders, std::vector<int>& OutBorder ) 
{
    // находим для каждого сегмента индексы подходящих граней
    std::vector< std::vector<int> > SegmentCorrectIndexes( Segments.size() );

    for( int i = 0; i < Segments.size(); i++ ) {
        for( int j = 0; j < Borders.size(); j++ ) {
            if( CheckBorderContaining(Borders[j], Segments[i], GPlain) ) {
                SegmentCorrectIndexes[i].push_back(j);
            }
        }

        if( CheckBorderContaining(OutBorder, Segments[i], GPlain) ) {
            SegmentCorrectIndexes[i].push_back(-1);
        }
    }

    // находим сегмент, который вмещают минимальное количество граней
    int SegmentMinIndex = 0;

    for( int i = 1; i < SegmentCorrectIndexes.size(); i++ ) {
        if( SegmentCorrectIndexes[i].size() < SegmentCorrectIndexes[SegmentMinIndex].size() ) {
            SegmentMinIndex = i;
        }
    }

    if( SegmentCorrectIndexes[SegmentMinIndex].empty() ) {
        return false;
    }

    std::vector<int> NewBorder1;
    std::vector<int> NewBorder2;
    CGraph Path;

    FindPathBetweenContactVertices(Segments[SegmentMinIndex], GPlain, Path);

    // проверка укладки во внешнюю грань
    if( SegmentCorrectIndexes[SegmentMinIndex].size() == 1 && SegmentCorrectIndexes[SegmentMinIndex][0] == -1 ) {
        CreateNewBorders(Path, OutBorder, NewBorder1, NewBorder2, true);
        Borders.push_back(NewBorder1);
        OutBorder = NewBorder2;
    } else {
        // укладываем цепочку в грань
        int ReplacingBorderIndex = SegmentCorrectIndexes[SegmentMinIndex][0];

        CreateNewBorders( Path, Borders[ReplacingBorderIndex], NewBorder1, NewBorder2, false);
        Borders[ReplacingBorderIndex] = NewBorder1;
        Borders.push_back(NewBorder2);
    }

    // обновляем GPlain
    GPlain = GPlain + Path;

    return true;
}

bool CGraphPlanar::CheckBorderContaining( std::vector<int>& border, CGraph& segment, CGraph& GPlain ) 
{
    for( auto i = segment.BeginVertices(); i != segment.EndVertices(); i++ ) {
        if( GPlain.Contains( *i ) && std::find( border.begin(), border.end(), *i ) == border.end() ) {
            return false;
        }
    }

    return true;
}

void CGraphPlanar::MakeBasicBorders( CGraph& G, std::vector<std::vector<int> >& Borders ) 
{
    std::set<int> used;
    std::vector<int> result;

    int CurrentVertex = *G.BeginVertices();

    used.insert(CurrentVertex);

    while( result.size() != G.NumOfVertices() ) {
        result.push_back(CurrentVertex);

        auto it = G.BeginVertices(CurrentVertex);
        if( used.find(*it) != used.end() ) {
            ++it;
        }

        used.insert( *it );
        CurrentVertex = *it;
    }

    Borders.push_back(result);
}

CGraph operator-( const CGraph& g1, const CGraph& g2 ) 
{
    CGraph res = g1;
    for( std::set<int>::const_iterator i = g2.Vertices.begin(); i != g2.Vertices.end(); i++ ) {
        res.RemoveVertex(*i);
    }
    return res;
}

CGraph operator+( const CGraph& g1, const CGraph& g2 ) 
{
    CGraph res = g1;
    for( auto i = g2.BeginEdges(); i != g2.EndEdges(); i++ ) {
        res.InsertEdge(*i);
    }
    return res;
}

bool operator==( const CGraph& G1, const CGraph& G2 ) 
{
    if( G1.NumOfEdges() != G2.NumOfEdges() || G1.NumOfVertices() != G2.NumOfVertices() ) {
        return false;
    }

    for( auto i = G1.BeginVertices(); i != G1.EndVertices(); i++ ) {
        if( !G2.Contains(*i) ) {
            return false;
        }
    }

    for( auto i = G1.BeginEdges(); i != G1.EndEdges(); i++ ) {
        if( !G2.Contains(*i) ) {
            return false;
        }
    }

    return true;
}

bool operator!=( const CGraph& G1, const CGraph& G2 ) 
{
    return !(G1 == G2);
}
