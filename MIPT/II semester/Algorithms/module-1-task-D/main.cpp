#include <algorithm>
#include <iostream>
#include <map>
#include <queue>
#include <set>
#include <vector>
#include "cgraph.h"

int main() 
{
    int NumOfVertices;
    int NumOfEdges;
    CGraphPlanar G;

    std::cin >> NumOfVertices >> NumOfEdges;

    for( int i = 0; i < NumOfEdges; i++ ) {
        int v1;
        int v2;
        std::cin >> v1 >> v2;
        if( v1 == v2 ) {
            continue;
        }
        G.InsertEdge(v1, v2);
    }

    std::cout << (G.IsPlanar() ? "yes" : "no") << std::endl;

    return 0;
}
