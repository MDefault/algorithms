#ifndef CGRAPHFUNCS_H
#define CGRAPHFUNCS_H

#include <map>
#include <set>
#include <vector>

class CGraph;
class CEdge;

enum TColorsType {
    CT_CURRENT,
    CT_UNCHECKED,
    CT_CHECKED
};

void SplitGraph( CGraph&, std::vector<CGraph>& );

void FindConnectionPoints( CGraph&, std::set<int>& );
void __FindConnectionPoints_dfs( int, int, CGraph&, std::set<int>&, std::map<int, int>&, std::map<int, int>&, int&, std::set<int>& );

void FindBridges( CGraph&, std::set<CEdge>& );
void __FindBridges_dfs( int, int, CGraph&, std::set<int>&, std::map<int, int>&, std::map<int, int>&, int&, std::set<CEdge>& );

bool __FindCycle_dfs( int, CGraph&, std::map<int, int>&, std::map<int, int>&, int&, int& );
bool FindCycle( CGraph&, CGraph& result );

void FindPathBetweenVertices( int, int, CGraph& graph, CGraph& result );

#endif // CGRAPHFUNCS_H
