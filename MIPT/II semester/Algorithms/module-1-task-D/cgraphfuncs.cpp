#include "CGraphFuncs.h"
#include "cgraph.h"

// разбивает граф на связные компоненты
void SplitGraph( CGraph& g, std::vector<CGraph>& result ) 
{
    std::set<int> used;

    for( auto iter = g.BeginVertices(); iter != g.EndVertices(); iter++ ) {
        if( used.find(*iter) == used.end() ) {
            std::queue<int> q;

            q.push( *iter );
            used.insert( *iter );
            result.push_back(CGraph());

            while( !q.empty() ) {
                int vertex = q.front();
                q.pop();

                result.back().InsertVertex( vertex );

                for( auto i = g.BeginVertices(vertex); i != g.EndVertices(vertex); i++ ) {
                    if( used.find(*i) == used.end() ) {
                        q.push( *i );
                        used.insert( *i );
                        result.back().InsertEdge( vertex, *i );
                    }
                }
            }

            for( auto i = g.BeginEdges(); i != g.EndEdges(); i++ ) {
                if( result.back().Contains(i->First()) && result.back().Contains(i->Second()) ) {
                    result.back().InsertEdge(*i);
                }
            }
        }
    }
}

void __FindConnectionPoints_dfs( int v, int p, CGraph& g, std::set<int>& used, std::map<int, int>& tin, std::map<int, int>& fup, int& timer, std::set<int>& result ) 
{
    used.insert(v);
    tin[v] = fup[v] = timer++;
    int children = 0;

    for (auto i = g.BeginVertices(v); i != g.EndVertices(v); i++) {
        int to = *i;
        if( to == p ) {
            continue;
        }
        if( used.find(to) != used.end() ) {
            fup[v] = std::min(fup[v], tin[to]);
        } else {
            __FindConnectionPoints_dfs( to, v, g, used, tin, fup, timer, result );
            fup[v] = std::min(fup[v], fup[to]);
            if ( fup[to] >= tin[v] && p != -1 ) {
                result.insert(v);
            }
            ++children;
        }
    }

    if (p == -1 && children > 1) {
        result.insert(v);
    }
}

// ищет точки сочленения
void FindConnectionPoints( CGraph& g, std::set<int>& result ) 
{
    std::set<int> used;
    int timer = 0;
    std::map<int, int> tin, fup;

    for( auto i = g.BeginVertices(); i != g.EndVertices(); i++ ) {
        if( used.find(*i) == used.end() ) {
            __FindConnectionPoints_dfs(*i, -1, g, used, tin, fup, timer, result);
        }
    }
}

void __FindBridges_dfs( int v, int w, CGraph& g, std::set<int>& used, std::map<int, int>& order, std::map<int, int>& low, int& order_counter, std::set<CEdge>& result ) 
{
    order[w] = order_counter++;
    low[w] = order[w];
    used.insert(w);

    for( auto i = g.BeginVertices(w); i != g.EndVertices(w); i++ ) {
        int to = *i;
        if( used.find(to) == used.end() ) {
            __FindBridges_dfs(w, to, g, used, order, low, order_counter, result);
            if(low[w] > low[to]) low[w] = low[to];
            if(low[to] == order[to]) {
                result.insert( CEdge(to, w) );
            }
        } else if(to != v) {
            if(low[w] > order[to]) low[w] = order[to];
        }
    }
}

// ищет мосты
void FindBridges( CGraph& g, std::set<CEdge>& result ) 
{
    std::map<int, int> order;
    std::map<int, int> low;
    std::set<int> used;
    int order_counter = 0;

    for( auto i = g.BeginVertices(); i != g.EndVertices(); i++ ) {
        if( used.find(*i) == used.end() ) {
            __FindBridges_dfs(*i, *i, g, used, order, low, order_counter, result);
        }
    }
}

bool __FindCycle_dfs( int v, CGraph& g, std::map<int, int>& colors, std::map<int, int>& parents, int& v_st, int& v_end ) 
{
    colors[v] = TColorsType::CT_CURRENT;

    for( auto i = g.BeginVertices(v); i != g.EndVertices(v); i++ ) {
        int to = *i;
        if( colors.find(to) == colors.end() ) {
            parents[to] = v;
            if( __FindCycle_dfs(to, g, colors, parents, v_st, v_end) ) {
                return true;
            }
        } else if( colors[to] == CT_CURRENT && parents[v] != to ) {
            v_st = to;
            v_end = v;
            return true;
        }
    }

    colors[v] = CT_CHECKED;
    return false;
}

// ищет цикл
bool FindCycle( CGraph& g, CGraph& result ) 
{
    std::map<int, int> colors;
    std::map<int, int> parents;
    int v_st = -1;
    int v_end = -1;

    for( auto i = g.BeginVertices(); i != g.EndVertices(); i++ ) {
        if( __FindCycle_dfs(*i, g, colors, parents, v_st, v_end) ) {
            break;
        }
    }

    if( v_end == -1 ) {
        return true;
    }

    result.Clear();
    for( int i = v_end; i != v_st; i = parents[i] ) {
        result.InsertEdge( i, parents[i] );
    }
    result.InsertEdge( v_st, v_end );

    return false;
}

// находит путь между конактными вершинами
void FindPathBetweenVertices( int vertex1, int vertex2, CGraph& graph, CGraph& result ) 
{
    std::set<int> used;
    std::map<int, int> parents;
    std::queue<int> q;

    q.push( vertex1 );
    parents[vertex1] = -1;

    while( !q.empty() ) {
        int v = q.front();
        q.pop();

        used.insert(v);

        if( v == vertex2 ) {
            break;
        }

        for( auto i = graph.BeginVertices(v); i != graph.EndVertices(v); i++ ) {
            if( used.find(*i) == used.end() ) {
                parents[*i] = v;
                q.push(*i);
            }
        }
    }

    result.Clear();

    for( int i = vertex1; parents[i] != -1; i = parents[i] ) {
        result.InsertEdge(i, parents[i]);
    }
}
