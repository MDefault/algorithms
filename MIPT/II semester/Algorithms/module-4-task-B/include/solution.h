#pragma once

#include <iostream>
#include <vector>

struct TaskType {
    int max;
    int add;

    TaskType(int max = 0, int add = -1);

    void Plus(int);
    int Sum() const;
};

TaskType::TaskType(int max, int add)
    : max(max), add(add)
{
}

void TaskType::Plus(int value) {
    if(add == -1) {
        add = value;
    } else {
        add += value;
    }
}

int TaskType::Sum() const {
    return (add == -1) ? max : max + add;
}

TaskType BuildFunc(TaskType a, TaskType b) {
    int max1 = a.Sum();
    int max2 = b.Sum();
    return TaskType(std::max(max1, max2));
}

void BuildSegTree(std::vector<TaskType>& segTree, const std::vector<TaskType>& sourceArr, int segVertex, int leftBorder, int rightBorder) {
    if(leftBorder == rightBorder) {
        segTree[segVertex] = sourceArr[leftBorder];
    } else {
        int middleBorder = (leftBorder + rightBorder) / 2;
        BuildSegTree(segTree, sourceArr, segVertex * 2, leftBorder, middleBorder);
        BuildSegTree(segTree, sourceArr, segVertex * 2 + 1, middleBorder + 1, rightBorder);
        segTree[segVertex] = BuildFunc(segTree[segVertex * 2], segTree[segVertex * 2 + 1]);
    }
}

void PushSegTaskType(std::vector<TaskType>& segTree, int segVertex) {
    int value = segTree[segVertex].add;

    if(value != -1) {
        segTree[segVertex * 2].Plus(value);
        segTree[segVertex * 2 + 1].Plus(value);
        segTree[segVertex].add = -1;
    }
}

TaskType QuerySegTree(std::vector<TaskType>& segTree, int segVertex, int leftBorder, int rightBorder, int queryLeft, int queryRight) {
    if(queryLeft == leftBorder && rightBorder == queryRight) {
        return TaskType(segTree[segVertex].Sum());
    }

    int middleBorder = (leftBorder + rightBorder) / 2;

    int add = (segTree[segVertex].add == -1) ? 0 : segTree[segVertex].add;

    if(queryRight <= middleBorder) {
        return TaskType(QuerySegTree(segTree, segVertex * 2, leftBorder, middleBorder, queryLeft, queryRight).Sum() + add);
    } else if(queryLeft >= middleBorder + 1) {
        return TaskType(QuerySegTree(segTree, segVertex * 2 + 1, middleBorder + 1, rightBorder, queryLeft, queryRight).Sum() + add);
    }

    return std::max(QuerySegTree(segTree, segVertex * 2, leftBorder, middleBorder, queryLeft, middleBorder).Sum(),
                        QuerySegTree(segTree, segVertex * 2 + 1, middleBorder + 1, rightBorder, middleBorder + 1, queryRight).Sum()) + add;
}

void UpdateSegTree(std::vector<TaskType>& segTree, int segVertex, int leftBorder, int rightBorder, int queryLeft, int queryRight, int value) {
    if(leftBorder == queryLeft && rightBorder == queryRight) {
        segTree[segVertex].Plus(value);
    } else {
        PushSegTaskType(segTree, segVertex);

        int middleBorder = (leftBorder + rightBorder) / 2;

        if(queryRight <= middleBorder) {
            UpdateSegTree(segTree, segVertex * 2, leftBorder, middleBorder, queryLeft, queryRight, value);
        } else if(queryLeft >= middleBorder + 1) {
            UpdateSegTree(segTree, segVertex * 2 + 1, middleBorder + 1, rightBorder, queryLeft, queryRight, value);
        } else {
            UpdateSegTree(segTree, segVertex * 2, leftBorder, middleBorder, queryLeft, middleBorder, value);
            UpdateSegTree(segTree, segVertex * 2 + 1, middleBorder + 1, rightBorder, middleBorder + 1, queryRight, value);
        }

        segTree[segVertex] = BuildFunc(segTree[segVertex * 2], segTree[segVertex * 2 + 1]);
    }
}

void SolveTask(std::ostream& ostr, std::istream& istr) {
    int numOfCities;
    istr >> numOfCities;

    std::vector<TaskType> segTree(numOfCities * 4);

    std::vector<TaskType> sourceTaskType(numOfCities, TaskType(0, -1));

    for(int i = 0; i < numOfCities - 1; i++) {
        int initValue = 0;
        istr >> initValue;
        sourceTaskType[i].max = initValue;
    }

    BuildSegTree(segTree, sourceTaskType, 1, 0, numOfCities - 1);

    int maxT = 0;
    istr >> maxT;

    int numOfQueries;
    istr >> numOfQueries;

    for(int i = 0; i < numOfQueries; i++) {
        int start;
        int end;
        int num;

        istr >> start >> end >> num;

        if(QuerySegTree(segTree, 1, 0, numOfCities - 1, start, end - 1).max + num > maxT) {
            ostr << i << ' ';
        } else {
            UpdateSegTree(segTree, 1, 0, numOfCities - 1, start, end - 1, num);
        }
    }
}
