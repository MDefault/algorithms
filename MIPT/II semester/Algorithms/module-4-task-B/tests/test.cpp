#define CATCH_CONFIG_MAIN

#include <iostream>
#include <sstream>
#include <string>
#include <catch2/catch.hpp>
#include "../include/solution.h"

TEST_CASE("ANSWER_HOLDER", "TEST1") {
    std::ostringstream ostr;
    std::istringstream istr("6\n1 0 0 0 1\n5\n8\n0 2 1\n1 4 2\n2 5 1\n1 3 3\n3 5 2\n0 4 1\n0 1 3\n1 3 2");
    SolveTask(ostr, istr);
    
    REQUIRE(ostr.str() == "3 5 ");
}

TEST_CASE("ANSWER_HOLDER2", "TEST2") {
	std::ostringstream ostr;
    std::istringstream istr("6\n0 1 1 0 0\n7\n4\n1 3 5\n2 4 1\n1 5 1\n4 5 3");
    SolveTask(ostr, istr);
    
    REQUIRE(ostr.str() == "2 ");
}

TEST_CASE("ANSWER_HOLDER3", "TEST3") {
	std::ostringstream ostr;
    std::istringstream istr("5\n1 1 1 1\n4\n5\n1 2 1\n2 3 1\n3 4 4\n3 4 2\n1 4 1");
    SolveTask(ostr, istr);
    
    REQUIRE(ostr.str() == "2 ");
}
