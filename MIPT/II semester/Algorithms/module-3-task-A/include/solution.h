#pragma once
#include <bits/stdc++.h>

int Minkey(const std::vector<int>& key, const std::vector<bool>& MSTSet)
{
    int min = INT_MAX;
    int minIndex = -1;

    for(int vertex = 0; vertex < key.size(); vertex++) {
        if(!MSTSet[vertex] && key[vertex] < min) {
            min = key[vertex];
            minIndex = vertex;
        }
    }

    return minIndex;
}

void PrintMST(const std::vector<int>& parent, const std::vector< std::vector<int> >& graph, std::ostream& ostr) {
    long long Sum = 0;

    for(int i = 1; i < parent.size(); i++) {
        Sum += graph[i][parent[i]];
    }

    ostr << Sum;
}

void PrimMST(const std::vector< std::vector<int> >& graph, std::vector<int>& parent)
{
    int NumOfVertices = graph.size();
    std::vector<int> key(NumOfVertices);
    std::vector<bool> MSTSet(NumOfVertices);

    for (int i = 0; i < NumOfVertices; i++) {
        key[i] = INT_MAX;
        MSTSet[i] = false;
    }

    key[0] = 0;
    parent[0] = -1;

    for(int count = 0; count < NumOfVertices - 1; count++)
    {
        int vertex1 = Minkey(key, MSTSet);

        MSTSet[vertex1] = true;

        for(int vertex2 = 0; vertex2 < NumOfVertices; vertex2++) {
            if(graph[vertex1][vertex2] && !MSTSet[vertex2] && graph[vertex1][vertex2] < key[vertex2]) {
                parent[vertex2] = vertex1;
                key[vertex2] = graph[vertex1][vertex2];
            }
        }
    }
}

void SolveTask(std::ostream& ostr, std::istream& istr) {
    int NumOfVertices;
    int NumOfEdges;

    istr >> NumOfVertices >> NumOfEdges;

    std::vector< std::vector<int> > graph(NumOfVertices, std::vector<int>(NumOfVertices, 0));
    std::vector<int> parents(NumOfVertices);

    for(std::size_t i = 0; i < NumOfEdges; i++) {
        int vertex1;
        int vertex2;
        int Cost;

        istr >> vertex1 >> vertex2 >> Cost;
        --vertex1;
        --vertex2;

        graph[vertex1][vertex2] = Cost;
        graph[vertex2][vertex1] = Cost;
    }

    PrimMST(graph, parents);
    PrintMST(parents, graph, ostr);
}

