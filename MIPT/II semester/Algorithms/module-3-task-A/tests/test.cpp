#define CATCH_CONFIG_MAIN

#include <iostream>
#include <sstream>
#include <string>
#include <catch2/catch.hpp>
#include "../include/solution.h"

TEST_CASE("ANSWER_HOLDER", "TEST1") {
    std::ostringstream ostr;
    std::istringstream istr("2 1\n1 2 10986");
    SolveTask(ostr, istr);
    
    REQUIRE(ostr.str() == "10986");
}

TEST_CASE("ANSWER_HOLDER2", "TEST2") {
	std::ostringstream ostr;
    std::istringstream istr("4 4\n2 3 10\n1 2 2\n1 3 3\n1 4 4");
    SolveTask(ostr, istr);
    
    REQUIRE(ostr.str() == "9");
}

TEST_CASE("ANSWER_HOLDER3", "TEST3") {
	std::ostringstream ostr;
    std::istringstream istr("3 3\n1 2 1\n2 3 2\n3 1 3");
    SolveTask(ostr, istr);
    
    REQUIRE(ostr.str() == "3");
}
