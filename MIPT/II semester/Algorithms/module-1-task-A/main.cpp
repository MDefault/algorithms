#include <iostream>
#include <queue>
#include <vector>
#include <utility>

enum TEdgesDistance {
	NON = 0,
	INFINITY = 100000000
};

// находит кратчайшее расстояние (в ребрах) от from, до всех остальных
void CalculateDistances( int from, std::vector<int>& dist, const std::vector< std::vector<int> >& graph ) {
    std::queue< std::pair<int, int> > q;
    std::vector<bool> used(graph.size(), false);

    dist.resize(graph.size(), TEdgesDistance::INFINITY);
    q.push( std::make_pair(from, TEdgesDistance::NON) );
    used[from] = true;

    while( !q.empty() ) {
        int vertex = q.front().first;
        dist[vertex] = q.front().second;
        q.pop();

        for( int i = 0; i < graph[vertex].size(); i++ ) {
            int vertex2 = graph[vertex][i];
            if( !used[vertex2] ) {
                used[vertex2] = true;
                q.push( std::make_pair(vertex2, dist[vertex] + 1) );
            }
        }
    }
}


int main() {
    int numOfVertices;
    int numOfEdges;
    int Leon;
    int Matilda;
    int Milk;

    std::cin >> numOfVertices >> numOfEdges >> Leon >> Matilda >> Milk;
    --Leon;
    --Matilda;
    --Milk;

    std::vector< std::vector<int> > graph(numOfVertices);

    for( int i = 0; i < numOfEdges; i++ ) {
        int vertex1;
	int vertex2;
        std::cin >> vertex1 >> vertex2;
        --vertex1;
	--vertex2;
        graph[vertex1].push_back(vertex2);
        graph[vertex2].push_back(vertex1);
    }

    std::vector<int> distances[3];
    CalculateDistances(Leon, distances[0], graph);
    CalculateDistances(Matilda, distances[1], graph);
    CalculateDistances(Milk, distances[2], graph);

    int minDistance = TEdgesDistance::INFINITY;
    for( int i = 0; i < numOfVertices; i++ ) {
        minDistance = std::min( minDistance, distances[0][i] + distances[1][i] + distances[2][i] );
    }

    std::cout << minDistance << std::endl;

    return 0;
}
