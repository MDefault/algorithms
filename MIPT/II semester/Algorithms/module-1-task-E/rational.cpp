#include "CRational.h"

CRational::CRational() : 
	Up(0), 
	Down(1)
{
}

CRational::CRational( int Value ) : 
	Up(Value), 
	Down(1)
{
}

CRational::CRational( const CBigInteger& Int) : 
	Up(Int), 
	Down(1)
{
}

CRational::CRational( const CRational& R ) : 
	Up(R.Up), 
	Down(R.Down)
{
}

CRational::~CRational() 
{
}

CRational& CRational::operator=( const CRational& R) 
{
    if( this == &R ) {
        return *this;
    }

    Up = R.Up;
    Down = R.Down;

    return *this;
}

CRational& CRational::operator+=( const CRational& R ) 
{
    Up = Up * R.Down + Down * R.Up;
    Down = Down * R.Down;

    CBigInteger nod = gcd( abs(Up), abs(Down) );

    Up /= nod;
    Down /= nod;

    return *this;
}

CRational& CRational::operator-=( const CRational& R ) 
{
    *this = -*this;
    *this += R;
    *this = -*this;

    return *this;
}

CRational& CRational::operator*=( const CRational& R ) 
{
    Up *= R.Up;
    Down *= R.Down;

    CBigInteger nod = gcd( abs(Up), abs(Down) );
    Up /= nod;
    Down /= nod;

    return *this;
}

CRational& CRational::operator/=( const CRational& R ) 
{
    if( R.Up == 0 ) {
        exit(-1);
    }

    Up *= R.Down;
    Down *= R.Up;

    if( Down < CBigInteger(0) ) {
        Down = -Down;
        Up = -Up;
    }

    CBigInteger nod = gcd( abs(Up), abs(Down) );
    Up /= nod;
    Down /= nod;

    return *this;
}

CRational CRational::operator-() const 
{
    CRational result = *this;
    result.Up = -result.Up;
    return result;
}

bool CRational::operator==( const CRational& R ) const 
{
    return Up == R.Up && Down == R.Down;
}

bool CRational::operator!=( const CRational& R ) const 
{
    return !(*this == R);
}

bool CRational::operator<=( const CRational& R ) const 
{
    return *this < R || *this == R;
}

bool CRational::operator>=( const CRational& R ) const 
{
    return !(*this < R);
}

bool CRational::operator<( const CRational& R ) const 
{
    return Up * R.Down < R.Up * Down;
}

bool CRational::operator>( const CRational& R ) const 
{
    return !(*this <= R);
}

std::string CRational::toString() const 
{
    std::string result;
    result += Up.toString();

    if( Up == 0 ) {
        return "0";
    }

    if( Down != 1 ) {
        result.push_back('/');
        result += Down.toString();
    }

    return result;
}

std::string CRational::asDecimal( size_t precision ) const 
{
    std::string result;
    CBigInteger Current = Up;

    if( Up == 0 ) {
        return "0";
    } else if( Up < 0 ) {
        Current = -Current;
        result.push_back('-');
    }

    result += (Current / Down).toString();
    result.push_back('.');

    Current %= Down;
    Current *= 10;
    precision += result.size();

    while( result.size() < precision ) {
        if( Current < Down ) {
            result.push_back('0');
            Current *= 10;
        } else {
            result += (Current / Down).toString();
            Current %= Down;
            Current *= 10;
        }
    }

    while( result.back() == '0' ) {
        result.pop_back();
    }

    if( result.back() == '.' ) {
        result.push_back('0');
    }

    return result;
}

CRational::operator double() const 
{
    double result = 0.0;
    double Mul = 1.0;
    CBigInteger Current = Up;

    bool IsNegative = Current < 0;
    Current = abs(Current);

    std::string FirstPart = (Current / Down).toString();
    for( size_t i = 0; i < FirstPart.size(); i++ ) {
        result = result * 10 + (FirstPart[i] - '0');
    }

    Current %= Down;

    for( int i = 0; i < 10; i++, Mul /= 10.0 ) {
        if( Current < Down ) {
            Current *= 10;
        } else {
            result += Mul * (( Current / Down ).toString()[0] - '0');
            Current %= Down;
            Current *= 10;
        }
    }

    if( IsNegative ) {
        result = -result;
    }

    return result;
}

CBigInteger CRational::gcd( const CBigInteger& a, const CBigInteger& b ) 
{
    if( a == 0 ) {
        return b;
    } else if( b == 0 ) {
        return a;
    } else if( a == 1 || b == 1 ) {
        return 1;
    }

    if( a > b ) {
        return gcd( b, a );
    }

    return gcd( b % a, a );
}

CBigInteger CRational::abs( const CBigInteger& Int ) 
{
    if( Int < 0 ) {
        return -Int;
    }
    return Int;
}

CRational operator+( const CRational& R1, const CRational& R2 ) 
{
    CRational result = R1;
    result += R2;
    return result;
}

CRational operator+( const CRational& R1, int R2 ) 
{
    CRational result = R1;
    result += CRational(R2);
    return result;
}

CRational operator+( int R1, const CRational& R2 ) 
{
    CRational result = R1;
    result += R2;
    return result;
}

CRational operator+( const CRational& R1, const CBigInteger& R2 ) 
{
    CRational result = R1;
    result += CRational(R2);
    return result;
}

CRational operator+( const CBigInteger& R1, const CRational& R2 ) 
{
    CRational result = R1;
    result += R2;
    return result;
}

CRational operator-( const CRational& R1, const CRational& R2 ) 
{
    CRational result = R1;
    result -= R2;
    return result;
}

CRational operator-( const CRational& R1, int R2 ) 
{
    CRational result = R1;
    result -= CRational(R2);
    return result;
}

CRational operator-( int R1, const CRational& R2 ) 
{
    CRational result = R1;
    result -= R2;
    return result;
}

CRational operator-( const CRational& R1, const CBigInteger& R2 ) 
{
    CRational result = R1;
    result -= CRational(R2);
    return result;
}

CRational operator-( const CBigInteger& R1, const CRational& R2 ) 
{
    CRational result = R1;
    result -= R2;
    return result;
}

CRational operator*( const CRational& R1, const CRational& R2 ) 
{
    CRational result = R1;
    result *= R2;
    return result;
}

CRational operator*( const CRational& R1, int R2 ) 
{
    CRational result = R1;
    result *= CRational(R2);
    return result;
}

CRational operator*( int R1, const CRational& R2 ) 
{
    CRational result = R1;
    result *= R2;
    return result;
}

CRational operator*( const CRational& R1, const CBigInteger& R2 ) 
{
    CRational result = R1;
    result *= CRational(R2);
    return result;
}

CRational operator*( const CBigInteger& R1, const CRational& R2 ) 
{
    CRational result = R1;
    result *= R2;
    return result;
}

CRational operator/( const CRational& R1, const CRational& R2 ) 
{
    CRational result = R1;
    result /= R2;
    return result;
}

CRational operator/( const CRational& R1, int R2 ) 
{
    CRational result = R1;
    result /= CRational(R2);
    return result;
}

CRational operator/( int R1, const CRational& R2 ) 
{
    CRational result = R1;
    result /= R2;
    return result;
}

CRational operator/( const CRational& R1, const CBigInteger& R2 )
{
    CRational result = R1;
    result /= CRational(R2);
    return result;
}

CRational operator/( const CBigInteger& R1, const CRational& R2 ) 
{
    CRational result = R1;
    result /= R2;
    return result;
}

bool operator==( const CRational& R1, const CBigInteger& R2 ) 
{
    return R1 == CRational(R2);
}

bool operator==( const CBigInteger& R1, const CRational& R2 ) 
{
    return CRational(R1) == R2;
}

bool operator==( const CRational& R1, int R2 ) 
{
    return R1 == CRational(R2);
}

bool operator==( int R1, const CRational& R2 ) 
{
    return CRational(R1) == R2;
}

bool operator!=( const CRational& R1, const CBigInteger& R2 ) 
{
    return R1 != CRational(R2);
}

bool operator!=( const CBigInteger& R1, const CRational& R2 ) 
{
    return CRational(R1) != R2;
}

bool operator!=( const CRational& R1, int R2 ) 
{
    return R1 != CRational(R2);
}

bool operator!=( int R1, const CRational& R2 ) 
{
    return CRational(R1) != R2;
}

bool operator<=( const CRational& R1, const CBigInteger& R2 ) 
{
    return R1 <= CRational(R2);
}

bool operator<=( const CBigInteger& R1, const CRational& R2 ) 
{
    return CRational(R1) <= R2;
}

bool operator<=( const CRational& R1, int R2 ) 
{
    return R1 <= CRational(R2);
}

bool operator<=( int R1, const CRational& R2 ) 
{
    return CRational(R1) <= R2;
}

bool operator>=( const CRational& R1, const CBigInteger& R2 ) 
{
    return R1 >= CRational(R2);
}

bool operator>=( const CBigInteger& R1, const CRational& R2 ) 
{
    return CRational(R1) >= R2;
}

bool operator>=( const CRational& R1, int R2 ) 
{
    return R1 >= CRational(R2);
}

bool operator>=( int R1, const CRational& R2 ) 
{
    return CRational(R1) >= R2;
}

bool operator<( const CRational& R1, const CBigInteger& R2 ) 
{
    return R1 < CRational(R2);
}

bool operator<( const CBigInteger& R1, const CRational& R2 ) 
{
    return CRational(R1) < R2;
}

bool operator<( const CRational& R1, int R2 ) 
{
    return R1 < CRational(R2);
}

bool operator<( int R1, const CRational& R2 ) 
{
    return CRational(R1) < R2;
}

bool operator>( const CRational& R1, const CBigInteger& R2 ) 
{
    return R1 > CRational(R2);
}

bool operator>( const CBigInteger& R1, const CRational& R2 ) 
{
    return CRational(R1) > R2;
}

bool operator>( const CRational& R1, int R2 ) 
{
    return R1 > CRational(R2);
}

bool operator>( int R1, const CRational& R2 ) 
{
    return CRational(R1) > R2;
}
