#ifndef CRational_H
#define CRational_H

#include "biginteger.h"

class CRational {

public:
    CRational();
    CRational( int );
    CRational( const CBigInteger& );
    CRational( const CRational& );
    ~CRational();

    CRational& operator=( const CRational& );

    CRational& operator+=( const CRational& );
    CRational& operator-=( const CRational& );
    CRational& operator*=( const CRational& );
    CRational& operator/=( const CRational& );

    CRational operator-() const;

    bool operator==( const CRational& ) const;
    bool operator!=( const CRational& ) const;
    bool operator<=( const CRational& ) const;
    bool operator>=( const CRational& ) const;
    bool operator<( const CRational& ) const;
    bool operator>( const CRational& ) const;

    std::string toString() const;
    std::string asDecimal( size_t precision = 0 ) const;

    explicit operator double() const;

private:
    CBigInteger Up;
    CBigInteger Down;

    static CBigInteger gcd( const CBigInteger&, const CBigInteger& );
    static CBigInteger abs( const CBigInteger& );
};

CRational operator+( const CRational&, const CRational& );
CRational operator+( const CRational&, int );
CRational operator+( int, const CRational& );
CRational operator+( const CRational&, const CBigInteger& );
CRational operator+( const CBigInteger&, const CRational& );

CRational operator-( const CRational&, const CRational& );
CRational operator-( const CRational&, int );
CRational operator-( int, const CRational& );
CRational operator-( const CRational&, const CBigInteger& );
CRational operator-( const CBigInteger&, const CRational& );

CRational operator*( const CRational&, const CRational& );
CRational operator*( const CRational&, int );
CRational operator*( int, const CRational& );
CRational operator*( const CRational&, const CBigInteger& );
CRational operator*( const CBigInteger&, const CRational& );

CRational operator/( const CRational&, const CRational& );
CRational operator/( const CRational&, int );
CRational operator/( int, const CRational& );
CRational operator/( const CRational&, const CBigInteger& );
CRational operator/( const CBigInteger&, const CRational& );

bool operator==( const CRational&, const CBigInteger& );
bool operator==( const CBigInteger&, const CRational& );
bool operator==( const CRational&, int );
bool operator==( int, const CRational& );

bool operator!=( const CRational&, const CBigInteger& );
bool operator!=( const CBigInteger&, const CRational& );
bool operator!=( const CRational&, int );
bool operator!=( int, const CRational& );

bool operator<=( const CRational&, const CBigInteger& );
bool operator<=( const CBigInteger&, const CRational& );
bool operator<=( const CRational&, int );
bool operator<=( int, const CRational& );

bool operator>=( const CRational&, const CBigInteger& );
bool operator>=( const CBigInteger&, const CRational& );
bool operator>=( const CRational&, int );
bool operator>=( int, const CRational& );

bool operator<( const CRational&, const CBigInteger& );
bool operator<( const CBigInteger&, const CRational& );
bool operator<( const CRational&, int );
bool operator<( int, const CRational& );

bool operator>( const CRational&, const CBigInteger& );
bool operator>( const CBigInteger&, const CRational& );
bool operator>( const CRational&, int );
bool operator>( int, const CRational& );

#endif // CRational_H
