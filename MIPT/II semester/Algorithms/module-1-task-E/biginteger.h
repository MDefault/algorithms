#pragma once

#include <iostream>
#include <string>
#include <vector>

class CBigInteger {

public:
    CBigInteger();
    CBigInteger( int );
    explicit CBigInteger( long long );
    CBigInteger( const CBigInteger& );
    ~CBigInteger();

    CBigInteger& operator=( const CBigInteger& );

    CBigInteger& operator+=( const CBigInteger& );
    CBigInteger& operator-=( const CBigInteger& );
    CBigInteger& operator*=( const CBigInteger& );
    CBigInteger& operator*=( int );
    CBigInteger& operator/=( const CBigInteger& );
    CBigInteger& operator/=( int );
    CBigInteger& operator%=( const CBigInteger& );
    CBigInteger& operator%=( int );

    CBigInteger& operator++();
    CBigInteger operator++( int );
    CBigInteger& operator--();
    CBigInteger operator--( int );

    CBigInteger operator-() const;

    bool operator==( const CBigInteger& ) const;
    bool operator!=( const CBigInteger& ) const;
    bool operator<=( const CBigInteger& ) const;
    bool operator>=( const CBigInteger& ) const;
    bool operator<( const CBigInteger& ) const;
    bool operator>( const CBigInteger& ) const;

    operator bool() const;

    std::string toString() const;

    friend std::ostream& operator<<( std::ostream&, const CBigInteger& );
    friend std::istream& operator>>( std::istream&, CBigInteger& );

private:
    typedef long long BaseType;
    static const size_t BaseLen = 9;
    static const BaseType Base = 1000000000;
    static const size_t KaratsubaMinLen = 4;

    std::vector<BaseType> Digits;
    bool IsNegative;

    static CBigInteger PositivePlus( const CBigInteger&, const CBigInteger& ); // |a| >= |b|
    static CBigInteger PositiveMinus( const CBigInteger&, const CBigInteger& ); // |a| >= |b|

    static CBigInteger PositiveShortMul( const CBigInteger&, int );
    static CBigInteger PositiveNativeMul( const CBigInteger&, const CBigInteger& );
    static CBigInteger PositiveKaratsubaMul( const CBigInteger&, const CBigInteger& );
    static void ShiftInteger( CBigInteger&, size_t );
    static void Copy( const CBigInteger&, CBigInteger&, size_t, size_t ); // [,)

    static std::pair<CBigInteger, CBigInteger> PositiveNativeDiv( const CBigInteger&, int );
    static std::pair<CBigInteger, CBigInteger> PositiveNativeDiv(const CBigInteger&, const CBigInteger&);

    static bool AbsEqual( const CBigInteger&, const CBigInteger& );
    static bool AbsLess( const CBigInteger&, const CBigInteger& );
    static void Normalize( CBigInteger& );

    static std::string itoa(BaseType , size_t Precision);
};

CBigInteger operator+( const CBigInteger&, const CBigInteger& );
CBigInteger operator+( const CBigInteger&, int );
CBigInteger operator+( int, const CBigInteger& );

CBigInteger operator-( const CBigInteger&, const CBigInteger& );
CBigInteger operator-( const CBigInteger&, int );
CBigInteger operator-( int, const CBigInteger& );

CBigInteger operator*( const CBigInteger&, const CBigInteger& );
CBigInteger operator*( const CBigInteger&, int );
CBigInteger operator*( int, const CBigInteger& );

CBigInteger operator/( const CBigInteger&, const CBigInteger& );
CBigInteger operator/( const CBigInteger&, int );
CBigInteger operator/( int, const CBigInteger& );

CBigInteger operator%( const CBigInteger&, const CBigInteger& );
CBigInteger operator%( const CBigInteger&, int );
CBigInteger operator%( int, const CBigInteger& );

bool operator==( const CBigInteger&, int );
bool operator==( int, const CBigInteger& );

bool operator!=( const CBigInteger&, int );
bool operator!=( int, const CBigInteger& );

bool operator<=( const CBigInteger&, int );
bool operator<=( int, const CBigInteger& );

bool operator>=( const CBigInteger&, int );
bool operator>=( int, const CBigInteger& );

bool operator<( const CBigInteger&, int );
bool operator<( int, const CBigInteger& );

bool operator>( const CBigInteger&, int );
bool operator>( int, const CBigInteger& );
