#ifndef SOLUTION_H
#define SOLUTION_H

#include <bitset>
#include <iostream>
#include <string>
#include <vector>

const int BitMaskLen = 64;

void SetGraphRow(std::vector< std::bitset<BitMaskLen> >& graph, const std::string& row) {
    for (int j = 0; j < row.size(); j++) {
        if (row[j] == '1') {
            graph[j / BitMaskLen].set(BitMaskLen - 1 - j % BitMaskLen);
        }
    }
}

void FloydAlg(std::vector< std::vector< std::bitset<BitMaskLen> > >& graph) {
    for (int k = 0; k < graph.size(); k++) {
        for (int i = 0; i < graph.size(); i++) {
            if (graph[i][k / BitMaskLen].test(BitMaskLen - 1 - (k % BitMaskLen))) {
                for (int j = 0; j < graph.size() / BitMaskLen + 1; j++) {
                    graph[i][j] = graph[i][j] | graph[k][j];
                }
            }
        }
    }
}

void SolveTask(std::ostream& ostr, std::istream& istr) {
    int numOfVertices;
    std::vector< std::vector< std::bitset<BitMaskLen> > > graph;

    istr >> numOfVertices;

    graph.resize(numOfVertices, std::vector< std::bitset<BitMaskLen> >(numOfVertices / BitMaskLen + 1));

    for (int i = 0; i < numOfVertices; i++) {
        std::string row;

        istr >> row;
        SetGraphRow(graph[i], row);
    }

    FloydAlg(graph);

    for (int i = 0; i < numOfVertices; i++) {
        std::string result(numOfVertices, '0');

        for (int j = 0; j < numOfVertices; j++) {
            result[j] = graph[i][j / BitMaskLen].test(BitMaskLen - 1 - j % BitMaskLen) + '0';
        }
        ostr << result << std::endl;
    }
}

#endif // SOLUTION_H
