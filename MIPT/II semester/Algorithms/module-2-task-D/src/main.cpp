#include <bitset>
#include <iostream>
#include <string>
#include <vector>
#include "solution.h"

int main() {
    std::ios_base::sync_with_stdio(false);
    std::cin.tie(nullptr);
    std::cout.tie(nullptr);

    SolveTask(std::cout, std::cin);

    return 0;
}

