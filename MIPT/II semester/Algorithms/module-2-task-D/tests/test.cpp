#define CATCH_CONFIG_MAIN

#include <iostream>
#include <sstream>
#include <string>
#include <catch2/catch.hpp>
#include "../include/solution.h"

TEST_CASE("ANSWER_HOLDER", "TEST1") {
    std::ostringstream ostr;
    std::istringstream istr("3\n010\n001\n000");
    SolveTask(ostr, istr);
    
    REQUIRE(ostr.str() == "011\n001\n000\n");
}

TEST_CASE("ANSWER_HOLDER2", "TEST2") {
	std::ostringstream ostr;
	std::istringstream istr("4\n0100\n0011\n0000\n0000");
	SolveTask(ostr, istr);
	
	REQUIRE(ostr.str() == "0111\n0011\n0000\n0000\n");
}

TEST_CASE("ANSWER_HOLDER3", "TEST3") {
	std::ostringstream ostr;
	std::istringstream istr("4\n0100\n0010\n0001\n1000");
	SolveTask(ostr, istr);
	
	REQUIRE(ostr.str() == "1111\n1111\n1111\n1111\n");
}
