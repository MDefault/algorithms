#include <bits/stdc++.h>
#include <iostream>

class SuperStringArray {
public:
    SuperStringArray();
    SuperStringArray(const SuperStringArray&);
    ~SuperStringArray();

    std::string AtPos(int);
    void Insert(const std::string&, int);
    void Remove(int);
private:
    struct Node {
        int priority;
        int count;
        std::string value;
        Node* right;
        Node* left;
    };
private:
    int Count(Node*);
    void UpdateCount(Node*);
    Node* Merge(Node*, Node*);
    std::pair<Node*, Node*> Split(Node*, int, int);
    Node* MakeNode(const std::string&);
    void DeleteTree(Node*);
    Node* AtPos(Node*, int);
    Node* Insert(const std::string&, Node*, int);
    Node* Remove(Node*, int);
    Node* DeepCopy(Node*);
private:
    Node* root;
};

SuperStringArray::SuperStringArray()
    : root(nullptr)
{
}

SuperStringArray::SuperStringArray(const SuperStringArray& SSA)
    : SuperStringArray()
{
    root = DeepCopy(SSA.root);
}

SuperStringArray::~SuperStringArray()
{
    DeleteTree(root);
}

int SuperStringArray::Count(Node* n) {
    return n ? n->count : 0;
}

void SuperStringArray::UpdateCount(Node* n) {
    if(n) {
        n->count = Count(n->left) + Count(n->right) + 1;
    }
}

SuperStringArray::Node* SuperStringArray::Merge(Node* left, Node* right) {
    Node* root;

    if(!left || !right) {
        return left ? left : right;
    } else if(left->priority > right->priority) {
        root = left;
        left->right = Merge(left->right, right);
    } else {
        root = right;
        right->left = Merge(left, right->left);
    }

    UpdateCount(root);

    return root;
}

std::pair<SuperStringArray::Node*, SuperStringArray::Node*> SuperStringArray::Split(Node* root, int key, int tmp) {
    if(!root) {
        return { nullptr, nullptr };
    }
    Node* left;
    Node* right;

    int current = tmp + Count(root->left);
    if(key <= current) {
        right = root;
        auto res = Split(root->left, key, tmp);
        left = res.first;
        root->left = res.second;
    } else {
        left = root;
        auto res = Split(root->right, key, tmp + 1 + Count(root->left));
        root->right = res.first;
        right = res.second;
    }

    UpdateCount(root);

    return { left, right };
}

SuperStringArray::Node* SuperStringArray::MakeNode(const std::string& value) {
    Node* result = new Node;

    result->count = 1;
    result->right = result->left = nullptr;
    result->value = value;
    result->priority = rand();

    return result;
}

void SuperStringArray::DeleteTree(Node* root) {
    if(!root) {
        return;
    }

    DeleteTree(root->left);
    DeleteTree(root->right);
    delete root;
}

SuperStringArray::Node* SuperStringArray::AtPos(Node* root, int pos) {
    if(Count(root->left) == pos) {
        return root;
    } else if(Count(root->left) > pos) {
        return AtPos(root->left, pos);
    }

    return AtPos(root->right, pos - Count(root->left) - 1);
}

std::string SuperStringArray::AtPos(int pos) {
    return AtPos(root, pos)->value;
}

SuperStringArray::Node* SuperStringArray::Insert(const std::string& str, Node* root, int pos) {
    auto half = Split(root, pos, 0);
    half.first = Merge(half.first, MakeNode(str));
    return Merge(half.first, half.second);
}

void SuperStringArray::Insert(const std::string& str, int pos) {
    root = Insert(str, root, pos);
}

SuperStringArray::Node* SuperStringArray::Remove(Node* root, int pos) {
    auto half = Split(root, pos, 0);
    auto half2 = Split(half.second, 1, 0);
    delete half2.first;
    return Merge(half.first, half2.second);
}

void SuperStringArray::Remove(int pos) {
    root = Remove(root, pos);
}

SuperStringArray::Node* SuperStringArray::DeepCopy(Node* from) {
    if(from == nullptr) {
        return from;
    }

    Node* result = new Node;
    result->left = result->right = nullptr;
    result->count = from->count;
    result->value = from->value;
    result->priority = from->priority;

    result->left = DeepCopy(from->left);
    result->right = DeepCopy(from->right);

    return result;
}

void SolveTask(std::ostream& ostr, std::istream& istr) {
    int K;
    istr >> K;

    SuperStringArray root;
    for(int i = 0; i < K; i++) {
        char type;
        std::string value;
        int num1;
        int num2;

        istr >> type;

        if(type == '+') {
            istr >> num1 >> value;

            root.Insert(value, num1);
        } else if(type == '-') {
            istr >> num1 >> num2;

            while(num2 >= num1) {
                root.Remove(num1);
                --num2;
            }
        } else {
            istr >> num1;

            ostr << root.AtPos(num1) << '\n';
        }
    }
}
