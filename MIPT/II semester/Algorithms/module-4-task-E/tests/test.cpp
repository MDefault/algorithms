#define CATCH_CONFIG_MAIN

#include <iostream>
#include <sstream>
#include <string>
#include <catch2/catch.hpp>
#include "../include/solution.h"

TEST_CASE("ANSWER_HOLDER", "TEST1") {
    std::ostringstream ostr;
    std::istringstream istr("6\n+ 0 myau\n+ 0 krya\n? 0\n+ 2 gav\n- 1 1\n? 1");
    SolveTask(ostr, istr);
    
    REQUIRE(ostr.str() == "krya\ngav\n");
}

TEST_CASE("ANSWER_HOLDER2", "TEST2") {
	std::ostringstream ostr;
    std::istringstream istr("4\n+ 0 First\n+ 0 Second\n? 1\n? 0");
    SolveTask(ostr, istr);
    
    REQUIRE(ostr.str() == "First\nSecond\n");
}

TEST_CASE("ANSWER_HOLDER3", "TEST3") {
	std::ostringstream ostr;
    std::istringstream istr("5\n+ 0 A\n+ 1 B\n+ 2 C\n- 1 1\n? 1");
    SolveTask(ostr, istr);
    
    REQUIRE(ostr.str() == "C\n");
}
