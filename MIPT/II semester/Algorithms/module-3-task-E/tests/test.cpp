#define CATCH_CONFIG_MAIN

#include <iostream>
#include <sstream>
#include <string>
#include <catch2/catch.hpp>
#include "../include/solution.h"

TEST_CASE("ANSWER_HOLDER", "TEST1") {
    std::ostringstream ostr;
    std::istringstream istr("3 3 1 3\n1 2\n1 3\n2 3");
    SolveTask(ostr, istr);
    
    REQUIRE(ostr.str() == "YES\n1 2 3\n1 3");
}

TEST_CASE("ANSWER_HOLDER2", "TEST2") {
	std::ostringstream ostr;
    std::istringstream istr("4 3 2 4\n1 2\n1 3\n1 4");
    SolveTask(ostr, istr);
    
    REQUIRE(ostr.str() == "NO");
}

TEST_CASE("ANSWER_HOLDER3", "TEST3") {
	std::ostringstream ostr;
    std::istringstream istr("5 6 1 3\n1 2\n2 3\n3 4\n4 5\n1 4\n1 3");
    SolveTask(ostr, istr);
    
    REQUIRE(ostr.str() == "YES\n1 2 3\n1 3");
}
