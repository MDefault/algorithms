#include <iostream>
#include <bits/stdc++.h>

// to, cost
void Dekstra(int Vertex, std::vector<int>& MinDist, const std::vector<std::vector< std::pair<int, int> > >& Graph, std::vector<int>& Parents, std::set<std::pair<int, int> >& Q) {
    Q.erase( {MinDist[Vertex], Vertex} );

    for(int i = 0; i < Graph[Vertex].size(); i++) {
        int Vertex2 = Graph[Vertex][i].first;
        int Cost = Graph[Vertex][i].second;
        if( MinDist[Vertex] + Cost < MinDist[Vertex2]) {
            Q.erase( {MinDist[Vertex2], Vertex2} );
            MinDist[Vertex2] = MinDist[Vertex] + Cost;
            Parents[Vertex2] = Vertex;
            Q.insert( {MinDist[Vertex2], Vertex2} );
        }
    }

    if(!Q.empty()) {
        Dekstra(Q.begin()->second, MinDist, Graph, Parents, Q);
    }
}

std::vector<int> RescuePath(std::vector<int>& Parents, int Start, int End) {
    std::vector<int> Result;

    for(int i = End; Parents[i] != -1; i = Parents[i]) {
        Result.push_back(i);
    }
    Result.push_back(Start);

    std::reverse(Result.begin(), Result.end());

    return Result;
}

void ReverseEdges(std::vector< std::vector< std::pair<int, int> > >& Graph, const std::vector<int>& Path) {
    for(int i = 0; i + 1 < Path.size(); i++) {
        int V1 = Path[i];
        int V2 = Path[i + 1];

        auto iter = std::find(Graph[V1].begin(), Graph[V1].end(), std::make_pair(V2, 0));
        Graph[V1].erase(iter);
        Graph[V2].push_back({V1, 0});
    }
}

void SetNewWeights(std::vector< std::vector< std::pair<int, int> > >& Graph, const std::vector<int>& MinDist) {
    for(int i = 0; i < Graph.size(); i++) {
        int V1 = i;
        for(int j = 0; j < Graph[i].size(); j++) {
            int V2 = Graph[V1][j].first;

            Graph[V1][j].second += MinDist[V1] - MinDist[V2];
        }
    }
}

std::vector< std::pair<int, int> > DeleteSameEdges(const std::vector<int>& P1, const std::vector<int>& P2) {
    std::vector< std::pair<int, int> > P1Reversed, NewP1;
    for(int i = 0; i + 1 < P1.size(); i++) {
        P1Reversed.push_back({P1[i + 1], P1[i]});
        NewP1.push_back({P1[i], P1[i + 1]});
    }

    std::vector< std::pair<int, int> > P2Reversed, NewP2;
    for(int i = 0; i + 1 < P2.size(); i++) {
        P2Reversed.push_back({P2[i + 1], P2[i]});
        NewP2.push_back({P2[i], P2[i + 1]});
    }

    std::sort(P1Reversed.begin(), P1Reversed.end());
    std::sort(P2Reversed.begin(), P2Reversed.end());
    std::sort(NewP1.begin(), NewP1.end());
    std::sort(NewP2.begin(), NewP2.end());

    std::vector< std::pair<int, int> > NewGraph(NewP1.size() + NewP2.size());
    std::vector< std::pair<int, int> >::iterator it = std::set_union(NewP1.begin(), NewP1.end(), NewP2.begin(), NewP2.end(), NewGraph.begin());
    NewGraph.resize((it - NewGraph.begin()));

    std::vector< std::pair<int, int> > Intersect1(NewP1.size());
    it = std::set_intersection(NewP1.begin(), NewP1.end(), P2Reversed.begin(), P2Reversed.end(), Intersect1.begin());
    Intersect1.resize(it - Intersect1.begin());

    std::vector< std::pair<int, int> > Intersect2(NewP2.size());
    it = std::set_intersection(NewP2.begin(), NewP2.end(), P1Reversed.begin(), P1Reversed.end(), Intersect2.begin());
    Intersect2.resize(it - Intersect2.begin());

    std::vector< std::pair<int, int> > IntersectUnion(Intersect1.size() + Intersect2.size());
    it = std::set_union(Intersect1.begin(), Intersect1.end(), Intersect2.begin(), Intersect2.end(), IntersectUnion.begin());
    IntersectUnion.resize(it - IntersectUnion.begin());

    std::vector< std::pair<int, int> > Result(NewGraph.size());
    it = std::set_difference(NewGraph.begin(), NewGraph.end(), IntersectUnion.begin(), IntersectUnion.end(), Result.begin());
    Result.resize(it - Result.begin());

    return Result;
}

void PrintAns(const std::vector< std::pair<int, int> >& Graph, int StartVertex, int EndVertex, int MaxN, std::ostream& ostr) {
    std::vector< std::vector< std::pair<int, bool> > > NewG(MaxN);
	ostr << "YES\n";	

    for(int i = 0; i < Graph.size(); i++) {
        int V1 = Graph[i].first;
        int V2 = Graph[i].second;
        NewG[V1].push_back({V2, false});
    }

    int V = StartVertex;
    while(V != EndVertex) {
        ostr << V + 1 << ' ';
        if(NewG[V].size() == 1) {
            NewG[V][0].second = true;
            V = NewG[V][0].first;
        } else if(!NewG[V][0].second) {
            NewG[V][0].second = true;
            V = NewG[V][0].first;
        } else {
            NewG[V][1].second = true;
            V = NewG[V][1].first;
        }
    }
    ostr << EndVertex + 1 << '\n';

    V = StartVertex;
    while(V != EndVertex) {
        ostr << V + 1 << ' ';
        if(NewG[V].size() == 1) {
            NewG[V][0].second = true;
            V = NewG[V][0].first;
        } else if(!NewG[V][0].second) {
            NewG[V][0].second = true;
            V = NewG[V][0].first;
        } else {
            NewG[V][1].second = true;
            V = NewG[V][1].first;
        }
    }
    ostr << EndVertex + 1;
}

void SolveTask(std::ostream& ostr, std::istream& istr) {
	const int Inf = 1e9;

    int NumOfVertices;
    int NumOfEdges;
    int StartVertex;
    int EndVertex;

    istr >> NumOfVertices >> NumOfEdges >> StartVertex >> EndVertex;
    --StartVertex;
    --EndVertex;

    std::vector< std::vector< std::pair<int, int> > > Graph(NumOfVertices);
    std::vector<int> MinDist(NumOfVertices, Inf);
    std::vector<int> Parents(NumOfVertices, -1);
    std::set< std::pair<int, int> > Q;
    MinDist[StartVertex] = 0;

    for(int i = 0; i < NumOfEdges; i++) {
        int V1;
        int V2;
        istr >> V1 >> V2;
        --V1;
        --V2;

        Graph[V1].push_back({V2, 1});
    }

    Q.insert(std::make_pair(MinDist[StartVertex], StartVertex));
    Dekstra(StartVertex, MinDist, Graph, Parents, Q);

    if(MinDist[EndVertex] == Inf) {
        ostr << "NO";
        return;
    }

    SetNewWeights(Graph, MinDist);
    std::vector<int> P1 = RescuePath(Parents, StartVertex, EndVertex);

    ReverseEdges(Graph, P1);

    Q.clear();
    MinDist.assign(NumOfVertices, Inf);
    Parents.assign(NumOfVertices, -1);
    MinDist[StartVertex] = 0;
    Q.insert({MinDist[StartVertex], StartVertex});
    Dekstra(StartVertex, MinDist, Graph, Parents, Q);

    if(MinDist[EndVertex] == Inf) {
        ostr << "NO";
        return;
    }

    std::vector<int> P2 = RescuePath(Parents, StartVertex, EndVertex);

    std::vector< std::pair<int, int> > ResultGraph = DeleteSameEdges(P1, P2);
    PrintAns(ResultGraph, StartVertex, EndVertex, NumOfVertices, ostr);
}
