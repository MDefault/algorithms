#include <algorithm>
#include <iostream>
#include <vector>

enum TColorsType {
	CT_CURRENT,
	CT_UNCHECKED,
	CT_CHECKED
};

// проверка графа на цикличность
bool CheckCyclic( int vertex, int parent, const std::vector< std::vector<int> >& graph, std::vector<int>& colors ) {
    colors[vertex] = TColorsType::CT_CURRENT;

    for( int i = 0; i < graph[vertex].size(); i++ ) {
        int to = graph[vertex][i];
        if( colors[to] == TColorsType::CT_UNCHECKED ) {
            if( CheckCyclic(to, vertex, graph, colors) ) {
                return true;
	    }
        } else if( colors[to] == TColorsType::CT_CURRENT && to != parent ) {
            return true;
        }
    }

    colors[vertex] = TColorsType::CT_CHECKED;

    return false;
}

// топологическая сортировка
void TopSortBasic( int vertex, std::vector<bool>& used, const std::vector< std::vector<int> >& graph, std::vector<int>& order ) {
    used[vertex] = true;

    for( int i = 0; i < graph[vertex].size(); i++ ) {
        if( !used[graph[vertex][i]] ) {
            TopSortBasic( graph[vertex][i], used, graph, order );
	}
    }

    order.push_back(vertex);
}

bool TopSort( const std::vector< std::vector<int> >& graph, std::vector<int>& result ) {
	int numOfVertices = graph.size();
	std::vector<bool> used( graph.size(), false );
	std::vector<int> colors( numOfVertices, TColorsType::CT_UNCHECKED );

	for( int i = 0; i < numOfVertices; i++ ) {
		if( colors[i] == TColorsType::CT_UNCHECKED && CheckCyclic(i, -1, graph, colors) ) 
		{
			return false;
		}
	}

	for (int i = 0; i < numOfVertices; i++ ) {
		if( !used[i] ) {
			TopSortBasic(i, used, graph, result);
		}
	}

	std::reverse(result.begin(), result.end());

	return true;
}

void ReturnTrueAnswer( const std::vector<int>& order ) {
	std::cout << "YES" << std::endl;
	for( size_t i = 0; i < order.size(); i++ ) {
		std::cout << order[i] << ' ';
	}
}

void ReturnFalseAnswer() {
	std::cout << "NO" << std::endl;
}

int main() {
    int numOfVertices;
    int numOfEdges;
    std::cin >> numOfVertices >> numOfEdges;
    std::vector< std::vector<int> > graph(numOfVertices);
    std::vector<int> order;

    for( int i = 0; i < numOfEdges; i++ ) {
        int vertex1;
	int vertex2;
        std::cin >> vertex1 >> vertex2;
        graph[vertex1].push_back(vertex2);
    }
    
    ( !TopSort(graph, order) ? ReturnFalseAnswer() : ReturnTrueAnswer(order) );

    return 0;
}
