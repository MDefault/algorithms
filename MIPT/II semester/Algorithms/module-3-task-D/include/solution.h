#pragma once

#include <iostream>
#include <bits/stdc++.h>

bool Kunh(int vertex, std::vector<int>& used, const std::vector< std::vector<int> >& graph, std::vector<int>& maxMatching) {
	if(used[vertex]) {
		return false;
	}
	
    used[vertex] = true;
    for(int i = 0; i < graph[vertex].size(); i++) {
        int to = graph[vertex][i];
        if (maxMatching[to] == -1 || Kunh(maxMatching[to], used, graph, maxMatching)) {
            maxMatching[to] = vertex;
            return true;
        }
    }
    return false;
}

void BuildGraph(std::vector< std::vector<int> >& graph, const std::vector<std::string>& Data, const std::map<std::pair<int, int>, int>& Table) {
	int Rows = Data.size();
	int Cols = Data.empty() ? 0 : Data[0].size();
	
	for(int i = 0; i < Rows; i++) {
        for(int j = 0; j < Cols; j++) {
            if((i + j) % 2 != 0 || Data[i][j] != '*') {
                continue;
            }

            int V = Table.at(std::make_pair(i, j));
            if(j + 1 < Cols && Data[i][j + 1] == '*') {
                graph[V].push_back(Table.at({i, j + 1}));
            }

            if(j - 1 >= 0 && Data[i][j - 1] == '*') {
                graph[V].push_back(Table.at({i, j - 1}));
            }

            if(i - 1 >= 0 && Data[i - 1][j] == '*') {
                graph[V].push_back(Table.at({i - 1, j}));
            }

            if(i + 1 < Rows && Data[i + 1][j] == '*') {
                graph[V].push_back(Table.at({i + 1, j}));
            }
        }
    }
}

int ProcessKunh(const std::map<std::pair<int, int>, int>& Table, const std::vector<std::string>& Data, int n, int k) {
	std::vector< std::vector<int> > Graph(k);
	std::vector<int> maxMatching(n, -1);
	std::vector<int> used(k);

    BuildGraph(Graph, Data, Table);

    for(int i = 0; i < k; i++) {
        used.assign(k, false);
        Kunh(i, used, Graph, maxMatching);
    }

    int Count = 0;
    for(int i = 0; i < n; i++) {
        if(maxMatching[i] != -1) {
            ++Count;
        }
    }
    
    return Count;
}

void SolveTask(std::ostream& ostr, std::istream& istr) {
	int Rows;
    int Cols;
    int A;
    int B;
    istr >> Rows >> Cols >> A >> B;

    std::map< std::pair<int, int>, int > Table;
    std::vector<std::string> Data;
    int k = 0;
    int n = 0;

    for(int i = 0; i < Rows; i++) {
        std::string tmp;
        istr >> tmp;
        Data.push_back(tmp);

        for(int j = 0; j < tmp.size(); j++) {
            if(tmp[j] == '*') {
                if((i + j) % 2 == 0) {
                    Table[std::make_pair(i, j)] = k;
                    ++k;
                } else {
                    Table[std::make_pair(i, j)] = n;
                    ++n;
                }
            }
        }
    }

    if(2 * B <= A) {
        ostr << (n + k) * B;
        return;
    }
    
	int Count = ProcessKunh(Table, Data, n, k);

    ostr << Count * A + (n + k - Count * 2) * B;
}
