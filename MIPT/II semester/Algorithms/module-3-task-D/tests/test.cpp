#define CATCH_CONFIG_MAIN

#include <iostream>
#include <sstream>
#include <string>
#include <catch2/catch.hpp>
#include "../include/solution.h"

TEST_CASE("ANSWER_HOLDER", "TEST1") {
    std::ostringstream ostr;
    std::istringstream istr("2 3 3 2\n.**\n.*.");
    SolveTask(ostr, istr);
    
    REQUIRE(ostr.str() == "5");
}

TEST_CASE("ANSWER_HOLDER2", "TEST2") {
	std::ostringstream ostr;
    std::istringstream istr("5 6 3 2\n...*.*\n**..*.\n......\n..*...\n.**.**");
    SolveTask(ostr, istr);
    
    REQUIRE(ostr.str() == "17");
}

TEST_CASE("ANSWER_HOLDER3", "TEST3") {
	std::ostringstream ostr;
    std::istringstream istr("2 3 4 1\n.**\n*..");
    SolveTask(ostr, istr);
    
    REQUIRE(ostr.str() == "3");
}
