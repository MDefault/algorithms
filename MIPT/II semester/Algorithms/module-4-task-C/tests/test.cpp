#define CATCH_CONFIG_MAIN

#include <iostream>
#include <sstream>
#include <string>
#include <catch2/catch.hpp>
#include "../include/solution.h"

TEST_CASE("ANSWER_HOLDER", "TEST1") {
    std::ostringstream ostr;
    std::istringstream istr("5\n7 40 3\n54 90 255\n44 230 8\n33 57 132\n17 8 5\n2\n0 3 100 40 41 2 4\n2 4 0 200 57 1 3");
    SolveTask(ostr, istr);
    
    REQUIRE(ostr.str() == "30 181 ");
}

TEST_CASE("ANSWER_HOLDER2", "TEST2") {
	std::ostringstream ostr;
    std::istringstream istr("4\n89 42 4\n72 91 2\n111 93 255\n78 2 3\n3 \n1 2 56 82 94 1 2\n1 3 1 1 1 1 3\n0 3 89 66 4 1 2");
    SolveTask(ostr, istr);
    
    REQUIRE(ostr.str() == "232 3 159 ");
}

TEST_CASE("ANSWER_HOLDER3", "TEST3") {
	std::ostringstream ostr;
    std::istringstream istr("5\n1 3 5\n8 9 37\n93 14 18\n192 44 254\n78 94 0\n2\n1 2 56 82 41 3 4\n0 3 12 22 132 1 4");
    SolveTask(ostr, istr);
    
    REQUIRE(ostr.str() == "172 166 ");
}
