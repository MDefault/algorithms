#include <iostream>
#include <vector>

struct Color {
    int R;
    int G;
    int B;

    Color(int R = -1, int G = -1, int B = -1);

    bool operator==(const Color& c);
    bool operator!=(const Color& c);
    
    int Sum() const;
};

Color::Color(int R, int G, int B)
    : R(R), G(G), B(B)
{
}

bool Color::operator==(const Color &c) {
    return R == c.R && G == c.G && B == c.B;
}

bool Color::operator!=(const Color &c) {
    return !(*this == c);
}

int Color::Sum() const {
    return R + G + B;
}

struct TaskType {
    Color darkest;
    Color painted;

    TaskType(Color clr = Color(), Color pnt = Color());
};

TaskType::TaskType(Color clr, Color pnt)
    : darkest(clr),
      painted(pnt)
{
}

TaskType CalcFunc(TaskType a, TaskType b) {
    Color darkestA = (a.painted == Color()) ? a.darkest : a.painted;
    Color darkestB = (b.painted == Color()) ? b.darkest : b.painted;

    if(darkestA.Sum() < darkestB.Sum()) {
        return TaskType(darkestA, Color());
    }

    return TaskType(darkestB, Color());
}

void BuildSegTree(std::vector<TaskType>& segTree, const std::vector<TaskType>& sourceArr, int segVertex, int leftBorder, int rightBorder) {
    if(leftBorder == rightBorder) {
        segTree[segVertex] = sourceArr[leftBorder];
    } else {
        int middleBorder = (leftBorder + rightBorder) / 2;
        BuildSegTree(segTree, sourceArr, segVertex * 2, leftBorder, middleBorder);
        BuildSegTree(segTree, sourceArr, segVertex * 2 + 1, middleBorder + 1, rightBorder);
        segTree[segVertex] = CalcFunc(segTree[segVertex * 2], segTree[segVertex * 2 + 1]);
    }
}

void PushColor(std::vector<TaskType>& segTree, int segVertex, bool reset) {
    Color curr = segTree[segVertex].painted;

    if(curr != Color()) {
        segTree[segVertex * 2].painted = curr;
        segTree[segVertex * 2 + 1].painted = curr;
        if(reset) {
            segTree[segVertex].painted = Color();
        }
    }
}

TaskType QuerySegTree(std::vector<TaskType>& segTree, int segVertex, int leftBorder, int rightBorder, int queryLeft, int queryRight) {
    if(leftBorder == queryLeft && rightBorder == queryRight) {
        TaskType curr = segTree[segVertex];
        return TaskType((curr.painted == Color()) ? curr.darkest : curr.painted, Color());
    }

    PushColor(segTree, segVertex, false);

    int middleBorder = (leftBorder + rightBorder) / 2;

    if(queryRight <= middleBorder) {
        return QuerySegTree(segTree, segVertex * 2, leftBorder, middleBorder, queryLeft, queryRight);
    } else if(queryLeft >= middleBorder + 1) {
        return QuerySegTree(segTree, segVertex * 2 + 1, middleBorder + 1, rightBorder, queryLeft, queryRight);
    }

    return CalcFunc(QuerySegTree(segTree, segVertex * 2, leftBorder, middleBorder, queryLeft, middleBorder),
                    QuerySegTree(segTree, segVertex * 2 + 1, middleBorder + 1, rightBorder, middleBorder + 1, queryRight));
}

void UpdateSegTree(std::vector<TaskType>& segTree, int segVertex, int leftBorder, int rightBorder, int queryLeft, int queryRight, Color value) {
    if(leftBorder == queryLeft && queryRight == rightBorder) {
        segTree[segVertex].painted = value;
    } else {
        PushColor(segTree, segVertex, true);

        int middleBorder = (leftBorder + rightBorder) / 2;
        if(queryRight <= middleBorder) {
            UpdateSegTree(segTree, segVertex * 2, leftBorder, middleBorder, queryLeft, queryRight, value);
        } else if(queryLeft >= middleBorder + 1) {
            UpdateSegTree(segTree, segVertex * 2 + 1, middleBorder + 1, rightBorder, queryLeft, queryRight, value);
        } else {
            UpdateSegTree(segTree, segVertex * 2, leftBorder, middleBorder, queryLeft, middleBorder, value);
            UpdateSegTree(segTree, segVertex * 2 + 1, middleBorder + 1, rightBorder, middleBorder + 1, queryRight, value);
        }

        segTree[segVertex] = CalcFunc(segTree[segVertex * 2], segTree[segVertex * 2 + 1]);
    }
}

void SolveTask(std::ostream& ostr, std::istream& istr) {
    int Len;
    istr >> Len;

    std::vector<TaskType> segTree(Len * 4);

    std::vector<TaskType> sourceTaskType(Len);

    for(int i = 0; i < Len; i++) {
        int R;
        int G;
        int B;

        istr >> R >> G >> B;
        sourceTaskType[i] = TaskType(Color(R, G, B));
    }

    BuildSegTree(segTree, sourceTaskType, 1, 0, Len - 1);

    int numOfQueries;
    istr >> numOfQueries;

    for(int i = 0; i < numOfQueries; i++) {
        int C;
        int D;
        int R;
        int G;
        int B;
        int E;
        int F;

        istr >> C >> D >> R >> G >> B >> E >> F;

        UpdateSegTree(segTree, 1, 0, Len - 1, C, D, Color(R, G , B));

        ostr << QuerySegTree(segTree, 1, 0, Len - 1, E, F).darkest.Sum() << ' ';
    }
}
