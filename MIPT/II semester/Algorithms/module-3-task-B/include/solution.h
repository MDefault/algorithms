#pragma once

#include <algorithm>
#include <cmath>
#include <iostream>
#include <vector>

void MakeHeap(std::vector<int>& Heaps, std::vector<int>& Size, int Vertex) {
    Heaps[Vertex] = Vertex;
    Size[Vertex] = 1;
}

bool IsConnected(std::vector<int>& Heaps, int Vertex1, int Vertex2) {
    int p, q;

    for(p = Vertex1; p != Heaps[p]; p = Heaps[p]) {
        Heaps[p] = Heaps[Heaps[p]];
    }

    for(q = Vertex2; q != Heaps[q]; q = Heaps[q]) {
        Heaps[q] = Heaps[Heaps[q]];
    }

    return p == q;
}

void Connect(std::vector<int>& Heaps, std::vector<int>& Size, int Vertex1, int Vertex2) {
    int p, q;

    for(p = Vertex1; p != Heaps[p]; p = Heaps[p]) {
        Heaps[p] = Heaps[Heaps[p]];
    }

    for(q = Vertex2; q != Heaps[q]; q = Heaps[q]) {
        Heaps[q] = Heaps[Heaps[q]];
    }

    if(p == q) {
        return;
    }

    if(Size[p] > Size[q]) {
        Heaps[q] = p; Size[p] += Size[q];
    } else {
        Heaps[p] = q; Size[q] += Size[p];
    }
}

void SolveTask(std::ostream& ostr, std::istream& istr) {
	int NumOfVertices;
    int NumOfEdges;
    std::vector<int> Heaps;
    std::vector<int> Size;
    std::vector< std::pair<int, std::pair<int, int> > > Edges;

    istr >> NumOfVertices >> NumOfEdges;
    Heaps.resize(NumOfVertices, 0);
    Size.resize(NumOfVertices);
    Edges.resize(NumOfEdges);

    for(int i = 0; i < NumOfVertices; i++) {
        MakeHeap(Heaps, Size, i);
    }

    for(int i = 0; i < NumOfEdges; i++) {
        int V1;
        int V2;
        int Cost;
        istr >> V1 >> V2 >> Cost;
        --V1, --V2;

        Edges[i].first = Cost;
        Edges[i].second = std::make_pair(V1, V2);
    }

    std::sort(Edges.begin(), Edges.end());

    int Answer = 0;

    for(int i = 0; i < NumOfEdges; i++) {
        int C = Edges[i].first;
        int V1 = Edges[i].second.first;
        int V2 = Edges[i].second.second;

        if(!IsConnected(Heaps, V1, V2)) {
            Answer += C;
            Connect(Heaps, Size, V1, V2);
        }
    }

    ostr << Answer;
}
