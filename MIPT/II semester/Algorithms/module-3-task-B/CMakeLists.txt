cmake_minimum_required(VERSION 2.8)

project("CompileProject")

include_directories(include)

find_package(Catch2 REQUIRED)
add_executable(TestExec tests/test.cpp)
add_executable(MainExec src/main.cpp)
target_link_libraries(TestExec Catch2::Catch2)

include(Catch)
include(CTest)
catch_discover_tests(TestExec)
enable_testing()
