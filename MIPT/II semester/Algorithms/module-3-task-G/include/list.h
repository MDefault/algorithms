#ifndef LIST2_H
#define LIST2_H

#include <utility>
#include <algorithm>

template <typename T>
class ListIterator;
template <typename T>
class ConstListIterator;

template <typename T>
class List {
public:
    typedef T value_type;
    typedef ListIterator<value_type> iterator;
    typedef std::reverse_iterator<iterator> reverse_iterator;
    typedef ConstListIterator<value_type> const_iterator;
    typedef std::reverse_iterator<const_iterator> const_reverse_iterator;
    typedef std::size_t size_type;
    typedef std::ptrdiff_t difference_type;
    typedef value_type& reference;
    typedef const value_type& const_reference;
    typedef value_type * pointer;
    typedef const pointer const_pointer;
public:
    List();
    List(std::size_t, const T& value = T());
    List(const List&);
    List(List&&);
    ~List();

    List& operator=(const List&);
    List& operator=(List&&);

    std::size_t size() const {
        return len;
    }

    const T& front() const {
        return *head->next->value;
    }

    T& front() {
        return *head->next->value;
    }

    const T& back() const {
        return *tail->prev->value;
    }

    T& back() {
        return *tail->prev->value;
    }

    void clear();

    bool empty() const {
        return len == 0;
    }

    iterator begin() {
        return iterator(head->next);
    }

    const_iterator cbegin() const {
        return const_iterator(head->next);
    }

    iterator end() {
        return iterator(tail);
    }

    const_iterator cend() const {
        return const_iterator(tail);
    }

    reverse_iterator rbegin() {
        return reverse_iterator(end());
    }

    const_reverse_iterator crbegin() const {
        return const_reverse_iterator(cend());
    }

    reverse_iterator rend() {
        return reverse_iterator(begin());
    }

    const_reverse_iterator crend() const {
        return const_reverse_iterator(cbegin());
    }

    iterator insert(const_iterator, const T&);
    iterator insert(const_iterator, T&&);
    template <typename InputIt>
    iterator insert(const_iterator, InputIt, InputIt);

    iterator erase(const_iterator);
    iterator erase(const_iterator, const_iterator);

    void push_back(const T&);
    void push_back(T&&);
    void pop_back();
    void push_front(const T&);
    void push_front(T&&);
    void pop_front();

    template <typename... Args>
    iterator emplace(iterator, Args&&...);

    template<typename... Args>
    void emplace_back(Args&&...);

    template<typename... Args>
    void emplace_front(Args&&...);

    void reverse();

    void unique();
private:
    struct Node {
        Node * next;
        Node * prev;
        List<T>::pointer value;

        Node(List<T>::pointer p = nullptr, Node * _next = nullptr, Node * _prev = nullptr)
            : next(_next), prev(_prev), value(p)
        {
        }
    };
private:
    friend class ListIterator<value_type>;
    friend class ConstListIterator<value_type>;
private:
    // удаляет старый диапазон и копирует новый, [)
    template <typename InputIt>
    void Copy(InputIt, InputIt);
    // связывает два указателя
    void Connect(Node * first, Node * second) {
        first->next = second;
        second->prev = first;
    }
    // вставляет новое значение и связывает; возвращает созданный объект
    template <typename M>
    Node * Insert(Node * first, Node * second, M&& value) {
        Node * current = new Node(new T(std::forward<M>(value)), second, first);
        Connect(first, current);
        Connect(current, second);
        return current;
    }
    // удаляет элемент и связывает своих соседей, возвращает следующий за ним элемент
    Node * DeleteNode(Node * node) {
        Node * prev = node->prev;
        Node * next = node->next;
        delete node->value;
        delete node;
        Connect(prev, next);
        return next;
    }
    // рекурсивная версия вставки в конец
    template <typename M>
    void EmplaceBack(M&& value) {
        push_back(std::forward<M>(value));
    }

    template <typename M, typename... Args>
    void EmplaceBack(M&& arg, Args&&... args) {
        push_back(std::forward<M>(arg));
        EmplaceBack(std::forward<Args>(args)...);
    }
private:
    Node * head;
    Node * tail;
    size_type len;
};

template <typename Type>
class ListIterator : public std::iterator<std::bidirectional_iterator_tag, Type> {
public:
    ListIterator() = default;
    ListIterator(const ListIterator<Type>&) = default;
    ListIterator(const ConstListIterator<Type>& l) {
        value = const_cast<typename List<Type>::Node*>(l.value);
    }
    ListIterator(ListIterator<Type>&&) = default;
    ~ListIterator() = default;

    ListIterator<Type>& operator=(const ListIterator<Type>&) = default;
    ListIterator<Type>& operator=(ListIterator<Type>&&) = default;

    bool operator!=(const ListIterator<Type>& l) const {
        return !this->operator==(l);
    }

    bool operator==(const ListIterator<Type>& l) const {
        return l.value == value;
    }

    Type& operator*() const {
        if(value == nullptr || value->value == nullptr) {
            throw std::exception();
        }
        return  *value->value;
    }

    ListIterator<Type>& operator++() {
        if(value->next) {
            value = value->next;
        }
        return *this;
    }

    ListIterator<Type> operator++(int) {
        ListIterator<Type> temp = *this;
        if(value->next) {
            value = value->next;
        }
        return temp;
    }

    ListIterator<Type>& operator--() {
        if(value->prev) {
            value = value->prev;
        }
        return *this;
    }

    ListIterator<Type> operator--(int) {
        ListIterator<Type> temp = *this;
        if(value->prev) {
            value = value->prev;
        }
        return temp;
    }
private:
    friend class List<Type>;
    friend class ConstListIterator<Type>;
private:
    typename List<Type>::Node * value;
private:
    ListIterator(typename List<Type>::Node * ptr)
        : value(ptr)
    {
    }
};

template <typename Type>
class ConstListIterator : public std::iterator<std::bidirectional_iterator_tag, Type> {
public:
    ConstListIterator() = default;
    ConstListIterator(const ConstListIterator<Type>&) = default;
    ConstListIterator(const ListIterator<Type>& l) {
        value = l.value;
    }
    ConstListIterator(ConstListIterator<Type>&&) = default;
    ~ConstListIterator() = default;

    ConstListIterator<Type>& operator=(const ConstListIterator<Type>&) = default;
    ConstListIterator<Type>& operator=(ConstListIterator<Type>&&) = default;

    bool operator!=(const ConstListIterator<Type>& l) const {
        return !this->operator==(l);
    }

    bool operator==(const ConstListIterator<Type>& l) const {
        return l.value == value;
    }

    const Type& operator*() const {
        if(value == nullptr || value->value == nullptr) {
            throw std::exception();
        }
        return *value->value;
    }

    ConstListIterator<Type>& operator++() {
        if(value->next) {
            value = value->next;
        }
        return *this;
    }

    ConstListIterator<Type> operator++(int) {
        ConstListIterator<Type> temp = *this;
        if(value->next) {
            value = value->next;
        }
        return temp;
    }

    ConstListIterator<Type>& operator--() {
        if(value->prev) {
            value = value->prev;
        }
        return *this;
    }

    ConstListIterator<Type> operator--(int) {
        ConstListIterator<Type> temp = *this;
        if(value->prev) {
            value = value->prev;
        }
        return temp;
    }
private:
    friend class List<Type>;
    friend class ListIterator<Type>;
private:
    const typename List<Type>::Node * value;
private:
    ConstListIterator(const typename List<Type>::Node * ptr)
        : value(ptr)
    {
    }
};

template <typename T>
List<T>::List()
    : head(new Node),
      tail(new Node),
      len(0)
{
    Connect(head, tail);
}

template <typename T>
List<T>::List(std::size_t sz, const T& value)
    : List()
{
    for(size_type i = 0; i < sz; i++) {
        push_back(value);
    }
}

template <typename T>
List<T>::List(const List<T>& l)
    : List()
{
    Copy(l.cbegin(), l.cend());
}

template <typename T>
List<T>::List(List&& l)
    : head(std::move(l.head)),
      tail(std::move(l.tail)),
      len(l.len)
{
    l.head = nullptr;
    l.tail = nullptr;
    l.len = 0;
}

template <typename T>
List<T>::~List() {
    clear();
    delete head;
    delete tail;

    head = tail = nullptr;
    len = 0;
}

template <typename T>
List<T>& List<T>::operator=(const List<T>& l) {
    if(this == &l) {
        return *this;
    }

    Copy(l.cbegin(), l.cend());

    return *this;
}

template <typename T>
List<T>& List<T>::operator=(List<T>&& l) {
    if(this == &l) {
        return *this;
    }

    clear();
    delete head;
    delete tail;

    head = l.head;
    tail = l.tail;
    len = l.len;

    l.head = nullptr;
    l.tail = nullptr;
    l.len = 0;

    return *this;
}

template <typename T>
void List<T>::clear() {
    if(head == nullptr || tail == nullptr) {
        return;
    }

    Node * ptr = head->next;

    while(ptr != tail) {
        if(ptr == nullptr) {
            throw 1;
        }
        Node * next = ptr->next;
        delete ptr->value;
        delete ptr;
        ptr = next;
    }

    Connect(head, tail);
    len = 0;
}

template <typename T>
typename List<T>::iterator List<T>::insert(List<T>::const_iterator iter, const T& value) {
    ++len;
    Node * prev = iterator(iter).value->prev;
    Node * next = iterator(iter).value;

    return iterator(Insert(prev, next, value));
}

template <typename T>
typename List<T>::iterator List<T>::insert(List<T>::const_iterator iter, T&& value) {
    ++len;
    Node * prev = iterator(iter).value->prev;
    Node * next = iterator(iter).value;

    return iterator(Insert(prev, next, std::move(value)));
}

template <typename T>
template<typename InputIt>
typename List<T>::iterator List<T>::insert(List<T>::const_iterator iter, InputIt first, InputIt second) {
    if(first == second) {
        return iter;
    }

    List<T> temp;
    Node * prev = iterator(iter).value->prev;
    Node * next = iterator(iter).value;

    temp.Copy(first, second);
    len += temp.len;
    Connect(prev, temp.head->next);
    Connect(temp.tail->prev, next);
    Connect(temp.head, temp.tail);
    temp.len = 0;

    return iterator(prev->next);
}

template <typename T>
typename List<T>::iterator List<T>::erase(List<T>::const_iterator iter) {
    if(iter == end()) {
        return begin();
    }

    --len;

    return iterator(DeleteNode(iterator(iter).value));
}

template <typename T>
typename List<T>::iterator List<T>::erase(List<T>::const_iterator first, List<T>::const_iterator second) {
    if(first == second) {
        return iterator(first);
    }

    for(auto i = first; i != second; i++) {
        --len;
    }

    Node * prev = iterator(first).value->prev;
    Node * next = iterator(second).value;

    List<T> temp;
    temp.Connect(temp.head, prev->next);
    temp.Connect(next->prev, temp.tail);
    temp.clear();
    Connect(prev, next);

    return iterator(second);
}

template <typename T>
void List<T>::push_back(const T& value) {
    Insert(tail->prev, tail, value);
    ++len;
}

template <typename T>
void List<T>::push_back(T&& value) {
    Insert(tail->prev, tail, std::move(value));
    ++len;
}

template <typename T>
void List<T>::pop_back() {
    if(!empty()) {
        DeleteNode(tail->prev);
        --len;
    }
}

template <typename T>
void List<T>::push_front(const T& value) {
    Insert(head, head->next, value);
    ++len;
}

template <typename T>
void List<T>::push_front(T&& value) {
    Insert(head, head->next, std::move(value));
    ++len;
}

template <typename T>
void List<T>::pop_front() {
    if(!empty()) {
        DeleteNode(head->next);
        --len;
    }
}

template  <typename T>
template <typename... Args>
typename List<T>::iterator List<T>::emplace(List<T>::iterator iter, Args&&... args) {
    if(sizeof...(args) == 0) {
        return iter;
    }

    if(empty()) {
        EmplaceBack(std::forward<Args>(args)...);
        return begin();
    }

    List<T> temp;
    temp.EmplaceBack(std::forward<Args>(args)...);
    len += temp.len;

    Node * prev = iter.value->prev;
    Node * next = prev->next;

    Connect(prev, temp.head->next);
    Connect(temp.tail->prev, next);
    Connect(temp.head, temp.tail);

    return iterator(prev->next);
}

template <typename T>
template <typename... Args>
void List<T>::emplace_back(Args&&... args) {
    if(sizeof...(args) == 0) {
        return;
    } else if(empty()) {
        emplace(begin(), std::forward<Args>(args)...);
        return;
    }

    List<T> temp;
    Node * prev = tail->prev;
    Node * next = tail;

    temp.emplace(temp.begin(), std::forward<Args>(args)...);
    len += temp.len;
    Connect(prev, temp.head->next);
    Connect(temp.tail->prev, next);
    Connect(temp.head, temp.tail);
}

template <typename T>
template <typename... Args>
void List<T>::emplace_front(Args&&... args) {
    if(sizeof...(args) == 0) {
        return;
    } else if(empty()) {
        emplace(begin(), std::forward<Args>(args)...);
        return;
    }

    List<T> temp;
    Node * prev = head;
    Node * next = head->next;

    temp.emplace(temp.begin(), std::forward<Args>(args)...);
    len += temp.len;
    Connect(prev, temp.head->next);
    Connect(temp.tail->prev, next);
    Connect(temp.head, temp.tail);
}

template <typename T>
void List<T>::reverse() {
    List<T> l;

    for(auto i = rbegin(); i != rend(); i++) {
        l.push_back(*i);
    }

    *this = l;
}

template <typename T>
void List<T>::unique() {
    if(empty()) {
        return;
    }

    List<T> L;
    iterator i = begin();
    iterator j = i;

    L.push_back(*i);

    for(i++; i != end(); i++, j++) {
        if(*j != *i) {
            L.push_back(*i);
        }
    }

    *this = L;
}

template <typename T>
template <typename InputIt>
void List<T>::Copy(InputIt first, InputIt second) {
    clear();
    List<T> temp;

    for(auto i = first; i != second; i++) {
        temp.push_back(*i);
    }

    len = temp.len;
    Connect(head, temp.head->next);
    Connect(temp.tail->prev, tail);
    Connect(temp.head, temp.tail);
}

#endif // LIST2_H
