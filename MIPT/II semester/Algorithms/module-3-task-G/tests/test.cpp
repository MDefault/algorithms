#define CATCH_CONFIG_MAIN

#include <iostream>
#include <sstream>
#include <string>
#include <catch2/catch.hpp>
#include "../include/list.h"

template <typename T>
void print(List<T>& a, std::ostringstream& ostr) {
    for(auto i : a) {
        ostr << i << ' ';
    }
    ostr << std::endl << a.size();
}

TEST_CASE("VALID_CHECKER1", "TEST1") {
	std::ostringstream str;

	List<int> l(10, 3);
    print(l, str);
    
    REQUIRE(str.str() == "3 3 3 3 3 3 3 3 3 3 \n10");
}

TEST_CASE("VALID_CHECKER2", "TEST2") {
	std::ostringstream str;

	List<int> l1(10, 3);
    List<int> l2(l1);
    print(l2, str);
    
    REQUIRE(str.str() == "3 3 3 3 3 3 3 3 3 3 \n10");
}

TEST_CASE("VALID_CHECKER3", "TEST3") {
	std::ostringstream str;

	List<int> l1(List<int>(10, 3));
    print(l1, str);
    
    REQUIRE(str.str() == "3 3 3 3 3 3 3 3 3 3 \n10");
}

TEST_CASE("VALID_CHECKER4", "TEST4") {
	std::ostringstream str;

	List<int> l1;
    List<int> l2(10, 3);
    l1 = l2;
    print(l1, str);
    
    REQUIRE(str.str() == "3 3 3 3 3 3 3 3 3 3 \n10");
}

TEST_CASE("VALID_CHECKER5", "TEST5") {
	std::ostringstream str;

	List<int> l1(List<int>(10, 3));
    print(l1, str);
    
    REQUIRE(str.str() == "3 3 3 3 3 3 3 3 3 3 \n10");
}

TEST_CASE("VALID_CHECKER6", "TEST6") {
	std::ostringstream str;

	List<int> l;
    l.emplace_back(1, 2, 3);
    str << l.front() << ' ' << l.back() << std::endl;
    
    REQUIRE(str.str() == "1 3\n");
}

TEST_CASE("VALID_CHECKER7", "TEST7") {
	std::ostringstream str;

	List<int> l;
    l.insert(l.begin(), 12);
    l.insert(l.begin(), 24);
    l.insert(++l.begin(), 18);
    List<int> l2;
    l2.insert(l2.begin(), l.begin(), l.end());
    print(l2, str);
    l2.insert(++++l2.begin(), l.rbegin(), l.rend());
    print(l2, str);
    
    REQUIRE(str.str() == "24 18 12 \n324 18 12 18 24 12 \n6");
}

TEST_CASE("VALID_CHECKER8", "TEST8") {
	std::ostringstream str;

	List<int> l;
    l.insert(l.begin(), 12);
    l.insert(l.begin(), 24);
    l.insert(++l.begin(), 18);
    l.erase(++l.begin());
    print(l, str);
    l.insert(++l.begin(), 18);
    print(l, str);
    l.erase(++l.begin(), l.end());
    print(l, str);
    
    REQUIRE(str.str() == "24 12 \n224 18 12 \n324 \n1");
}

TEST_CASE("VALID_CHECKER9", "TEST9") {
	std::ostringstream str;

	List<int> l;
    l.push_back(2);
    l.push_back(3);
    l.push_front(1);
    print(l, str);
    l.pop_back();
    l.pop_front();
    print(l, str);
    
    REQUIRE(str.str() == "1 2 3 \n32 \n1");
}
