#define CATCH_CONFIG_MAIN

#include <iostream>
#include <sstream>
#include <string>
#include <catch2/catch.hpp>
#include "../include/solution.h"

TEST_CASE("ANSWER_HOLDER", "TEST1") {
    std::ostringstream ostr;
    std::istringstream istr("5 7 1 3\n1 2 20\n3 4 10\n1 5 37\n1 3 50\n2 3 20\n4 5 92\n1 4 67");
    SolveTask(ostr, istr);
    
    REQUIRE(ostr.str() == "0.36");
}
