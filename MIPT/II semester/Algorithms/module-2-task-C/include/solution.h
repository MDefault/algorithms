#ifndef SOLUTION_H
#define SOLUTION_H

#include <algorithm>
#include <iostream>
#include <map>
#include <set>
#include <vector>

void Deykstra(int vertex, std::vector<long double>& distances, std::set< std::pair<long double, int> >& q, const std::vector< std::vector< std::pair<int, int> > >& graph) {
    q.erase(*q.begin());

    for (size_t i = 0; i < graph[vertex].size(); i++) {
        int vertex2 = graph[vertex][i].first;
        long double weight = static_cast<long double>(graph[vertex][i].second) / 100.0;
        long double newPrice = distances[vertex] + weight - distances[vertex] * weight;

        if (distances[vertex2] > newPrice) {
            q.erase(std::make_pair(distances[vertex2], vertex2));
            distances[vertex2] = newPrice;
            q.insert(std::make_pair(distances[vertex2], vertex2));
        }
    }

    if (!q.empty()) {
        Deykstra((*q.begin()).second, distances, q, graph);
    }
}

void SolveTask(std::ostream& ostr, std::istream& istr) {
    int startVertex;
    int endVertex;
    int numOfVertices;
    int numOfEdges;
    std::vector< std::vector< std::pair<int, int> > > graph;
    std::vector<long double> distances;
    std::set< std::pair<long double, int> > q;

    istr >> numOfVertices >> numOfEdges >> startVertex >> endVertex;
    --startVertex;
    --endVertex;

    graph.resize(numOfVertices);
    distances.resize(numOfVertices, 2.0);

    for (int i = 0; i < numOfEdges; i++) {
        int vertex1;
        int vertex2;
        int p;

        istr >> vertex1 >> vertex2 >> p;
        --vertex1;
        --vertex2;

        graph[vertex1].push_back(std::make_pair(vertex2, p));
        graph[vertex2].push_back(std::make_pair(vertex1, p));
    }

    distances[startVertex] = 0;
    q.insert(std::make_pair(distances[startVertex], startVertex));

    Deykstra(startVertex, distances, q, graph);

    ostr << distances[endVertex];
}

#endif // SOLUTION_H
