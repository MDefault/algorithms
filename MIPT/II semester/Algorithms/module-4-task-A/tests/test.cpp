#define CATCH_CONFIG_MAIN

#include <iostream>
#include <sstream>
#include <string>
#include <catch2/catch.hpp>
#include "../include/solution.h"

TEST_CASE("ANSWER_HOLDER", "TEST1") {
    std::ostringstream ostr;
    std::istringstream istr("10 3\n1 2 3 4 5 6 7 8 9 10\n1 2\n1 10\n2 7");
    SolveTask(ostr, istr);
    
    REQUIRE(ostr.str() == "2\n2\n3\n");
}

TEST_CASE("ANSWER_HOLDER2", "TEST2") {
	std::ostringstream ostr;
    std::istringstream istr("5 3\n2 18 7 6 1\n1 2\n2 3\n4 5");
    SolveTask(ostr, istr);
    
    REQUIRE(ostr.str() == "18\n18\n6\n");
}

TEST_CASE("ANSWER_HOLDER3", "TEST3") {
	std::ostringstream ostr;
    std::istringstream istr("6 4\n9 8 2 -1 4 2\n1 2\n4 6\n2 6\n1 3");
    SolveTask(ostr, istr);
    
    REQUIRE(ostr.str() == "9\n2\n2\n8\n");
}
