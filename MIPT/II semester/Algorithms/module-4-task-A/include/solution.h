#pragma once

#include <iostream>
#include <vector>

struct TaskType {
    int min;
    int max;

    TaskType(int min = -1, int max = -1)
        : min(min),
          max(max)
    {
    }
};

std::vector<std::vector<TaskType>> BuildSparseTable(const std::vector<int>& arr) {
    std::vector<std::vector<TaskType>> result;
    int maxJ = 0;

    while((1 << maxJ) <= arr.size()) {
        ++maxJ;
    }

    result.resize(arr.size(), std::vector<TaskType>(maxJ));

    for(int i = 0; i < result.size(); i++) {
        result[i][0] = TaskType(-1, i);
    }

    for(int i = 0; i + 2 <= result.size(); i++) {
        TaskType d1 = result[i][0];
        TaskType d2 = result[i + 1][0];

        if(arr[d1.max] < arr[d2.max]) {
            result[i][1] = TaskType(d1.max, d2.max);
        } else {
            result[i][1] = TaskType(d2.max, d1.max);
        }
    }

    for(int j = 2; j <= maxJ; j++) {
        for(int i = 0; i + (1 << j) <= result.size(); i++) {
            std::vector<int> temp;
            TaskType res1 = result[i][j - 1];
            TaskType res2 = result[i + (1 << (j - 1))][j - 1];

            if(arr[res1.min] > arr[res2.min]) {
                std::swap(res1, res2);
            }

            if(res1.min == res2.min) {
                if(arr[res1.max] < arr[res2.max]) {
                    result[i][j] = TaskType(res1.min, res1.max);
                } else {
                    result[i][j] = TaskType(res1.min, res2.max);
                }
            } else if(res1.max == res2.min) {
                result[i][j] = TaskType(res1.min, res1.max);
            } else if(res1.max == res2.max) {
                result[i][j] = TaskType(res1.min, res2.min);
            }

            if(arr[res1.max] < arr[res2.min]) {
                result[i][j] = TaskType(res1.min, res1.max);
            } else {
                result[i][j] = TaskType(res1.min, res2.min);
            }
        }
    }

    return result;
}

std::vector<int> MakeFloor(int len) {
    std::vector<int> floor(len + 1);

    floor[1] = 0;

    for(int i = 2; i <= len; i++) {
        floor[i] = floor[i / 2] + 1;
    }

    return floor;
}

int QuerySparseTable(const std::vector<std::vector<TaskType>>& vec, const std::vector<int>& arr, const std::vector<int>& floor, int leftBorder, int rightBorder) {
    int j = floor[rightBorder - leftBorder + 1];

    TaskType res1 = vec[leftBorder][j];
    TaskType res2 = vec[rightBorder - (1 << j) + 1][j];

    if(arr[res1.min] > arr[res2.min]) {
        std::swap(res1, res2);
    }

    if(res1.min == res2.min) {
        return std::min(arr[res1.max], arr[res2.max]);
    } else if(res1.max == res2.min) {
        return arr[res1.max];
    } else if(res1.max == res2.max) {
        return arr[res2.min];
    }

    return std::min(arr[res1.max], arr[res2.min]);
}

void SolveTask(std::ostream& ostr, std::istream& istr) {
    int numOfDigits;
    int numOfQueries;

    istr >> numOfDigits >> numOfQueries;
    std::vector<int> arr(numOfDigits);

    for(int i = 0; i < arr.size(); i++) {
        istr >> arr[i];
    }

    auto sparseTable = BuildSparseTable(arr);
    auto floor = MakeFloor(numOfDigits);

    for(int i = 0; i < numOfQueries; i++) {
        int a1;
        int a2;

        istr >> a1 >> a2;

        ostr << QuerySparseTable(sparseTable, arr, floor, a1 - 1, a2 - 1) << '\n';
    }
}
