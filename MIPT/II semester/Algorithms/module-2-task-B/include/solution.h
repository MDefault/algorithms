#ifndef SOLUTION_H
#define SOLUTION_H

#include <iostream>
#include <queue>
#include <set>
#include <vector>

void BFSSetDistances(const std::vector< std::vector< std::pair<int, int> > >& graph, std::vector<int>& distances, int startVertex) {
    std::queue< std::pair<int, int> > q;
    std::vector<bool> used(graph.size(), false);

    distances[startVertex] = 0;
    q.push(std::make_pair(startVertex, 0));

    while (!q.empty()) {
        int vertex = q.front().first;
        int dist = q.front().second;

        q.pop();

        for (int i = 0; i < graph[vertex].size(); i++) {
            int vertex2 = graph[vertex][i].first;

            if (!used[vertex2]) {
                distances[vertex2] = dist + 1;
                used[vertex2] = true;
                q.push(std::make_pair(vertex2, dist + 1));
            }
        }
    }
}

// <вершина, вес>
int AAlgorithm(const std::vector< std::vector< std::pair<int, int> > >& graph, int startVertex, int endVertex, int k) {
    static const int startValue = -1;

    std::vector<int> bfsDistances(graph.size());
    std::vector<int> dekstraDistances(graph.size(), startValue);
    std::vector<int> priorityDistances(graph.size());
    std::set< std::pair<int, int> > q;

    BFSSetDistances(graph, bfsDistances, startVertex);
    priorityDistances[startVertex] = 0;
    q.insert(std::make_pair(priorityDistances[startVertex], startVertex));
    dekstraDistances[startVertex] = 0;

    if (bfsDistances[endVertex] > k) {
        return -1;
    }

    while (!q.empty()) {
        int current = q.begin()->second;
        q.erase(*q.begin());

        if (current == endVertex) {
            break;
        }

        for (int i = 0; i < graph[current].size(); i++) {
            int next = graph[current][i].first;
            int newCost = dekstraDistances[current] + graph[current][i].second;
            if (dekstraDistances[next] == startValue || newCost < dekstraDistances[next]) {
                int newPriority = newCost + bfsDistances[next];

                dekstraDistances[next] = newCost;
                q.erase(std::make_pair(priorityDistances[next], next));
                priorityDistances[next] = newPriority;
                q.insert(std::make_pair(priorityDistances[next], next));
            }
        }
    }

    return dekstraDistances[endVertex];
}

void SolveTask(std::ostream& ostr, std::istream& istr) {
    int numOfVertices;
    int numOfEdges;
    int k;
    int startVertex;
    int endVertex;
    std::vector< std::vector< std::pair<int, int> > > graph;

    istr >> numOfVertices >> numOfEdges >> k >> startVertex >> endVertex;
    --startVertex;
    --endVertex;

    graph.resize(numOfVertices);

    for (int i = 0; i < numOfEdges; i++) {
        int vertex1;
        int vertex2;
        int cost;

        istr >> vertex1 >> vertex2 >> cost;
        --vertex1;
        --vertex2;

        graph[vertex1].push_back(std::make_pair(vertex2, cost));
    }

    ostr << AAlgorithm(graph, startVertex, endVertex, k);
}

#endif // SOLUTION_H
