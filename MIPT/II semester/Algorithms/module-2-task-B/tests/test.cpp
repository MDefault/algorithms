#define CATCH_CONFIG_MAIN

#include <iostream>
#include <sstream>
#include <string>
#include <catch2/catch.hpp>
#include "../include/solution.h"

TEST_CASE("ANSWER_HOLDER", "TEST1") {
    std::ostringstream ostr;
    std::istringstream istr("5 7 2 4 1\n1 2 6\n5 1 1\n4 1 9\n4 5 3\n4 3 2\n2 5 7\n3 5 1");
    SolveTask(ostr, istr);
    
    REQUIRE(ostr.str() == "4");
}

TEST_CASE("ANSWER_HOLDER2", "TEST2") {
    std::ostringstream ostr;
    std::istringstream istr("3 3 1 1 3\n1 2 4\n2 3 5\n3 1 6");
    SolveTask(ostr, istr);
    
    REQUIRE(ostr.str() == "-1");
}

