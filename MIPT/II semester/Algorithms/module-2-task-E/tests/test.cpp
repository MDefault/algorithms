#define CATCH_CONFIG_MAIN

#include <catch2/catch.hpp>
#include <iostream>
#include <sstream>
#include <string>
#include "../include/solution.h"

TEST_CASE("ANSWER_HOLDER", "TEST1") {
    std::ostringstream ostr;
    std::istringstream istr("3 2 1 10.0\n1 2 1.0 1.0 1.0 1.0\n2 3 1.1 1.0 1.1 1.0");
    SolveTask(ostr, istr);
    
    REQUIRE(ostr.str() == "NO");
}
