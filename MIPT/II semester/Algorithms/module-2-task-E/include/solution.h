#ifndef SOLUTION_H
#define SOLUTION_H
#include <iostream>
#include <map>
#include <set>
#include <string>
#include <vector>

bool FindWay(std::map< std::pair<int, int>, std::pair<double, double> >& table, std::vector< std::vector<int> >& graph, int startVertex, double startCapital) {
    const double InitialPrice = -1e10;

    std::vector<double> MaxValues(graph.size(), InitialPrice);
    std::set<int> CurrentBorder;
    std::set< std::vector<double> > History;

    MaxValues[startVertex] = startCapital;
    CurrentBorder.insert(startVertex);

    while (true) {
        std::set<int> TempBorder;
        History.insert(MaxValues);

        for (auto j = CurrentBorder.begin(); j != CurrentBorder.end(); j++) {
            int Vertex = *j;

            for (int k = 0; k < graph[Vertex].size(); k++) {
                int Vertex2 = graph[Vertex][k];
                std::pair<double, double> Price = table[std::make_pair(Vertex, Vertex2)];
                double NewPrice = (MaxValues[Vertex] - Price.second) * Price.first;

                if(MaxValues[Vertex2] < NewPrice) {
                    MaxValues[Vertex2] = NewPrice;
                    TempBorder.insert(Vertex2);
                }
            }
        }

        if (History.find(MaxValues) != History.end()) {
            break;
        }

        CurrentBorder = TempBorder;
    }

    return MaxValues[startVertex] > startCapital;
}

void SolveTask(std::ostream& ostr, std::istream& istr) {
    int numOfVertices;
    int numOfEdges;
    int startVertex;
    double startCapital;
    std::vector< std::vector<int> > graph;
    std::map< std::pair<int, int>, std::pair<double, double> > table;

    istr >> numOfVertices >> numOfEdges >> startVertex >> startCapital;
    --startVertex;

    graph.resize(numOfVertices);

    for (int i = 0; i < numOfEdges; i++) {
        int a;
        int b;
        double rab;
        double cab;
        double rba;
        double cba;

        istr >> a >> b >> rab >> cab >> rba >> cba;
        --a;
        --b;

        graph[a].push_back(b);
        table[std::make_pair(a, b)] = std::make_pair(rab, cab);
        graph[b].push_back(a);
        table[std::make_pair(b, a)] = std::make_pair(rba, cba);
    }

    ostr << (FindWay(table, graph, startVertex, startCapital) ? "YES" : "NO");
}


#endif // SOLUTION_H
