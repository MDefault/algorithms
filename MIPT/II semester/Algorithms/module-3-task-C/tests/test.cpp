#define CATCH_CONFIG_MAIN

#include <iostream>
#include <sstream>
#include <string>
#include <catch2/catch.hpp>
#include "../include/solution.h"

TEST_CASE("ANSWER_HOLDER", "TEST1") {
    std::ostringstream ostr;
    std::istringstream istr("3 2\n1 3 3\n2 3 5\n2 1 1");
    SolveTask(ostr, istr);
    
    REQUIRE(ostr.str() == "5");
}

TEST_CASE("ANSWER_HOLDER2", "TEST2") {
	std::ostringstream ostr;
    std::istringstream istr("4 0\n1 3 3 7");
    SolveTask(ostr, istr);
    
    REQUIRE(ostr.str() == "16");
}

TEST_CASE("ANSWER_HOLDER3", "TEST3") {
	std::ostringstream ostr;
    std::istringstream istr("5 4\n1 2 3 4 5\n1 2 8\n1 3 10\n1 4 7\n1 5 15");
    SolveTask(ostr, istr);
    
    REQUIRE(ostr.str() == "18");
}
