#pragma once

#include <algorithm>
#include <cmath>
#include <iostream>
#include <vector>

void MakeHeap(std::vector<int>& Heaps, std::vector<int>& Size, int Vertex) {
    Heaps[Vertex] = Vertex;
    Size[Vertex] = 1;
}

bool IsConnected(std::vector<int>& Heaps, int Vertex1, int Vertex2) {
    int p, q;

    for(p = Vertex1; p != Heaps[p]; p = Heaps[p]) {
        Heaps[p] = Heaps[Heaps[p]];
    }

    for(q = Vertex2; q != Heaps[q]; q = Heaps[q]) {
        Heaps[q] = Heaps[Heaps[q]];
    }

    return p == q;
}

void Connect(std::vector<int>& Heaps, std::vector<int>& Size, int Vertex1, int Vertex2) {
    int p, q;

    for(p = Vertex1; p != Heaps[p]; p = Heaps[p]) {
        Heaps[p] = Heaps[Heaps[p]];
    }

    for(q = Vertex2; q != Heaps[q]; q = Heaps[q]) {
        Heaps[q] = Heaps[Heaps[q]];
    }

    if(p == q) {
        return;
    }

    if(Size[p] > Size[q]) {
        Heaps[q] = p; Size[p] += Size[q];
    } else {
        Heaps[p] = q; Size[q] += Size[p];
    }
}

void SolveTask(std::ostream& ostr, std::istream& istr) {
	const long long Inf = 1e18;

    int NumOfVertices;
    int NumOfOrders;
    std::vector<int> Heaps;
    std::vector<int> Size;
    std::vector< std::pair<long long, int> > CostArray;
    std::vector< std::pair<long long, std::pair<int, int> > > Orders;

    istr >> NumOfVertices >> NumOfOrders;
    Heaps.resize(NumOfVertices, 0);
    Size.resize(NumOfVertices);
    CostArray.resize(NumOfVertices);
    Orders.resize(NumOfOrders);

    for(int i = 0; i < NumOfVertices; i++) {
        MakeHeap(Heaps, Size, i);
    }

    for(int i = 0; i < NumOfVertices; i++) {
        istr >> CostArray[i].first;
        CostArray[i].second = i;
    }

    for(int i = 0; i < NumOfOrders; i++) {
        istr >> Orders[i].second.first >> Orders[i].second.second >> Orders[i].first;
        Orders[i].second.first--;
        Orders[i].second.second--;
    }

    std::sort(Orders.begin(), Orders.end());
    std::sort(CostArray.begin(), CostArray.end());

    long long Answer = 0;
    int P2 = 1;
    int P3 = 0;

    while(P2 < CostArray.size()) {
        long long Cost1 = CostArray[0].first + CostArray[P2].first;
        long long Cost2 = (P3 < Orders.size()) ? Orders[P3].first : Inf;

        if(Cost1 <= Cost2) {
            int V1 = CostArray[0].second;
            int V2 = CostArray[P2].second;
            if(!IsConnected(Heaps, V1, V2)) {
                Answer += Cost1;
                Connect(Heaps, Size, V1, V2);
            }
            ++P2;
        } else {
            int V1 = Orders[P3].second.first;
            int V2 = Orders[P3].second.second;
            if(!IsConnected(Heaps, V1, V2)) {
                Answer += Cost2;
                Connect(Heaps, Size, V1, V2);
            }
            ++P3;
        }
    }

    ostr << Answer;
}

