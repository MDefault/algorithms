#ifndef CShape_H
#define CShape_H

#include "Defines.h"

class CShape
{
public:
    CShape();

    virtual ~CShape() = 0;

    virtual bool operator==(const CShape&) const = 0;
    virtual bool operator!=(const CShape&) const;

    virtual bool isCongruentTo(const CShape&) const = 0;
    virtual bool isSimilarTo(const CShape&) const = 0;
    virtual bool containsCPoint(CPoint) const = 0;

    virtual GeomType perimeter() const = 0;
    virtual GeomType area() const = 0;

    virtual void rotate(CPoint, GeomType) = 0;
    virtual void reflex(CPoint) = 0;
    virtual void reflex(CLine) = 0;
    virtual void scale(CPoint, GeomType) = 0;
};

#endif // CShape_H
