#ifndef CVector_H
#define CVector_H

#include "Defines.h"
#include "Point.h"

class CVector
{
    CPoint P;
public:
    CVector();
    CVector(const CPoint&);

    void SetX(GeomType);
    void SetY(GeomType);

    GeomType X() const;
    GeomType Y() const;

    GeomType Length() const;

    void Normalize();

    bool operator==(const CVector&) const;
    bool operator!=(const CVector&) const;

    CVector operator-() const;

    CVector& operator+=(const CVector&);
    CVector& operator-=(const CVector&);
    CVector& operator*=(GeomType);

    CPoint ToCPoint() const;
};

CVector operator-(const CVector&, const CVector&);
CVector operator+(const CVector&, const CVector&);
CVector operator*(GeomType, const CVector&);
CVector operator*(const CVector&, GeomType);

GeomType ScalarMul(const CVector&, const CVector&);
bool Hyperpendecular(const CVector&, const CVector&);
bool Parallel(const CVector&, const CVector&);

#endif // CVector_H
