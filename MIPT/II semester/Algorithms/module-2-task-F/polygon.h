#ifndef CPolygon_H
#define CPolygon_H

#include "Defines.h"
#include "shape.h"
#include <algorithm>
#include <initializer_list>
#include <utility>
#include <vector>

class CPolygon : public CShape
{
protected:
    // делает точки в порядке обхода против часовой стрелки
    void Normalize();

    CPolygon();

    std::vector<CPoint> Vertices;
public:
    CPolygon(const std::vector<CPoint>&);

    template<typename... T>
    CPolygon(const CPoint& P, const T&... args);

    bool operator==(const CShape&) const override;

    bool isSimilarTo(const CShape &) const override;
    bool isCongruentTo(const CShape &) const override;
    bool containsCPoint(CPoint) const override;

    GeomType perimeter() const override;
    GeomType area() const override;

    void rotate(CPoint, GeomType) override;
    void reflex(CLine) override;
    void reflex(CPoint) override;
    void scale(CPoint, GeomType) override;

    std::size_t verticesCount() const;
    const std::vector<CPoint>& getVertices() const;
    bool isConvex() const;
};

#endif // CPolygon_H
