#include "square.h"

CSquare::CSquare()
{
}

CSquare::CSquare(const CPoint& P1, const CPoint& P2)
    : CRectangle(P1, P2, 1.0),
      A((CVector(P1) - P2).Length() / powl(2, 0.5))
{
}

CCircle CSquare::inscribedCCircle() const {
    return CCircle(center(), A / 2);
}

CCircle CSquare::circumscribedCCircle() const {
    return CCircle(center(), A / powl(2, 0.5));
}
