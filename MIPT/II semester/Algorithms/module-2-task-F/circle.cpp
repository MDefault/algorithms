#include "circle.h"

CCircle::CCircle()
{
}

CCircle::CCircle(const CPoint& Center, GeomType radius)
    : CEllipse(Center, Center, 2.0 * radius)
{
}

GeomType CCircle::radius() const {
    return Distance / 2.0;
}
