#ifndef CRectangle_H
#define CRectangle_H

#include "polygon.h"
#include "Defines.h"
#include "line.h"

class CRectangle : public CPolygon
{
public:
    CRectangle();
    CRectangle(const CPoint&, const CPoint&, GeomType);

    CPoint center() const;
    std::pair<CLine, CLine> diagonals() const;
};

#endif // CRectangle_H
