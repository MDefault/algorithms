#include "rectangle.h"

CRectangle::CRectangle()
{
}

CRectangle::CRectangle(const CPoint& P1, const CPoint& P2, GeomType K)
{
    if(K < 1.0) {
        K = 1.0 / K;
    }

    CVector Direct = CVector(P2) - P1;
    GeomType FirstLen = Direct.Length() / powl(K * K + 1.0, 0.5);
    CVector Vec2;

    Vec2 = CVectorRotate(Direct, atan(K));
    Vec2.Normalize();
    Vec2 *= FirstLen;

    std::vector<CPoint> Args;

    Args.push_back(P1);
    Args.push_back((Vec2 + P1).ToCPoint());
    Args.push_back(P2);
    Args.push_back((CVector(P2) - Vec2).ToCPoint());

    CPolygon::operator=(CPolygon(Args));
}

CPoint CRectangle::center() const {
    return (0.5*(CVector(Vertices[0]) + CVector(Vertices[2]))).ToCPoint();
}

std::pair<CLine, CLine> CRectangle::diagonals() const {
    CLine Result1(Vertices[0], Vertices[2]);
    CLine Result2(Vertices[1], Vertices[3]);

    return std::make_pair(Result1, Result2);
}
