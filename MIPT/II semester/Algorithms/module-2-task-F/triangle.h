#ifndef CTriangle_H
#define CTriangle_H

#include "polygon.h"
#include "Defines.h"
#include "circle.h"
#include "line.h"

class CTriangle : public CPolygon
{
protected:
    CTriangle();
public:
    CTriangle(const CPoint&, const CPoint&, const CPoint&);

    CCircle circumscribedCCircle() const;
    CCircle inscribedCCircle() const;
    CPoint centroid() const;
    CPoint orthocenter() const;
    CLine EulerCLine() const;
    CCircle nineCPointsCCircle() const;
};

#endif // CTriangle_H
