#ifndef CLine_H
#define CLine_H

#include "Point.h"
#include "vector.h"

class CLine
{
private:
    CVector Direction;
    CPoint Start;

    void NormalizeDirect();
public:
    CLine();
    CLine(const CPoint&, const CPoint&);
    CLine(GeomType, GeomType);
    CLine(const CPoint&, GeomType);
    CLine(const CLine&);

    const CVector& GetDirect() const;
    CPoint CLineCPoint() const;

    CLine& operator=(const CLine&);

    bool operator==(const CLine&) const;
    bool operator!=(const CLine&) const;
};

#endif // CLine_H
