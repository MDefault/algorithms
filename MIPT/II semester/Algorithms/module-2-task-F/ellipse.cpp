#include "ellipse.h"
#include "vector.h"
#include "line.h"

CEllipse::CEllipse()
{
}

CEllipse::CEllipse(const CPoint& F1, const CPoint& F2, GeomType R)
    : F1(F1),
      F2(F2),
      Distance(R)
{
}

bool CEllipse::operator==(const CShape& S) const {
    const CEllipse * E;
    if (!(E = dynamic_cast<const CEllipse*>(&S))) {
        return false;
    }

    return Equal(E->Distance, Distance) && ((F1 == E->F1 && F2 == E->F2) || (F1 == E->F2 && F2 == E->F1));
}

bool CEllipse::isCongruentTo(const CShape& S) const {
    return isSimilarTo(S) && Equal(area(), S.area());
}

bool CEllipse::isSimilarTo(const CShape& S) const {
    const CEllipse * E;
    if (!(E = dynamic_cast<const CEllipse*>(&S))) {
        return false;
    }

    std::pair<GeomType, GeomType> P1 = CalculateParameters(*this);
    std::pair<GeomType, GeomType> P2 = CalculateParameters(*E);

    return Equal(P1.first / P2.first, P1.second / P2.second);
}

bool CEllipse::containsCPoint(CPoint P) const {
    GeomType D1 = ::Distance(F1, P);
    GeomType D2 = ::Distance(F2, P);
    return D1 + D2 < Distance || Equal(D1 + D2, Distance);
}

std::pair<GeomType, GeomType> CEllipse::CalculateParameters(const CEllipse& E) {
    GeomType R = (CVector(E.F2) - E.F1).Length();
    GeomType A = E.Distance / 2;
    GeomType B = sqrtl(pow(A, 2) - pow(R / 2, 2));

    return std::make_pair(A, B);
}

GeomType CEllipse::area() const {
    std::pair<GeomType, GeomType> P = CalculateParameters(*this);
    return M_PIl * P.first * P.second;
}

GeomType CEllipse::perimeter() const {
    std::pair<GeomType, GeomType> P = CalculateParameters(*this);
    return M_PIl * (P.first + P.second) * (1 + (3 * powl((P.first - P.second) / (P.first + P.second), 2)) / (10 + sqrtl(4 - 3 * powl((P.first - P.second) / (P.first + P.second), 2))));
}

void CEllipse::rotate(CPoint center, GeomType angle) {
    angle = angle * M_PIl / 180;

    CVector Result1 = CVector(F1) - center;
    CVector Result2 = CVector(F2) - center;

    Result1 = CVectorRotate(Result1, angle);
    Result2 = CVectorRotate(Result2, angle);
    Result1 += center;
    Result2 += center;

    F1 = Result1.ToCPoint();
    F2 = Result2.ToCPoint();
}

void CEllipse::reflex(CPoint center) {
    F1 = Homotetia(F1, center, -1.0);
    F2 = Homotetia(F2, center, -1.0);
}

void CEllipse::reflex(CLine L) {
    F1 = ReflectByCLine(F1, L);
    F2 = ReflectByCLine(F2, L);
}

void CEllipse::scale(CPoint center, GeomType coefficient) {
    F1 = Homotetia(F1, center, coefficient);
    F2 = Homotetia(F2, center, coefficient);
    Distance *= fabsl(coefficient);
}

std::pair<CPoint, CPoint> CEllipse::focuses() const {
    return std::make_pair(F1, F2);
}

std::pair<CLine, CLine> CEllipse::directrices() const {
    CVector Sub = F2 - F1;
    CVector CLineDirect = CVectorRotate(Sub, M_PI_2);
    std::pair<GeomType, GeomType> Temp = CalculateParameters(*this);
    GeomType Length = Temp.second * Temp.second / Temp.first / eccentricity();

    Sub.Normalize();
    Sub *= Length;

    CLine Result1((F2 + Sub).ToCPoint(), (F2 + Sub + CLineDirect).ToCPoint());
    CLine Result2((F1 - Sub).ToCPoint(), (F1 - Sub + CLineDirect).ToCPoint());

    return std::make_pair(Result1, Result2);
}

GeomType CEllipse::eccentricity() const {
    std::pair<GeomType, GeomType> P = CalculateParameters(*this);

    return powl(1.0 - powl(P.second / P.first, 2.0), 0.5);
}

CPoint CEllipse::center() const {
    return (0.5 * (CVector(F1) + CVector(F2))).ToCPoint();
}































