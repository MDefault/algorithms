#include "triangle.h"

CTriangle::CTriangle()
{
}

CTriangle::CTriangle(const CPoint& P1, const CPoint& P2, const CPoint& P3)
    : CPolygon(std::vector<CPoint>{P1, P2, P3})
{
}

CCircle CTriangle::circumscribedCCircle() const {
    GeomType a = Distance(Vertices[0], Vertices[1]);
    GeomType b = Distance(Vertices[1], Vertices[2]);
    GeomType c = Distance(Vertices[2], Vertices[0]);
    GeomType ASum = pow(a, 2) * (pow(b, 2) + pow(c, 2) - pow(a, 2));
    GeomType BSum = pow(b, 2) * (pow(c, 2) + pow(a, 2) - pow(b, 2));
    GeomType CSum = pow(c, 2) * (pow(a, 2) + pow(b, 2) - pow(c, 2));
    CPoint center = (1.0 / (ASum + BSum + CSum) * (CVector(Vertices[2]) * ASum + CVector(Vertices[0]) * BSum + CVector(Vertices[1]) * CSum)).ToCPoint();

    return CCircle(center, Distance(center, Vertices[0]));
}

CCircle CTriangle::inscribedCCircle() const {
    GeomType A = Distance(Vertices[0], Vertices[1]);
    GeomType B = Distance(Vertices[1], Vertices[2]);
    GeomType C = Distance(Vertices[2], Vertices[0]);

    CVector center = A / (A + B + C) * CVector(Vertices[2]) + B / (A + B + C) * CVector(Vertices[0]) + C / (A + B + C) * CVector(Vertices[1]);

    return CCircle(center.ToCPoint(), Distance(center.ToCPoint(), CLine(Vertices[0], Vertices[1])));
}

CPoint CTriangle::centroid() const {
    return (1.0 / 3.0 * (CVector(Vertices[0]) + Vertices[1] + Vertices[2])).ToCPoint();
}

CPoint CTriangle::orthocenter() const {
    CVector center = circumscribedCCircle().center();
    return (CVector(Vertices[0]) - center + CVector(Vertices[1]) - center + CVector(Vertices[2]) - center + center).ToCPoint();
}

CLine CTriangle::EulerCLine() const {
    return CLine(circumscribedCCircle().center(), orthocenter());
}

CCircle CTriangle::nineCPointsCCircle() const {
    CCircle Result = circumscribedCCircle();
    Result.scale(orthocenter(), 0.5);
    return Result;
}
