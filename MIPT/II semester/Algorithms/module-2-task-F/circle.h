#ifndef CCircle_H
#define CCircle_H

#include "Defines.h"
#include "ellipse.h"

class CCircle : public CEllipse
{
public:
    CCircle();
    CCircle(const CPoint&, GeomType);

    GeomType radius() const;
};

#endif // CCircle_H
