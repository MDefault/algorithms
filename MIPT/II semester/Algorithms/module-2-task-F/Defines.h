#ifndef DEFINES_H
#define DEFINES_H

#include <algorithm>
#include <cmath>
#include <limits>
#include <utility>

struct CPoint;
class CLine;
class CShape;
class CVector;

typedef long double GeomType;

bool Equal(GeomType, GeomType);
GeomType Distance(const CPoint&, const CPoint&);

// расстояние между точкой и прямой
GeomType Distance(const CPoint&, const CLine&);

// проверяет, образует ли указанная тройка точек поворот по часовой стрелке
bool ClockWise(const CPoint&, const CPoint&, const CPoint&);

// находит знаковую площадь треугольника (против часовой - плюс)
GeomType CTriangleSignArea(const CPoint&, const CPoint&, const CPoint&);

// возвращает синус угла между векторами (+ если поворот против часовой стрелки)
GeomType CVectorAngle(const CVector&, const CVector&);

// поворот против часовой стрелки на angle
CVector CVectorRotate(const CVector&, GeomType angle);

// возвращает ортогональный вектор такой, что они образую правую двойку (первый
CVector GetHyperpendecular(const CVector&);

// отражает точку относительно прямой
CPoint ReflectByCLine(const CPoint&, const CLine&);

// возвращает гомотетию точки относительно центра с коэффициентом
CPoint Homotetia(const CPoint&, const CPoint&, GeomType);

#endif // DEFINES_H
