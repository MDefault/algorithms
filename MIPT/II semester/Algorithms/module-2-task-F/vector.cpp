#include "vector.h"

CVector::CVector()
{
}

CVector::CVector(const CPoint& P)
    : P(P)
{
}

void CVector::SetX(GeomType X) {
    P.x = X;
}

void CVector::SetY(GeomType Y) {
    P.y = Y;
}

GeomType CVector::X() const {
    return P.x;
}

GeomType CVector::Y() const {
    return P.y;
}

bool CVector::operator==(const CVector& V) const {
    return Equal(V.X(), X()) && Equal(V.Y(), Y());
}

bool CVector::operator!=(const CVector& V) const {
    return !this->operator==(V);
}

CVector& CVector::operator+=(const CVector& V) {
    P.x += V.X();
    P.y += V.Y();

    return *this;
}

CVector& CVector::operator-=(const CVector& V) {
    *this += (-V);
    return *this;
}

CVector& CVector::operator*=(GeomType K) {
    P.x *= K;
    P.y *= K;

    return *this;
}

CVector CVector::operator-() const {
    CVector result = *this;
    result.P.x = -result.P.x;
    result.P.y = -result.P.y;

    return result;
}

GeomType CVector::Length() const {
    return powl(powl(P.x, 2.0L) + powl(P.y, 2.0L), 0.5L);
}

void CVector::Normalize() {
    if (P == CPoint()) {
        return;
    }

    GeomType Len = Length();
    P.x /= Len;
    P.y /= Len;
}

CPoint CVector::ToCPoint() const {
    return P;
}

CVector operator+(const CVector& V1, const CVector& V2) {
    CVector result = V1;
    result += V2;
    return result;
}

CVector operator-(const CVector& V1, const CVector& V2) {
    CVector result = V1;
    result -= V2;
    return result;
}

CVector operator*(const CVector& V, GeomType K) {
    CVector result = V;
    result *= K;
    return result;
}

CVector operator*(GeomType K, const CVector& V) {
    CVector result = V;
    result *= K;
    return result;
}

GeomType ScalarMul(const CVector& a, const CVector& b) {
    return a.X() * b.X() + a.Y() * b.Y();
}

bool Hyperpendecular(const CVector& a, const CVector& b) {
    return Equal(ScalarMul(a, b), GeomType());
}

bool Parallel(const CVector& a, const CVector& b) {
    if (Equal(a.X(), GeomType()) && !Equal(b.X(), GeomType())) {
        return false;
    } else if (!Equal(a.X(), GeomType()) && Equal(b.X(), GeomType())) {
        return false;
    }

    return Equal(a.Y() / a.X(), b.Y() / b.X());
}
