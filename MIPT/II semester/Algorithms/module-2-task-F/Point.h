#ifndef CPoint_H
#define CPoint_H

#include "Defines.h"

struct CPoint {
public:
    GeomType x;
    GeomType y;
public:
    CPoint(GeomType X = GeomType(), GeomType Y = GeomType());
    CPoint(const CPoint&);

    CPoint& operator=(const CPoint&);

    bool operator==(const CPoint&) const;
    bool operator!=(const CPoint&) const;
};

#endif // CPoint_H
