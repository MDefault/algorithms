#include "line.h"

CLine::CLine()
    : Direction(CPoint(1.0, 0.0))
{
    NormalizeDirect();
}

CLine::CLine(const CPoint& a, const CPoint& b)
{
    Direction.SetX(a.x - b.x);
    Direction.SetY(a.y - b.y);

    Start = a;

    NormalizeDirect();
}

CLine::CLine(GeomType K, GeomType B)
{
    Direction.SetX(1.0);
    Direction.SetY(K);

    Start.x = 0.0;
    Start.y = B;

    NormalizeDirect();
}

CLine::CLine(const CPoint& a, GeomType k)
{
    Direction.SetX(1.0);
    Direction.SetY(k);

    Start = a;

    NormalizeDirect();
}

CLine::CLine(const CLine& L)
    : Direction(L.Direction),
      Start(L.Start)
{
}

CLine& CLine::operator=(const CLine& L) {
    if (this == &L) {
        return *this;
    }

    Direction = L.Direction;
    Start = L.Start;

    return *this;
}

const CVector& CLine::GetDirect() const {
    return Direction;
}

CPoint CLine::CLineCPoint() const {
    return Start;
}

bool CLine::operator==(const CLine& L) const {
    if (Direction != L.Direction) {
        return false;
    }

    return CLine(Start, L.Start).Direction == Direction;
}

bool CLine::operator!=(const CLine& L) const {
    return !this->operator==(L);
}

void CLine::NormalizeDirect() {
    Direction.Normalize();

    if (Equal(Direction.X(), 0.0)) {
        Direction.SetY(fabsl(Direction.Y()));
    } else if (Direction.X() < 0.0) {
        Direction = -Direction;
    }
}
