#include <iomanip>
#include <iostream>
#include "polygon.h"
#include "Point.h"
#include "line.h"
#include "rectangle.h"
#include "triangle.h"

using namespace std;

GeomType fRand(GeomType fMin, GeomType fMax)
{
    GeomType f = (GeomType)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}

GeomType fRand() {
    return fRand(-rand(), rand());
}

CPoint RandCPoint() {
    return CPoint(fRand(), fRand());
}

int main()
{
    srand(time(0));

    CTriangle T(CPoint(0, 0), CPoint(2, 0), CPoint(1, 1));
    CTriangle Temp = T;
    CPoint P;
    GeomType angle;

    for(int i = 0; i < 20; i++) {
		GeomType Lim = 1e3;

		T.rotate(P = CPoint(fRand(-Lim, Lim), fRand(-Lim, Lim)), angle = fRand(-M_PI, M_PI));
		T.reflex(CLine(CPoint(fRand(-Lim, Lim), fRand(-Lim, Lim)), CPoint(fRand(-Lim, Lim), fRand(-Lim, Lim))));
		T.reflex(CPoint(fRand(-Lim, Lim), fRand(-Lim, Lim)));

		CTriangle Temp2 = T;
		T.scale(P = CPoint(fRand(-Lim, Lim), fRand(-Lim, Lim)), angle = fRand(-1, 1));

		if(!T.isSimilarTo(Temp)) {
		    std::cout << T.isSimilarTo(Temp) << std::endl;
		    std::cout << P.x << ' ' << P.y << std::endl;
		    std::cout << angle << std::endl;
		    std::cout << '!' << std::endl;
		    for(int j = 0; j < 3; j++) {
		    std::cout << T.getVertices()[j].x << ' ' << T.getVertices()[j].y << std::endl;
		    }
		    std::cout << "11" << std::endl;
		    for(int j = 0; j < 3; j++) {
		    std::cout << Temp2.getVertices()[j].x << ' ' << Temp2.getVertices()[j].y << std::endl;
		    }
		    std::cout << std::setprecision(30) << std::endl;
		    std::cout << T.area() << ' ' << Temp.area() << std::endl << std::endl;
		}
    }
    return 0;
}









































