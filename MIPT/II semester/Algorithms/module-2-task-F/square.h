#ifndef CSquare_H
#define CSquare_H

#include "Defines.h"
#include "rectangle.h"
#include "circle.h"

class CSquare : public CRectangle
{
protected:
    CSquare();

    GeomType A; // сторона
public:
    CSquare(const CPoint&, const CPoint&);

    CCircle circumscribedCCircle() const;
    CCircle inscribedCCircle() const;
};

#endif // CSquare_H
