#include "polygon.h"
#include "Point.h"
#include "line.h"
#include "vector.h"

CPolygon::CPolygon()
{
}

template <typename... T>
CPolygon::CPolygon(const CPoint& P, const T&... args) {
    Vertices.push_back(P);
    for (auto&& i : std::initializer_list<CPoint>{args...}) {
        Vertices.push_back(i);
    }
    Normalize();
}

CPolygon::CPolygon(const std::vector<CPoint>& vec)
    : Vertices(vec)
{
    Normalize();
}

std::size_t CPolygon::verticesCount() const {
    return Vertices.size();
}

const std::vector<CPoint>& CPolygon::getVertices() const {
    return Vertices;
}

bool CPolygon::isConvex() const {
    std::size_t Len = Vertices.size();

    for (std::size_t i = 0; i < Vertices.size(); i++) {
        if (ClockWise(Vertices[i], Vertices[(i + 1) % Len], Vertices[(i + 2) % Len])) {
            return false;
        }
    }

    return true;
}

GeomType CPolygon::perimeter() const {
    GeomType result = 0;
    std::size_t Len = Vertices.size();

    for (size_t i = 0; i < Vertices.size(); i++) {
        result += Distance(Vertices[i], Vertices[(i + 1) % Len]);
    }

    return result;
}

GeomType CPolygon::area() const {
    std::size_t Len = Vertices.size();
    GeomType Area = 0;
    CPoint Start;

    for (std::size_t i = 0; i < Len; i++) {
        Area += CTriangleSignArea(Start, Vertices[i], Vertices[(i + 1) % Len]);
    }

    return Area;
}

bool CPolygon::operator==(const CShape& S) const {
    const CPolygon * F;
    if (!(F = dynamic_cast<const CPolygon*>(&S))) {
        return false;
    }

    if (Vertices.size() != F->Vertices.size()) {
        return false;
    }

    int eqIndex = -1;
    for (size_t i = 0; i < F->Vertices.size(); i++) {
        if (F->Vertices[i] == Vertices[0]) {
            eqIndex = i;
            break;
        }
    }

    if (eqIndex == -1) {
        return false;
    }

    size_t len = Vertices.size();
    for (size_t i = 0; i < len; i++) {
        if (Vertices[i] != F->Vertices[(i + eqIndex) % len]) {
            return false;
        }
    }

    return true;
}

bool CPolygon::isSimilarTo(const CShape& S) const {
    const CPolygon * F;
    if (!(F = dynamic_cast<const CPolygon*>(&S))) {
        return false;
    }

    if (Vertices.size() != F->Vertices.size()) {
        return false;
    }

    std::size_t Len = Vertices.size();

    std::vector<GeomType> Angles1(Len);
    std::vector<GeomType> Angles2(Len);

    for (std::size_t i = 0; i < Len; i++) {
        Angles1[i] = CVectorAngle(CVector(Vertices[i % Len]) - Vertices[(i + 1) % Len], CVector(Vertices[(i + 2) % Len]) - Vertices[(i + 1) % Len]);
        Angles2[i] = CVectorAngle(CVector(F->Vertices[i % Len]) - F->Vertices[(i + 1) % Len], CVector(F->Vertices[(i + 2) % Len]) - F->Vertices[(i + 1) % Len]);
    }

    bool Eq = false;
    for (std::size_t i = 0; i < Len; i++) {
        bool Flag = true;
        for (std::size_t j = 0; j < Len; j++) {
            if (!Equal(Angles1[j], Angles2[(j + i) % Len])) {
                Flag = false;
                break;
            }
        }

        if (Flag) {
            Eq = true;
            break;
        }
    }

    for (std::size_t i = 0; i < Len && !Eq; i++) {
        bool Flag = true;
        for (std::size_t j = 0; j < Len; j++) {
            if (!Equal(Angles1[j], Angles2[(-j + i + 2 * Len) % Len])) {
                Flag = false;
                break;
            }
        }

        if (Flag) {
            Eq = true;
            break;
        }
    }

    return Eq;
}

bool CPolygon::isCongruentTo(const CShape& S) const {
    return isSimilarTo(S) && Equal(area(), S.area());
}

bool CPolygon::containsCPoint(CPoint P) const {
    std::size_t Len = Vertices.size();

    CPoint Zero = Vertices[0];
    size_t zeroIndex = 0;
    for (size_t i = 1; i < Len; i++) {
        if (Vertices[i].x < Zero.x || (Equal(Vertices[i].x, Zero.x) && Vertices[i].y < Zero.y)) {
            Zero = Vertices[i];
            zeroIndex = i;
        }
    }
    std::vector<CPoint> tempVertices = Vertices;
    std::rotate(tempVertices.begin(), tempVertices.begin() + zeroIndex, tempVertices.end());
    tempVertices.erase(tempVertices.begin());

    std::vector<double> angles(tempVertices.size());
    for (size_t i = 0; i < tempVertices.size(); i++) {
        GeomType xC = tempVertices[i].x - Zero.x;
        GeomType yC = tempVertices[i].y - Zero.y;
        angles[i] = Equal(xC, 0) ? M_PI_2 : atan(yC / xC);
    }

    if (P.x < Zero.x) {
        return false;
    } else if (P == Zero) {
        return true;
    }

    bool ans = false;
    double currentAngle = Equal(P.x, Zero.x) ? M_PI_2 : atan((P.y - Zero.y) / (P.x - Zero.x));
    std::vector<double>::iterator it = std::upper_bound(angles.begin(), angles.end(), currentAngle);

    if (it == angles.end() && Equal(currentAngle, angles.back())) {
        it = angles.end() - 1;
    }

    if (it != angles.end() && it != angles.begin()) {
        int tmp = int(it - angles.begin());
        double sq = CTriangleSignArea(tempVertices[tmp], tempVertices[tmp - 1], P);
        if (sq < 0 || Equal(sq, 0)) {
            ans = true;
        }
    }

    return ans;
}

void CPolygon::rotate(CPoint center, GeomType angle) {
    angle = angle * M_PIl / 180;

    for (size_t i = 0; i < Vertices.size(); i++) {
        CVector NewCoordinats = CVector(Vertices[i]) - CVector(center);
        NewCoordinats = CVectorRotate(NewCoordinats, angle);
        NewCoordinats += CVector(center);

        Vertices[i] = NewCoordinats.ToCPoint();
    }

    Normalize();
}

void CPolygon::reflex(CLine L) {
    for (std::size_t i = 0; i < Vertices.size(); i++) {
        Vertices[i] = ReflectByCLine(Vertices[i], L);
    }

    Normalize();
}

void CPolygon::reflex(CPoint center) {
    for (size_t i = 0; i < Vertices.size(); i++) {
        Vertices[i] = Homotetia(Vertices[i], center, -1.0);
    }

    Normalize();
}

void CPolygon::scale(CPoint O, GeomType K) {
    for (std::size_t i = 0; i < Vertices.size(); i++) {
        Vertices[i] = Homotetia(Vertices[i], O, K);
    }

    Normalize();
}

void CPolygon::Normalize() {
    std::size_t len = Vertices.size();
    for (std::size_t i = 0; i < Vertices.size(); i++) {
        if (ClockWise(Vertices[i], Vertices[(i + 1) % len], Vertices[(i + 2) % len])) {
            std::reverse(Vertices.begin(), Vertices.end());
            return;
        }
    }
}







































