#include "Defines.h"
#include "Point.h"
#include "vector.h"
#include "line.h"

bool Equal(GeomType a, GeomType b) {
    GeomType eps = std::numeric_limits<GeomType>::epsilon();

    return std::fabs(a - b) < eps;
}

GeomType Distance(const CPoint& a, const CPoint& b) {
    return sqrtl(powl(b.x - a.x, 2) + powl(b.y - a.y, 2));
}

GeomType Distance(const CPoint& R0, const CLine& l) {
    CVector R1 = l.CLineCPoint();
    CVector A = l.GetDirect();
    CVector V = R1 + ScalarMul(CVector(R0) - R1, A) / A.Length() / A.Length() * A - CVector(R0);

    return V.Length();
}

bool ClockWise(const CPoint& a, const CPoint& b, const CPoint& c) {
    return CTriangleSignArea(a, b, c) < 0;
}

GeomType CTriangleSignArea(const CPoint& a, const CPoint& b, const CPoint& c) {
    return ((b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x)) / 2;
}

CVector CVectorRotate(const CVector& V, GeomType angle) {
    CVector result;
    result.SetX(V.X() * cosl(angle) - V.Y() * sinl(angle));
    result.SetY(V.X() * sinl(angle) + V.Y() * cosl(angle));

    return result;
}

GeomType CVectorAngle(const CVector& a, const CVector& b) {
    CPoint P;
    GeomType CSquare = CTriangleSignArea(P, a.ToCPoint(), b.ToCPoint());

    return 2.0 * CSquare / a.Length() / b.Length();
}

CPoint ReflectByCLine(const CPoint& P, const CLine& L) {
    CVector LVec = L.GetDirect();
    CVector LCPoint = L.CLineCPoint();
    CVector result = 2.0 * LCPoint - CVector(P) + 2.0 * ScalarMul(CVector(P) - LCPoint, LVec) / powl(LVec.Length(), 2.0) * LVec;

    return result.ToCPoint();
}

CPoint Homotetia(const CPoint& P, const CPoint& center, GeomType K) {
    CVector Result = CVector(P) - CVector(center);
    Result *= K;
    Result += CVector(center);

    return Result.ToCPoint();
}
