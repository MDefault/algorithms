#ifndef ELLIPSE_H
#define ELLIPSE_H

#include <utility>
#include "Defines.h"
#include "shape.h"
#include "Point.h"

class CEllipse : public CShape
{
protected:
    CPoint F1;
    CPoint F2;
    GeomType Distance;

    // возвращает A и B
    static std::pair<GeomType, GeomType> CalculateParameters(const CEllipse&);
public:
    CEllipse();
    CEllipse(const CPoint&, const CPoint&, GeomType);

    bool operator==(const CShape&) const override;

    bool isCongruentTo(const CShape &) const override;
    bool isSimilarTo(const CShape &) const override;
    bool containsCPoint(CPoint) const override;

    GeomType area() const override;
    GeomType perimeter() const override;

    void rotate(CPoint, GeomType) override;
    void reflex(CPoint) override;
    void reflex(CLine) override;
    void scale(CPoint, GeomType) override;

    std::pair<CPoint, CPoint> focuses() const;
    std::pair<CLine, CLine> directrices() const;
    GeomType eccentricity() const;
    CPoint center() const;
};

#endif // ELLIPSE_H
