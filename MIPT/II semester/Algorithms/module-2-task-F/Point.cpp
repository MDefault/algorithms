#include "Point.h"

CPoint::CPoint(GeomType X, GeomType Y)
    : x(X),
      y(Y)
{
}

CPoint::CPoint(const CPoint& P)
    : x(P.x),
      y(P.y)
{
}

CPoint& CPoint::operator=(const CPoint& P) {
    if (this == &P) {
        return *this;
    }

    x = P.x;
    y = P.y;

    return *this;
}

bool CPoint::operator==(const CPoint& P) const {
    return Equal(x, P.x) && Equal(y, P.y);
}

bool CPoint::operator!=(const CPoint& P) const {
    return !this->operator==(P);
}
