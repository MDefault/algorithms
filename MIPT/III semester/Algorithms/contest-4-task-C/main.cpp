#include <algorithm>
#include <iostream>
#include <iterator>
#include <map>
#include <set>
#include <vector>

enum class WinnerName {
        Schtirlitz,
        Muller
};

class CruelGame {
public:
        CruelGame(int n);

        WinnerName Winner() const {
                return (outcome_[n_]) ? WinnerName::Schtirlitz : WinnerName::Muller;
        }

        std::vector<int> GetWinnerPos() const;
private:
        int MexFunc(std::vector<int>& outcome_);
        void CalculateOutcome();
private:
        std::vector<int> outcome_;
        int n_;
};

CruelGame::CruelGame(int n)
        : outcome_(n + 5),
        n_(n)
{
        CalculateOutcome();
}

int CruelGame::MexFunc(std::vector<int>& vec) {
        std::sort(vec.begin(), vec.end());
        
        if (vec[0] != 0) {
                return 0;
        }

        for (int i = 1; i < vec.size(); i++) {
                if (vec[i] - vec[i - 1] >= 2) {
                        return vec[i - 1] + 1;
                }
        }

        return vec.back() + 1;
}

void CruelGame::CalculateOutcome() {
        outcome_[0] = 0;
        outcome_[1] = 1;
        outcome_[2] = 1;
        outcome_[3] = 2;
        outcome_[4] = 0;

        for (int i = 5; i <= n_; i++) {
                std::vector<int> tmp = { outcome_[i - 1], outcome_[i - 2] };
                
                for (int j = 3; j <= i - 2; j++) {
                        tmp.push_back(outcome_[j - 1] ^ outcome_[i - j]);
                }

                outcome_[i] = MexFunc(tmp);
        }

}

std::vector<int> CruelGame::GetWinnerPos() const {
        std::vector<int> result;

        if (n_ == 2) {
                return { 1, 2 };
        } else if (n_ == 3) {
                return { 2 };
        }

        if (!outcome_[n_ - 1]) {
                result.push_back(1);
                result.push_back(n_);
        }

        if (!outcome_[n_ - 2]) {
                result.push_back(2);
                result.push_back(n_ - 1);
        }

        for (int i = 3; i <= n_ - 2; i++) {
                if (!(outcome_[i - 1] ^ outcome_[n_ - i])) {
                        result.push_back(i);
                }
        }

        std::sort(result.begin(), result.end());

        return result;
}

int main() {
        int n;
        std::cin >> n;

        CruelGame board(n); 
        std::cout << ((board.Winner() == WinnerName::Muller) ? "Mueller\n" : "Schtirlitz\n");
        
        if (board.Winner() == WinnerName::Schtirlitz) {
                for (auto v : board.GetWinnerPos()) {
                        std::cout << v << std::endl;
                }
        }

        return 0;
}
