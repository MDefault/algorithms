#include <ios>
#include <iostream>
#include <iterator>
#include <sstream>
#include <vector>

namespace Algorithms {
	class KMP {
	public:
		KMP() = delete;
		KMP(const KMP&) = delete;
		KMP(KMP&&) = delete;

		KMP(const std::string& pattern)
			: pattern_(pattern)
		{
		}

		KMP(std::string&& pattern)
			: pattern_(std::move(pattern))
		{
		}

		KMP& operator=(const KMP&) = delete;
		KMP& operator=(KMP&&) = delete;

		template <typename Input, typename Output>
		void PrintEntries(Input, Input, Output) const;
	private:
		std::string pattern_;
	};
}

namespace Algorithms {
	template <typename Input, typename Output>
	void KMP::PrintEntries(Input start, Input end, Output ostr) const {
		std::vector<std::size_t> pi(pattern_.size());

		pi[0] = 0;

		std::size_t prev_value = 0;
		std::size_t index = 1;
		char curr_symbol = 0;

		while (index <= pattern_.size() || start != end) {
			curr_symbol = *start;

			if (index < pattern_.size()) {
				curr_symbol = pattern_[index];
			} else if (index == pattern_.size()) {
				prev_value = 0;
				++index;
				continue;
			} else {
				++start;
			}

			while (curr_symbol != pattern_[prev_value] && prev_value > 0) {
				prev_value = pi[prev_value - 1];
			}

			if (curr_symbol == pattern_[prev_value]) {
				++prev_value;
			}

			if (index < pattern_.size()) {
				pi[index] = prev_value;
			}

			if (index > pattern_.size() && prev_value == pattern_.size()) {
				*ostr++ = index - 2 * pattern_.size();
				prev_value = pi[prev_value - 1];
			}

			++index;
		}
	}
}

int main() {
	std::ios_base::sync_with_stdio(false);
	std::cin.tie(nullptr);
	std::cout.tie(nullptr);
	
	std::string pattern;
	std::cin >> pattern;

	Algorithms::KMP alg(pattern);

	alg.PrintEntries(std::istream_iterator<char>(std::cin),
		       std::istream_iterator<char>(),
		       std::ostream_iterator<std::size_t>(std::cout, " "));

	return 0;
}