#include <algorithm>
#include <exception>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <vector>

namespace Algorithms {
namespace Geometry {

class Vector2dll {
public:
	Vector2dll(int64_t x = 0, int64_t y = 0)
		: x_(x),
		y_(y)
	{
	}

	Vector2dll operator-() const {
		Vector2dll result(-x_, -y_);

		return result;
	}

	Vector2dll& operator+=(const Vector2dll& v2) {
		x_ += v2[0];
		y_ += v2[1];

		return *this;
	}

	Vector2dll& operator-=(const Vector2dll& v2) {
		*this += (-v2);

		return *this;
	}

	Vector2dll& operator*=(int64_t value) {
		x_ *= value;
		y_ *= value;

		return *this;
	}

	int64_t operator[](std::size_t index) const {
		if (index >= 2) {
			throw std::invalid_argument("");
		}

		return (index == 0) ? x_ : y_;
	}
private:
	int64_t x_;
	int64_t y_;
};

Vector2dll operator+(const Vector2dll& a, const Vector2dll& b) {
	Vector2dll result = a;
	result += b;

	return result;
}

Vector2dll operator-(const Vector2dll& a, const Vector2dll& b) {
	Vector2dll result = a;
	result -= b;

	return result;
}

Vector2dll operator*(const Vector2dll& a, int64_t value) {
	Vector2dll result = a;
	result *= value;

	return result;
}

Vector2dll operator*(int64_t value, const Vector2dll& a) {
	Vector2dll result = a;
	result *= value;

	return result;
}

// @desc	| calculate vector mul of 2d vectors
// @par[1, 2]	| 2d vectors
// @return	| z-component of result vector
// 		| (because x, y-components are zero)
int64_t VectorMul(const Vector2dll& a, const Vector2dll& b) {
	return a[0] * b[1] - a[1] * b[0];
}

// @desc		| calculate oriented area of triangle,
// 			| consisting of par[1], par[2], par[3]
// @par[1, 2, 3]	| 2d vectors
// @return		| 2 * oriented area of (par[1], par[2], par[3])
// 			| >0 if (par[1], par[2], par[3]) are counterclock-wise
// 			| <0 else
int64_t OrientedArea(const Vector2dll& a,
		const Vector2dll& b,
		const Vector2dll& c)
{
	return a[0] * (b[1] - c[1]) + b[0] * (c[1] - a[1]) + c[0] * (a[1] - b[1]);
}

// @desc	| calculate area of par[1]
// @par[1]	| convex polygon in traversal order
// @return	| area of par[1]
int64_t PolygonArea(const std::vector<Vector2dll>& polygon) {
	Vector2dll first = polygon[0];
	int64_t area = 0;

	for (std::size_t i = 1; i + 1 < polygon.size(); i++) {
		area += OrientedArea(first, polygon[i], polygon[i + 1]);
	}

	return std::abs(area);
}

// @desc	| reorder convex pollygon such that
// 		| other vertexes are sorted by polar
// 		| andgle relative to the first vertex
// @par[1]	| consecutive vertices of a convex
// 		| polygon in traversal order
// @result	| see @desc
void ReorderByAlpha(std::vector<Vector2dll>& vec) {
	std::size_t left = 0;

	for (std::size_t i = 1; i < vec.size(); i++) {
		if (vec[i][1] < vec[left][1] ||
			(vec[i][1] == vec[left][1] && vec[i][0] < vec[left][0]))
		{
			left = i;
		}
	}

	std::rotate(vec.begin(), vec.begin() + left, vec.end());
}

// @desc	| Calculate Minkowski sum of 
// @par[1, 2]	| consecutive vertices of a convex
// 		| polygon in traversal order
// @return	| Minkowski sum of par[1] and par[2]
std::vector<Vector2dll> MinkowskiSum(const std::vector<Vector2dll>& A,
		const std::vector<Vector2dll>& B)
{
	std::vector<Vector2dll> reord_A = A;
	std::vector<Vector2dll> reord_B = B;
	std::vector<Vector2dll> result;

	ReorderByAlpha(reord_A);
	ReorderByAlpha(reord_B);

	std::size_t iter_A = 0;
	std::size_t iter_B = 0;

	reord_A.push_back(reord_A[0]);
	reord_A.push_back(reord_A[1]);
	reord_B.push_back(reord_B[0]);
	reord_B.push_back(reord_B[1]);

	while (iter_A + 2 < reord_A.size() || iter_B + 2 < reord_B.size()) {
		int64_t vec_mul = VectorMul(reord_A[iter_A + 1] - reord_A[iter_A],
				reord_B[iter_B + 1] - reord_B[iter_B]);
		
		result.push_back(reord_A[iter_A] + reord_B[iter_B]);
		
		if (vec_mul >= 0) {
			++iter_A;
		}

		if (vec_mul <= 0) {
			++iter_B;
		}
	}

	return result;
}

}
}

template <typename Iter>
auto Init(Iter& istr) {
	using Algorithms::Geometry::Vector2dll;

	std::vector<Vector2dll> polygons[2];

	for (int index = 0; index < 2; index++) {
		int num_of_points = 0;
		istr >> num_of_points;
		
		for (int i = 0; i < num_of_points; i++) {
			int64_t x = 0;
			int64_t y = 0;

			istr >> x >> y;

			polygons[index].push_back({x, y});
		}
	}

	return std::make_pair(polygons[0], polygons[1]);
}

template <typename Iter>
long double SolveTask(Iter& istr) {
	using namespace Algorithms::Geometry;

	auto [polygon1, polygon2] = Init(istr);
	std::vector<Vector2dll> mink_sum = MinkowskiSum(polygon1, polygon2);
	
	return (static_cast<long double>(PolygonArea(mink_sum))
		- PolygonArea(polygon1)
		- PolygonArea(polygon2)) / 4;
}

int main() {
	std::cout.precision(6);
	std::cout << std::fixed;
	std::cout << SolveTask(std::cin) << std::endl;

	return 0;
}