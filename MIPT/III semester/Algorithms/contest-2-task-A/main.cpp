#include <algorithm>
#include <array>
#include <iostream>
#include <iterator>
#include <sstream>
#include <vector>

namespace Algorithms {

template <uint32_t AlphabetSize, typename HashFunction>
class SuffixArray {
public:
	SuffixArray(HashFunction hash_f);

	template <typename Iter>
	std::vector<uint32_t> CalcSuffArr(Iter start, Iter end);
	std::vector<uint32_t> GetSuffArr() const {
		return suff_arr_;
	}
private:
	HashFunction hash_f_;
	std::vector<uint32_t> suff_arr_;
private:
	auto PrecalcSuffArr(const std::string& str);
};

template <uint32_t AlphabetSize, typename HashFunction>
SuffixArray<AlphabetSize, HashFunction>::SuffixArray(HashFunction hash_f)
	: hash_f_(hash_f)
{
}

template <uint32_t AlphabetSize, typename HashFunction>
template <typename Iter>
std::vector<uint32_t> SuffixArray<AlphabetSize, HashFunction>::CalcSuffArr(Iter start, Iter end) {
	std::string str;
	
	while (start != end) {
		str.push_back(*start++);
	}

	auto [perm_arr, eq_arr] = PrecalcSuffArr(str);
	std::vector<uint32_t> new_perm_arr (str.size());
	std::vector<uint32_t> new_eq_arr (str.size());
	std::vector<uint32_t> count_arr (str.size());
	std::size_t eq_number = 0;

	for (std::size_t power = 0; (1L << power) < str.size(); power++) {
		std::transform(perm_arr.begin(), perm_arr.end(),
				new_perm_arr.begin(), 
				[power, n = str.size()] (uint32_t value) {
					return (value + n - (1L << power)) % n;
				});
		
		count_arr.assign(count_arr.size(), 0);

		for (int i = 0; i < str.size(); i++) {
			count_arr[eq_arr[new_perm_arr[i]]]++;
		}

		for (int i = 1; i < new_eq_arr.size(); i++) {
			count_arr[i] += count_arr[i - 1];
		}

		for (int i = str.size() - 1; i >= 0; i--) {
			perm_arr[--count_arr[eq_arr[new_perm_arr[i]]]] = new_perm_arr[i];
		}

		new_eq_arr.assign(new_eq_arr.size(), 0);
		new_eq_arr[perm_arr[0]] = eq_number = 0;

		for (int i = 1; i < str.size(); i++) {
			uint32_t second_now = eq_arr[(perm_arr[i] + (1L << power)) % str.size()];
			uint32_t second_prev = eq_arr[(perm_arr[i - 1] + (1L << power)) % str.size()];

			if (eq_arr[perm_arr[i]] != eq_arr[perm_arr[i - 1]] ||
					second_now != second_prev)
			{
				++eq_number;
			}

			new_eq_arr[perm_arr[i]] = eq_number;
		}

		eq_arr = new_eq_arr;
	}

	return suff_arr_ = perm_arr;
}

template <uint32_t AlphabetSize, typename HashFunction>
auto SuffixArray<AlphabetSize, HashFunction>::PrecalcSuffArr(const std::string& str) {
	std::vector<uint32_t> perm_array(str.size());
	std::vector<uint32_t> eq_class(str.size());
	std::array<uint32_t, AlphabetSize> alpha_count;
	uint32_t eq_number = 0;
	
	std::transform(alpha_count.begin(), alpha_count.end(), 
			alpha_count.begin(), [] (uint32_t) { return 0; });

	for (int i = 0; i < str.size(); i++) {
		alpha_count[hash_f_(str[i])]++;
	}

	for (int i = 1; i < alpha_count.size(); i++) {
		alpha_count[i] += alpha_count[i - 1];
	}

	for (int i = 0; i < str.size(); i++) {
		perm_array[--alpha_count[hash_f_(str[i])]] = i;
	}

	eq_class[perm_array[0]] = eq_number;
	
	for (int i = 1; i < str.size(); i++) {
		if (str[perm_array[i]] != str[perm_array[i - 1]]) {
			++eq_number;
		}

		eq_class[perm_array[i]] = eq_number;
	}

	return std::make_pair(perm_array, eq_class);
}

std::vector<uint32_t> BuildLCPArr(const std::vector<uint32_t>& suff_arr, const std::string& str) {
	std::vector<uint32_t> pos_arr(suff_arr.size());
	std::vector<uint32_t> lcp(suff_arr.size());

	for (int i = 0; i < suff_arr.size(); i++) {
		pos_arr[suff_arr[i]] = i;
	}

	uint32_t k = 0;
	
	for (int i = 0; i < suff_arr.size(); i++) {
		if (k > 0) {
			--k;
		}

		if (pos_arr[i] + 1 == str.size()) {
			lcp[str.size() - 1] = 0;
			k = 0;
		} else {
			uint32_t j = suff_arr[pos_arr[i] + 1];

			while (std::max(i + k, j + k) < str.size() &&
					str[i + k] == str[j + k])
			{
				++k;
			}

			lcp[pos_arr[i]] = k;
		}
	}

	return lcp;
}

uint32_t CalcDiffSubstr(const std::vector<uint32_t>& suff_arr, const std::string& str) {
	std::vector<uint32_t> lcp_arr = BuildLCPArr(suff_arr, str);
	uint32_t answer = 0;

	for (int i = 0; i + 1 < lcp_arr.size(); i++) {
		answer += (lcp_arr.size() - 1 - suff_arr[i]) - lcp_arr[i];
	}
	answer += str.size() - 1 - suff_arr.back();

	return answer;
}

}

int main() {
	auto hash_func = [] (char x) -> uint32_t { return x ? x - 'a' + 1 : 0; };
	std::string str;
	Algorithms::SuffixArray<27, decltype(hash_func)> suff_arr(hash_func);
	
	std::cin >> str;
	str.push_back(0);

	std::istringstream str_stream(str);

	std::vector<uint32_t> result = suff_arr.CalcSuffArr(std::istream_iterator<char>(str_stream),
					std::istream_iterator<char>());	

	std::cout << Algorithms::CalcDiffSubstr(result, str) << std::endl;

	return 0;
}
