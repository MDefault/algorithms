#include <cassert>
#include <algorithm>
#include <cmath>
#include <exception>
#include <functional>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <limits>
#include <optional>
#include <set>
#include <vector>

namespace Algorithms {
namespace Geometry {

template <typename T>
int Comparator(T a, T b);

template <>
int Comparator(int64_t a, int64_t b) {
	return (a < b) ? -1 : ((a == b) ? 0 : 1);
}

template <>
int Comparator(long double a, long double b) {
	if (std::fabs(a - b) < 1e-9) {
		return 0;
	}

	return (a < b) ? -1 : 1;
}

}
}
namespace Algorithms {
namespace Geometry {

// @desc		| Point or Vector implementation in D dimenstion
// @par[D]		| Dimension of space
// @par[Type]		| Space over field Type
template <uint8_t Dimension, typename Type>
class Vector {
public:
	Vector() = default;
	Vector(const std::array<Type, Dimension>&);

	const Type& operator[](std::size_t index) const;
	Type& operator[](std::size_t index);

	Vector& operator+=(const Vector<Dimension, Type>& v);
	Vector& operator-=(const Vector<Dimension, Type>& v);
	Vector& operator*=(Type sc);
	Vector& operator/=(Type sc);

	// @desc	| evaluate the square of vector length
	// @return	| square of vector length
	Type LenSquare() const;
private:
	template <typename... Args>
	void Init(std::size_t, Args...);
private:
	std::array<Type, Dimension> coordinates_;
};

// @desc	| implementation of line segment
// @par[D]	| dimension of space
// @par[Type]	| the space's field
template <uint8_t Dimension, typename Type>
class LineSegment {
public:
	LineSegment()
	{
	}

	// @desc 	| Line segment with ends at A and B points
	// @par[A]	| First segment point
	// @par[B]	| Second segment point
	// @par[index]	| Segment's index in data
	LineSegment(const Vector<Dimension, Type>& A, const Vector<Dimension, Type>& B, std::size_t index = 0);

	// @desc 	| return point with lowest coordinate:
	// 		| (a1, a2, ..., an) <= (b1, b2, ..., bn)
	// 		| <=> a1 <= b1 && a2 <= b2 && ... && an <= bn
	// @return	| point with lowest coordinate
	const Vector<Dimension, Type>& First() const;

	// @desc	| return point with highest coordinate
	// 		| look at @desc of LineSegment::First
	// @return	| point with highest coordinate
	const Vector<Dimension, Type>& Second() const;

	std::size_t GetIndex() const {
		return index_;
	}
private:
	Vector<Dimension, Type> first_;
	Vector<Dimension, Type> second_;
	std::size_t index_;
};

// @desc 		| calculate oriented area of vectors<2, T>
// @par[1, 2, 3] 	| vectors<2, T>
// @return 		| 2*(oriented area of given vectors)
// 			| where oriented area are positive when
// 			| points A, B, C are counterclock-wise and negative else
template <typename T>
T OrientedArea(const Vector<2, T>& A, 
		const Vector<2, T>& B, 
		const Vector<2, T>& C);

// @desc	| Check if two line segments are intersec in 2d
// @par[1, 2]	| line segment of T type in 2d
// return	| True if given line segments are intersec and False else
template <typename T>
bool IsIntersec(const LineSegment<2, T>& first,
		const LineSegment<2, T>& second);

// @desc	| Check if two line segments are intersec in 1d
// @par[1, 2]	| line segment of T type in 1d
// return	| True if given line segments are intersec and False else
template <typename T>
bool IsIntersec(const LineSegment<1, T>& first,
		const LineSegment<1, T>& second);

// @desc	| comparator int Comparator(T a, T b) of T type:
// 		| should return
// 		| 1) -1 if a less than b
// 		| 2)  0 if a equal to b
// 		| 3) +1 if a greatest than b
template <typename T>
int Comparator(T a, T b);

template <uint8_t Dimension, typename Type>
Vector<Dimension, Type>::Vector(const std::array<Type, Dimension>& data)
	: coordinates_(data)
{
}

template <uint8_t Dimension, typename Type>
const Type& Vector<Dimension, Type>::operator[](std::size_t index) const {
	return coordinates_.at(index);
}

template <uint8_t Dimension, typename Type>
Type& Vector<Dimension, Type>::operator[](std::size_t index) {
	return coordinates_.at(index);
}

template <uint8_t Dimension, typename Type>
Vector<Dimension, Type>& Vector<Dimension, Type>::operator+=(const Vector<Dimension, Type>& v) {
	for (int i = 0; i < Dimension; i++) {
		coordinates_[i] += v.coordinates_[i];
	}

	return *this;
}

template <uint8_t Dimension, typename Type>
Vector<Dimension, Type>& Vector<Dimension, Type>::operator-=(const Vector<Dimension, Type>& v) {
	for (int i = 0; i < Dimension; i++) {
		coordinates_[i] -= v.coordinates_[i];
	}

	return *this;
}

template <uint8_t Dimension, typename Type>
Vector<Dimension, Type>& Vector<Dimension, Type>::operator*=(Type sc) {
	for (int i = 0; i < Dimension; i++) {
		coordinates_[i] *= sc;
	}

	return *this;
}

template <uint8_t Dimension, typename Type>
Vector<Dimension, Type>& Vector<Dimension, Type>::operator/=(Type sc) {
	if (sc == Type()) {
		return std::invalid_argument("Division by zero");
	}

	for (int i = 0; i < Dimension; i++) {
		coordinates_[i] /= sc;
	}

	return *this;
}

template <uint8_t Dimension, typename Type>
Type Vector<Dimension, Type>::LenSquare() const {
	Type result = Type();

	for (int i = 0; i < Dimension; i++) {
		result += coordinates_[i] * coordinates_[i];
	}

	return result;
}

template <uint8_t Dimension, typename Type>
Vector<Dimension, Type> operator+(const Vector<Dimension, Type>& v1,
		const Vector<Dimension, Type>& v2)
{
	Vector<Dimension, Type> result = v1;
	result += v2;
	return result;
}

template <uint8_t Dimension, typename Type>
Vector<Dimension, Type> operator-(const Vector<Dimension, Type>& v1,
		const Vector<Dimension, Type>& v2)
{
	Vector<Dimension, Type> result = v1;
	result -= v2;
	return result;
}

template <uint8_t Dimension, typename Type>
Vector<Dimension, Type> operator*(const Vector<Dimension, Type>& v1,
		Type sc)
{
	Vector<Dimension, Type> result = v1;
	result *= sc;
	return result;
}

template <uint8_t Dimension, typename Type>
Vector<Dimension, Type> operator/(const Vector<Dimension, Type>& v1,
		Type sc)
{
	Vector<Dimension, Type> result = v1;
	result /= sc;
	return result;
}

template <uint8_t Dimension, typename Type>
Vector<Dimension, Type> operator*(Type sc,
		const Vector<Dimension, Type>& v1)
{
	Vector<Dimension, Type> result = v1;
	result *= sc;
	return result;
}

template <uint8_t Dimension, typename Type>
Vector<Dimension, Type> operator/(Type sc,
		const Vector<Dimension, Type>& v1)
{
	Vector<Dimension, Type> result = v1;
	result /= sc;
	return result;
}

template <uint8_t Dimension, typename Type>
LineSegment<Dimension, Type>::LineSegment(const Vector<Dimension, Type>& A, 
		const Vector<Dimension, Type>& B,
		std::size_t index)
	: first_(A),
	second_(B),
	index_(index)
{
	for (int i = 0; i < Dimension; i++) {
		if (Comparator(first_[i], second_[i]) == 1) {
			std::swap(first_, second_);
			break;
		} else if (Comparator(first_[i], second_[i]) == -1) {
			break;
		}
	}
}

template <uint8_t Dimension, typename Type>
const Vector<Dimension, Type>& LineSegment<Dimension, Type>::First() const {
	return first_;
}

template <uint8_t Dimension, typename Type>
const Vector<Dimension, Type>& LineSegment<Dimension, Type>::Second() const {
	return second_;
}

// @desc		| calculate oriented area of triangle,
// 			| consisting of par[1], par[2], par[3]
// @par[1, 2, 3]	| 2d vectors
// @return		| 2 * oriented area of (par[1], par[2], par[3])
// 			| >0 if (par[1], par[2], par[3]) are counterclock-wise
// 			| <0 else
template <typename T>
T OrientedArea(const Vector<2, T>& a,
		const Vector<2, T>& b,
		const Vector<2, T>& c)
{
	return a[0] * (b[1] - c[1]) + b[0] * (c[1] - a[1]) + c[0] * (a[1] - b[1]);
}

template <typename T>
bool IsIntersec(const LineSegment<2, T>& first,
		const LineSegment<2, T>& second)
{
	LineSegment<1, T> first_x(Vector<1, T>({first.First()[0]}),
			Vector<1, T>({first.Second()[0]}));
	LineSegment<1, T> first_y(Vector<1, T>({first.First()[1]}),
			Vector<1, T>({first.Second()[1]}));
	LineSegment<1, T> second_x(Vector<1, T>({second.First()[0]}),
			Vector<1, T>({second.Second()[0]}));
	LineSegment<1, T> second_y(Vector<1, T>({second.First()[1]}),
			Vector<1, T>({second.Second()[1]}));

	return IsIntersec(first_x, second_x) &&
		IsIntersec(first_y, second_y) &&
		Comparator(OrientedArea(first.First(), first.Second(), second.First()) *
				OrientedArea(first.First(), first.Second(), second.Second()), 
				T()) != 1 &&
		Comparator(OrientedArea(second.First(), second.Second(), first.First()) *
				OrientedArea(second.First(), second.Second(), first.Second()),
				T()) != 1;
}

template <typename T>
bool IsIntersec(const LineSegment<1, T>& first,
		const LineSegment<1, T>& second)
{
	Vector<1, T> max_1 = first.First();
	if (Comparator(max_1[0], second.First()[0]) != 1) {
		max_1 = second.First();
	}

	Vector<1, T> min_1 = first.Second();
	if (Comparator(min_1[0], second.Second()[0]) != -1) {
		min_1 = second.Second();
	}

	return Comparator(max_1[0], min_1[0]) != 1;
}

template <typename T>
std::vector<Vector<2, T>> ConvexHull(const std::vector<Vector<2, T>>& points) 
{
	if (points.empty() || points.size() == 1) {
		return points;
	}

	std::vector<Vector<2, T>> result = points;
	std::sort(result.begin(), result.end(),
			[] (const Vector<2, T>& a, const Vector<2, T>& b)
			{
				return Comparator(a[0], b[0]) == -1 ||
					(Comparator(a[0], b[0]) == 0 && Comparator(a[1], b[1]) == -1);
			});
	auto clock_wise = [] (const Vector<2, T>& A,
			const Vector<2, T>& B,
			const Vector<2, T>& C)
	{
		return Comparator(OrientedArea(A, B, C), T()) == -1;
	};

	auto cclock_wise = [] (const Vector<2, T>& A,
			const Vector<2, T>& B,
			const Vector<2, T>& C)
	{
		return Comparator(OrientedArea(A, B, C), T()) == 1;
	};
	
	std::vector<Vector<2, T>> up;
	std::vector<Vector<2, T>> down;
	Vector<2, T> point1 = result[0];
	Vector<2, T> point2 = result.back();
	
	up.push_back(point1);
	down.push_back(point1);

	for (std::size_t i = 0; i < result.size(); i++) {
		if (i + 1 == result.size() || clock_wise(point1, result[i], point2)) {
			while (up.size() >= 2 && 
				!clock_wise(up[up.size() - 2], up.back(), result[i]))
			{
				up.pop_back();
			}

			up.push_back(result[i]);
		}

		if (i + 1 == result.size() || cclock_wise(point1, result[i], point2)) {
			while (down.size() >= 2 
				&& !cclock_wise(down[down.size() - 2], down.back(), result[i]))
			{
				down.pop_back();
			}

			down.push_back(result[i]);
		}
	}

	result.clear();
	
	for (std::size_t i = 0; i < up.size(); i++) {
		result.push_back(up[i]);
	}

	for (std::size_t i = down.size() - 2; i > 0; i--) {
		result.push_back(down[i]);
	}

	return result;
}

}
}

namespace Algorithms {
namespace Solution {

using namespace Algorithms::Geometry;

template <typename Iter>
long double SolveTask(Iter& istr) {
	std::size_t num_of_points = 0;
	istr >> num_of_points;

	std::vector<Vector<2, int64_t>> points(num_of_points);

	for (std::size_t i = 0; i < num_of_points; i++) {
		istr >> points[i][0] >> points[i][1];
	}
	
	std::vector<Vector<2, int64_t>> hull = Algorithms::Geometry::ConvexHull(points);
	
	long double result = 0;
	
	for (int i = 0; i < hull.size(); i++) {
		result += std::sqrt((hull[i] - hull[(i + 1) % hull.size()]).LenSquare());
	}

	return result;
}

}
}

int main() {
	std::cout << std::setprecision(16) << Algorithms::Solution::SolveTask(std::cin) << std::endl;

	return 0;
}
