#include <algorithm>
#include <iostream>
#include <limits>
#include <map>
#include <sstream>
#include <string>
#include <tuple>
#include <vector>

namespace Algorithms {
template <uint32_t AlphabetSize, typename HashFunction>
class Ukkonen {
public:
	Ukkonen(const std::string& text, int s1_size, HashFunction hash_f_);

	void PrintTree(std::ostream& out);
private:
	static constexpr int inf_ = std::numeric_limits<int>::max();
private:
	struct Vertex {
		std::map<char, int> links; 
		int suffix; 
		int start;
		int end;

		Vertex()
			: suffix(-1),
			start(-1),
			end(-1)
		{
		}
	};
private:
	uint8_t GetChar(int i);
	int MakeVertex();
	void MakeLink(int from, int to, int start, int end);
	int& GetSuffLink(int v);
	void InitTree();
	std::pair<int, int> Canonize(int v, int start, int end);
	std::pair<bool, int> TestAndSplit(int v, int start, int end, uint8_t c);
	std::pair<int, int> Update(int v, int start, int end);

	void DFSTree(int cur, int par, int left_index,
		       int right_index, int& dfs_cnt, std::ostream& out);
private:
	std::vector<Vertex> tree_;
	int root_;
	int dummy_;
	std::string text_;
	int s1_size_;
	HashFunction hash_f_;
};

template <uint32_t AlphabetSize, typename HashFunction>
Ukkonen<AlphabetSize, HashFunction>::Ukkonen(const std::string& text, int s1_size, HashFunction hash_f)
	: text_(text),
	s1_size_(s1_size),
	hash_f_(hash_f)
{
        InitTree();

        std::pair<int, int> activePoint = std::make_pair(root_, 0);
        for (int i = 0; i < text_.size(); i++) {
                activePoint = Update(activePoint.first, activePoint.second, i);
                activePoint = Canonize(activePoint.first, activePoint.second, i + 1);
        }
}

template <uint32_t AlphabetSize, typename HashFunction>
uint8_t Ukkonen<AlphabetSize, HashFunction>::GetChar(int i) {
        return (i < 0) ? (-i - 1) : hash_f_(text_[i]);
}

template <uint32_t AlphabetSize, typename HashFunction>
int Ukkonen<AlphabetSize, HashFunction>::MakeVertex() {
        int i = tree_.size();
        tree_.push_back(Vertex());
        return i;
}

template <uint32_t AlphabetSize, typename HashFunction>
void Ukkonen<AlphabetSize, HashFunction>::MakeLink(int from, int to, int start, int end) {
	tree_[from].links[GetChar(start)] = to;
	tree_[to].start = start;
	tree_[to].end = end;
}

template <uint32_t AlphabetSize, typename HashFunction>
int& Ukkonen<AlphabetSize, HashFunction>::GetSuffLink(int v) {
        return tree_[v].suffix;
}

template <uint32_t AlphabetSize, typename HashFunction>
void Ukkonen<AlphabetSize, HashFunction>::InitTree() {
        tree_.clear();
        dummy_ = MakeVertex();
        root_ = MakeVertex();

        GetSuffLink(root_) = dummy_;
        for (int i = 0; i < AlphabetSize; i++) {
                MakeLink(dummy_, root_, -i - 1, -i);
	}
}

template <uint32_t AlphabetSize, typename HashFunction>
std::pair<int, int> Ukkonen<AlphabetSize, HashFunction>::Canonize(int v, int start, int end) {
        if (end <= start) {
                return std::make_pair(v, start);
        } else {
		int to = tree_[v].links[GetChar(start)];
                Vertex cur = tree_[to];

                while (end - start >= cur.end - cur.start) {
                        start += cur.end - cur.start;
                        v = to;
                        if (end > start) {
				to = tree_[v].links[GetChar(start)];
                                cur = tree_[to];
			}
		}

                return std::make_pair(v, start);
        }
}

template <uint32_t AlphabetSize, typename HashFunction>
std::pair<bool, int> Ukkonen<AlphabetSize, HashFunction>::TestAndSplit(int v, int start, int end, uint8_t c) {
        if (end <= start) {
                return std::make_pair(tree_[v].links.count(c) != 0, v);
        } else {
		int to = tree_[v].links[GetChar(start)];
                Vertex cur = tree_[to];
                
		if (c == GetChar(cur.start + end - start)) {
                        return std::make_pair(true, v);
		}

                int middle = MakeVertex();

                MakeLink(v, middle, cur.start, cur.start + end - start);
                MakeLink(middle, to, cur.start + end - start, cur.end);

		return std::make_pair(false, middle);
        }
}

template <uint32_t AlphabetSize, typename HashFunction>
std::pair<int, int> Ukkonen<AlphabetSize, HashFunction>::Update(int v, int start, int end) {
        std::pair<bool, int> splitRes;
        int oldR = root_;

        splitRes = TestAndSplit(v, start, end, GetChar(end));
        while (!splitRes.first) {
                MakeLink(splitRes.second, MakeVertex(), end, inf_);

                if (oldR != root_) {
                        GetSuffLink(oldR) = splitRes.second;
		}
		
		oldR = splitRes.second;

		std::tie(v, start) = Canonize(GetSuffLink(v), start, end);
                
                splitRes = TestAndSplit(v, start, end, GetChar(end));
        }

        if (oldR != root_) {
                GetSuffLink(oldR) = splitRes.second;
	}
		
	return std::make_pair(v, start);
}

template <uint32_t AlphabetSize, typename HashFunction>
void Ukkonen<AlphabetSize, HashFunction>::DFSTree(int cur, int par, int left_index, 
		int right_index, int& dfs_cnt, std::ostream& out) 
{
	if (par != -1) {
		out << par << ' ';
		
		if (left_index < s1_size_) {
			out << 0 << ' ' << left_index 
				<< ' ' << (right_index == inf_ ? 
						text_.size() : 
						right_index)  << '\n';
		} else {
			out << 1 << ' ' << left_index - s1_size_
				<< ' ' << (right_index == inf_ ? 
						text_.size() : 
						right_index) - s1_size_ << '\n';
		}
	}
	
	int my_order = dfs_cnt;
	for (char c = 0; c <= 'z'; c++) {
		if (tree_[cur].links.count(c) == 0) {
			continue;
		}
		
		int to = tree_[cur].links[c];
		Vertex lnk = tree_[to];
					
		if (lnk.start < s1_size_ && lnk.end > s1_size_) {
			out << my_order << ' '  << 0 << ' ' << lnk.start << ' ' << s1_size_ << '\n';
			++dfs_cnt;
			continue;
		}
		
		++dfs_cnt;
		DFSTree(to, my_order, lnk.start, lnk.end, dfs_cnt, out);
	}
}

template <uint32_t AlphabetSize, typename HashFunction>
void Ukkonen<AlphabetSize, HashFunction>::PrintTree(std::ostream& out) {
	int dfs_cnt = 0;
	out << tree_.size() - 1 << std::endl;

	DFSTree(1, -1, -1, -1, dfs_cnt, out);
}
}

int main() {
	auto hash_f = [] (char x) -> uint8_t {
		return x >= 'a' ? x - 'a' + 2 : 
			(x == '#' ? 0 : 1);
	};

	std::string s1;
	std::string s2;
	std::cin >> s1 >> s2;

	Algorithms::Ukkonen<28, decltype(hash_f)> uk(s1 + s2, s1.size(), hash_f);
	uk.PrintTree(std::cout);

        return 0;
}