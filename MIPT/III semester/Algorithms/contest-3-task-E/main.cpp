#include <cassert>
#include <algorithm>
#include <cmath>
#include <exception>
#include <functional>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <limits>
#include <optional>
#include <set>
#include <vector>

namespace Algorithms {
namespace Geometry {

template <typename T>
int Comparator(T, T);

template <>
int Comparator(int64_t a, int64_t b) {
	return (a < b) ? -1 : ((a == b) ? 0 : 1);
}

template <>
int Comparator(long double a, long double b) {
	if (std::fabs(a - b) < 1e-9) {
		return 0;
	}

	return (a < b) ? -1 : 1;
}

}
}
namespace Algorithms {
namespace Geometry {

// @desc		| Point or Vector implementation in D dimenstion
// @par[D]		| Dimension of space
// @par[Type]		| Space over field Type
template <uint8_t Dimension, typename Type>
class Vector {
public:
	Vector() = default;
	Vector(const std::array<Type, Dimension>&);

	const Type& operator[](std::size_t index) const;
	Type& operator[](std::size_t index);

	Vector& operator+=(const Vector<Dimension, Type>& v);
	Vector& operator-=(const Vector<Dimension, Type>& v);
	Vector& operator*=(Type sc);
	Vector& operator/=(Type sc);

	// @desc	| evaluate the square of vector length
	// @return	| square of vector length
	Type LenSquare() const;
private:
	template <typename... Args>
	void Init(std::size_t, Args...);
private:
	std::array<Type, Dimension> coordinates_;
};

// @desc	| implementation of line segment
// @par[D]	| dimension of space
// @par[Type]	| the space's field
template <uint8_t Dimension, typename Type>
class LineSegment {
public:
	LineSegment()
	{
	}

	// @desc 	| Line segment with ends at A and B points
	// @par[A]	| First segment point
	// @par[B]	| Second segment point
	// @par[index]	| Segment's index in data
	LineSegment(const Vector<Dimension, Type>& A, const Vector<Dimension, Type>& B, std::size_t index = 0);

	// @desc 	| return point with lowest coordinate:
	// 		| (a1, a2, ..., an) <= (b1, b2, ..., bn)
	// 		| <=> a1 <= b1 && a2 <= b2 && ... && an <= bn
	// @return	| point with lowest coordinate
	const Vector<Dimension, Type>& First() const;

	// @desc	| return point with highest coordinate
	// 		| look at @desc of LineSegment::First
	// @return	| point with highest coordinate
	const Vector<Dimension, Type>& Second() const;

	std::size_t GetIndex() const {
		return index_;
	}
private:
	Vector<Dimension, Type> first_;
	Vector<Dimension, Type> second_;
	std::size_t index_;
};

// @desc 		| calculate oriented area of vectors<2, T>
// @par[1, 2, 3] 	| vectors<2, T>
// @return 		| 2*(oriented area of given vectors)
// 			| where oriented area are positive when
// 			| points A, B, C are counterclock-wise and negative else
template <typename T>
T OrientedArea(const Vector<2, T>& A, 
		const Vector<2, T>& B, 
		const Vector<2, T>& C);

// @desc	| Check if two line segments are intersec in 2d
// @par[1, 2]	| line segment of T type in 2d
// return	| True if given line segments are intersec and False else
template <typename T>
bool IsIntersec(const LineSegment<2, T>& first,
		const LineSegment<2, T>& second);

// @desc	| Check if two line segments are intersec in 1d
// @par[1, 2]	| line segment of T type in 1d
// return	| True if given line segments are intersec and False else
template <typename T>
bool IsIntersec(const LineSegment<1, T>& first,
		const LineSegment<1, T>& second);

// @desc	| comparator int Comparator(T a, T b) of T type:
// 		| should return
// 		| 1) -1 if a less than b
// 		| 2)  0 if a equal to b
// 		| 3) +1 if a greatest than b
template <typename T>
int Comparator(T a, T b);

template <uint8_t Dimension, typename Type>
Vector<Dimension, Type>::Vector(const std::array<Type, Dimension>& data)
	: coordinates_(data)
{
}

template <uint8_t Dimension, typename Type>
const Type& Vector<Dimension, Type>::operator[](std::size_t index) const {
	return coordinates_.at(index);
}

template <uint8_t Dimension, typename Type>
Type& Vector<Dimension, Type>::operator[](std::size_t index) {
	return coordinates_.at(index);
}

template <uint8_t Dimension, typename Type>
Vector<Dimension, Type>& Vector<Dimension, Type>::operator+=(const Vector<Dimension, Type>& v) {
	for (int i = 0; i < Dimension; i++) {
		coordinates_[i] += v.coordinates_[i];
	}

	return *this;
}

template <uint8_t Dimension, typename Type>
Vector<Dimension, Type>& Vector<Dimension, Type>::operator-=(const Vector<Dimension, Type>& v) {
	for (int i = 0; i < Dimension; i++) {
		coordinates_[i] -= v.coordinates_[i];
	}

	return *this;
}

template <uint8_t Dimension, typename Type>
Vector<Dimension, Type>& Vector<Dimension, Type>::operator*=(Type sc) {
	for (int i = 0; i < Dimension; i++) {
		coordinates_[i] *= sc;
	}

	return *this;
}

template <uint8_t Dimension, typename Type>
Vector<Dimension, Type>& Vector<Dimension, Type>::operator/=(Type sc) {
	if (sc == Type()) {
		return std::invalid_argument("Division by zero");
	}

	for (int i = 0; i < Dimension; i++) {
		coordinates_[i] /= sc;
	}

	return *this;
}

template <uint8_t Dimension, typename Type>
Type Vector<Dimension, Type>::LenSquare() const {
	Type result = Type();

	for (int i = 0; i < Dimension; i++) {
		result += coordinates_[i] * coordinates_[i];
	}

	return result;
}

template <uint8_t Dimension, typename Type>
Vector<Dimension, Type> operator+(const Vector<Dimension, Type>& v1,
		const Vector<Dimension, Type>& v2)
{
	Vector<Dimension, Type> result = v1;
	result += v2;
	return result;
}

template <uint8_t Dimension, typename Type>
Vector<Dimension, Type> operator-(const Vector<Dimension, Type>& v1,
		const Vector<Dimension, Type>& v2)
{
	Vector<Dimension, Type> result = v1;
	result -= v2;
	return result;
}

template <uint8_t Dimension, typename Type>
Vector<Dimension, Type> operator*(const Vector<Dimension, Type>& v1,
		Type sc)
{
	Vector<Dimension, Type> result = v1;
	result *= sc;
	return result;
}

template <uint8_t Dimension, typename Type>
Vector<Dimension, Type> operator/(const Vector<Dimension, Type>& v1,
		Type sc)
{
	Vector<Dimension, Type> result = v1;
	result /= sc;
	return result;
}

template <uint8_t Dimension, typename Type>
Vector<Dimension, Type> operator*(Type sc,
		const Vector<Dimension, Type>& v1)
{
	Vector<Dimension, Type> result = v1;
	result *= sc;
	return result;
}

template <uint8_t Dimension, typename Type>
Vector<Dimension, Type> operator/(Type sc,
		const Vector<Dimension, Type>& v1)
{
	Vector<Dimension, Type> result = v1;
	result /= sc;
	return result;
}

template <uint8_t Dimension, typename Type>
LineSegment<Dimension, Type>::LineSegment(const Vector<Dimension, Type>& A, 
		const Vector<Dimension, Type>& B,
		std::size_t index)
	: first_(A),
	second_(B),
	index_(index)
{
	for (int i = 0; i < Dimension; i++) {
		if (Comparator(first_[i], second_[i]) == 1) {
			std::swap(first_, second_);
			break;
		} else if (Comparator(first_[i], second_[i]) == -1) {
			break;
		}
	}
}

template <uint8_t Dimension, typename Type>
const Vector<Dimension, Type>& LineSegment<Dimension, Type>::First() const {
	return first_;
}

template <uint8_t Dimension, typename Type>
const Vector<Dimension, Type>& LineSegment<Dimension, Type>::Second() const {
	return second_;
}

// @desc		| calculate oriented area of triangle,
// 			| consisting of par[1], par[2], par[3]
// @par[1, 2, 3]	| 2d vectors
// @return		| 2 * oriented area of (par[1], par[2], par[3])
// 			| >0 if (par[1], par[2], par[3]) are counterclock-wise
// 			| <0 else
template <typename T>
T OrientedArea(const Vector<2, T>& a,
		const Vector<2, T>& b,
		const Vector<2, T>& c)
{
	return a[0] * (b[1] - c[1]) + b[0] * (c[1] - a[1]) + c[0] * (a[1] - b[1]);
}

template <typename T>
bool IsIntersec(const LineSegment<2, T>& first,
		const LineSegment<2, T>& second)
{
	LineSegment<1, T> first_x(Vector<1, T>({first.First()[0]}),
			Vector<1, T>({first.Second()[0]}));
	LineSegment<1, T> first_y(Vector<1, T>({first.First()[1]}),
			Vector<1, T>({first.Second()[1]}));
	LineSegment<1, T> second_x(Vector<1, T>({second.First()[0]}),
			Vector<1, T>({second.Second()[0]}));
	LineSegment<1, T> second_y(Vector<1, T>({second.First()[1]}),
			Vector<1, T>({second.Second()[1]}));

	return IsIntersec(first_x, second_x) &&
		IsIntersec(first_y, second_y) &&
		Comparator(OrientedArea(first.First(), first.Second(), second.First()) *
				OrientedArea(first.First(), first.Second(), second.Second()), 
				T()) != 1 &&
		Comparator(OrientedArea(second.First(), second.Second(), first.First()) *
				OrientedArea(second.First(), second.Second(), first.Second()),
				T()) != 1;
}

template <typename T>
bool IsIntersec(const LineSegment<1, T>& first,
		const LineSegment<1, T>& second)
{
	Vector<1, T> max_1 = first.First();
	if (Comparator(max_1[0], second.First()[0]) != 1) {
		max_1 = second.First();
	}

	Vector<1, T> min_1 = first.Second();
	if (Comparator(min_1[0], second.Second()[0]) != -1) {
		min_1 = second.Second();
	}

	return Comparator(max_1[0], min_1[0]) != 1;
}

}
}

namespace Algorithms {
namespace Solution {

using namespace Algorithms::Geometry;

class Point {
public:
	enum class Type {
		LEFT = 1,
		RIGHT = 0
	};
public:
	Point(const Vector<2, long double>& pt, Type type, std::size_t index)
		: point_(pt),
		type_(type),
		index_(index)
	{
	}

	long double operator[](std::size_t index) const {
		return point_[index];
	}

	bool operator<(const Point& a) const {
		if (Comparator(point_[0], a[0]) == 0) {
			return type_ > a.type_;
		}

		return Comparator(point_[0], a[0]) == -1;
	}

	Type GetType() const {
		return type_;
	}

	std::size_t GetIndex() const {
		return index_;
	}
private:
	Vector<2, long double> point_;
	Type type_;
	std::size_t index_;
};

template <typename Iter>
std::vector<LineSegment<2, long double>> Init(Iter& istr) {
	int num_of_lines = 0;
	std::vector<LineSegment<2, long double>> result;
	int x1 = 0;
	int x2 = 0;
	int y1 = 0;
	int y2 = 0;

	istr >> num_of_lines;
	
	result.resize(num_of_lines);

	for (std::size_t i = 0; i < num_of_lines; i++) {
		istr >> x1 >> y1 >> x2 >> y2;
		std::array<long double, 2> data1 = {x1, y1};
		std::array<long double, 2> data2 = {x2, y2};
		
		result[i] = LineSegment(Vector<2, long double>(data1),
				Vector<2, long double>(data2),
				i);
	}

	return result;
}

std::optional<std::pair<std::size_t, std::size_t>> SolveTask(const std::vector<LineSegment<2, long double>>& points) {
	using seg_type = LineSegment<2, long double>;

	auto y_by_x = [] (const seg_type& seg, long double x)
	{
		if (Comparator(seg.First()[0], seg.Second()[0]) == 0) {
			return seg.First()[1];
		}

		return seg.First()[1] + (seg.Second()[1] - seg.First()[1])
			* (x - seg.First()[0]) / (seg.Second()[0] - seg.First()[0]);
	};
	
	auto segment_comp = [y_by_x] (const seg_type& a, const seg_type& b) -> bool
	{
		long double x = std::max(a.First()[0], b.First()[0]);
		return Comparator(y_by_x(a, x), y_by_x(b, x)) == -1;
	};

	std::vector<Point> order;
	std::set<seg_type, std::function<bool(const seg_type&, const seg_type&)>> cur_points(segment_comp);
	std::vector<decltype(cur_points.begin())> hash_table(points.size());	

	for (std::size_t i = 0; i < points.size(); i++) {
		order.push_back({points[i].First(), Point::Type::LEFT, i});
		order.push_back({points[i].Second(), Point::Type::RIGHT, i});
	}

	std::sort(order.begin(), order.end());

	for (std::size_t i = 0; i < order.size(); i++) {
		std::size_t index = order[i].GetIndex();

		if (order[i].GetType() == Point::Type::LEFT) {
			auto next = cur_points.lower_bound(points[index]);
			auto prev = next;
			
			if (prev != cur_points.begin()) {
				--prev;
			} else {
				prev = cur_points.end();
			}
			
			if (next != cur_points.end() && IsIntersec(*next, points[index])) {
				return std::make_pair(next->GetIndex(), index);
			}

			if (prev != cur_points.end() && IsIntersec(*prev, points[index])) {
				return std::make_pair(prev->GetIndex(), index);
			}

			hash_table[index] = cur_points.insert(next, points[index]);
		} else {
			decltype(cur_points.begin()) next = hash_table[index];
			decltype(cur_points.begin()) prev = hash_table[index];

			++next;

			if (prev != cur_points.begin()) {
				--prev;
			} else {
				prev = cur_points.end();
			}

			if (next != cur_points.end() && prev != cur_points.end() &&
					IsIntersec(*next, *prev))
			{
				return std::make_pair(prev->GetIndex(), next->GetIndex());
			}

			cur_points.erase(hash_table[index]);
		}
	}

	return std::nullopt;
}

}
}

int main() {
	using namespace Algorithms::Geometry;
	using namespace Algorithms::Solution;

	std::vector<LineSegment<2, long double>> data = Init(std::cin);
	auto answer = SolveTask(data);

	if (answer.has_value()) {
		std::cout << "YES\n" <<
			answer->first + 1 << ' ' <<
			answer->second + 1;
	} else {
		std::cout << "NO\n";
	}

	return 0;
}
