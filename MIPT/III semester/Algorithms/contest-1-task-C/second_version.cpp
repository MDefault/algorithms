#include <algorithm>
#include <iostream>
#include <iterator>
#include <memory>
#include <sstream>
#include <vector>

/* На самом деле, запуская с санитайзером, можно заметить утечки памяти.
 * Происходят в функции BuildTrie из-за цикловых ссылок shared_ptr
 * (ребенок ссылается на родителя по _parent, а родитель на ребёнка.)
 * Нужно исправить!
 */

namespace Algorithms {

using HashFunction = uint32_t (*)(char);

uint32_t DefaultHash(char x) {
	return x - 'a';
}

template <uint32_t AlphabetSize = 26, HashFunction Hash = DefaultHash>
class AhoCorasick {
public:
	template <typename Iter>
	AhoCorasick(Iter begin, Iter end, char delim) {
		_root = std::make_shared<Node>();
		Init(begin, end, delim);
	}

	AhoCorasick(const AhoCorasick&) = delete;
	AhoCorasick(AhoCorasick&&) = delete;
	
	AhoCorasick& operator=(const AhoCorasick&) = delete;
	AhoCorasick& operator=(AhoCorasick&&) = delete;

	template <typename Iter>
	std::vector<uint32_t> FindEntries(Iter begin, Iter end);
public:	
	struct Node {
		// непосредственный родитель
		std::shared_ptr<Node> _parent;

		// буква, по которой перешли в текущий узел из родителя
		std::size_t _parent_letter; 

		// дети (переход по соответствующей букве)
		std::array<std::shared_ptr<Node>, AlphabetSize> _children; 

		// функция перехода
		std::array<std::shared_ptr<Node>, AlphabetSize> _delta; 

		// сжатая суффиксная ссылка
		std::shared_ptr<Node> _compressed_link;

		// суффиксная ссылка
		std::shared_ptr<Node> _suffix_link;

		// является ли строка терминальной
		bool _is_terminal; 

		// позиция строки в шаблоне (с учетом того, что строка может повторяться)
		std::vector<std::size_t> _index; 
	};
private:
	std::shared_ptr<Node> _root;
	std::size_t _pattern_count;
	std::size_t _pattern_len;

	void BuildTrie(const std::string&,
			std::size_t);

	std::shared_ptr<Node> CalcDelta(std::shared_ptr<Node>,
			std::size_t);

	std::shared_ptr<Node> CalcSuffLink(std::shared_ptr<Node>);

	std::shared_ptr<Node> CalcCompLink(std::shared_ptr<Node>);

	template <typename Iter>
	void Init(Iter, Iter, char);
};
}

namespace Algorithms {
template <uint32_t A, HashFunction F>
void AhoCorasick<A, F>::BuildTrie(const std::string& pattern, 
				std::size_t position) 
{
	std::size_t index = 0;
	std::shared_ptr<Node> leaf;
	std::shared_ptr<Node> start_pos = _root;

	for (std::size_t i = 0; i < pattern.size(); i++) {
		index = F(pattern[i]);

		// пытаемся сделать переход
		if (start_pos->_children[index]) {
			start_pos = start_pos->_children[index];
		} else {
			// добавляем новый узел в случае неудачи
			leaf = (start_pos->_children[index] = std::make_shared<Node>());
			leaf->_parent = start_pos;
			leaf->_parent_letter = index;
			start_pos = leaf;
		}
	}

	start_pos->_index.push_back(position);
	start_pos->_is_terminal = true;
}

// вычисляем сууффиксные ссылки
template <uint32_t A, HashFunction F>
std::shared_ptr<typename AhoCorasick<A, F>::Node> AhoCorasick<A, F>::CalcSuffLink(std::shared_ptr<typename AhoCorasick<A, F>::Node> leaf) 
{
	if (leaf->_suffix_link) {
		return leaf->_suffix_link;
	}
	
	if (leaf == _root || leaf->_parent == _root) {
		leaf->_suffix_link = _root;
	} else {
		leaf->_suffix_link = CalcDelta(CalcSuffLink(leaf->_parent), 
						leaf->_parent_letter);
	}

	return leaf->_suffix_link;
}

// подсчет функции перехода
template <uint32_t A, HashFunction F>
std::shared_ptr<typename AhoCorasick<A, F>::Node> AhoCorasick<A, F>::CalcDelta(std::shared_ptr<typename AhoCorasick<A, F>::Node> leaf, 
				std::size_t letter) 
{
	if (leaf->_delta[letter]) {
		return leaf->_delta[letter];
	}

	if (leaf->_children[letter]) {
		leaf->_delta[letter] = leaf->_children[letter];
	} else {
		if (leaf == _root) {
			leaf->_delta[letter] = _root;
		} else {
			leaf->_delta[letter] = CalcDelta(CalcSuffLink(leaf), 
							letter);
		}
	}

	return leaf->_delta[letter];
}

// посчитаем сжатую суффиксную ссылку
template <uint32_t A, HashFunction F>
std::shared_ptr<typename AhoCorasick<A, F>::Node> AhoCorasick<A, F>::CalcCompLink(std::shared_ptr<typename AhoCorasick<A, F>::Node> leaf)
{
	if (leaf->_compressed_link) {
		return leaf->_compressed_link;
	}
	
	if (CalcSuffLink(leaf) == _root) {
		leaf->_compressed_link = _root; 
	} else if (CalcSuffLink(leaf)->_is_terminal) {
		leaf->_compressed_link = CalcSuffLink(leaf);
	} else {
		leaf->_compressed_link = CalcCompLink(CalcSuffLink(leaf));
	}

	return leaf->_compressed_link;
}

template <uint32_t A, HashFunction F>
template <typename Iter>
void AhoCorasick<A, F>::Init(Iter begin, Iter end, char delim) {
	std::string current;
	std::size_t i = 0;

	_pattern_count = 0;

	for (i = 0; begin != end; i++, begin++) {
		if (*begin != delim) {
			current.push_back(*begin);
		} else {
			if (!current.empty()) {
				BuildTrie(current, i - 1);
				++_pattern_count;
			}

			current.clear();
		}
	}

	if (!current.empty()) {
		BuildTrie(current, i - 1);
		++_pattern_count;
	}

	_pattern_len = i;
}

template <uint32_t A, HashFunction F>
template <typename Iter>
std::vector<uint32_t> AhoCorasick<A, F>::FindEntries(Iter begin, Iter end) {
	std::string text;
	std::vector<uint32_t> num_of_templates;
	std::vector<uint32_t> result;
	std::shared_ptr<Node> temp;
	std::shared_ptr<Node> current = _root;
	std::size_t text_size = 0;
	
	if (_pattern_count == 0) {
		while (begin++ != end) {
			++text_size;
		}

		if (_pattern_len <= text_size) {
			for (std::size_t i = 0; i + _pattern_len <= text_size; i++) {
				result.push_back(i);
			}
		}

		return result;
	}
	
	for (; begin != end; text_size++, begin++) {
		current = CalcDelta(current, F(*begin));
		temp = current;
		
		while (temp != _root) {
			if (temp->_is_terminal) {
				for (auto index : temp->_index) {
					if (text_size >= index)
					{
						if (text_size - index >= num_of_templates.size()) {
							num_of_templates.resize((text_size - index + 1) * 2);
						}

						if (++num_of_templates[text_size - index] == _pattern_count) {
							result.push_back(text_size - index);
						}
					}
				}
			}

			temp = CalcCompLink(temp);
		}
	}
	
	result.erase(std::remove_if(result.begin(),
					result.end(),
					[text_size, this] (uint32_t value) { 
						return value + _pattern_len > text_size; 
					}
				), result.end());

	return result;
}
}

int main() {
	std::string p;

	std::cin >> p;

	std::istringstream pattern(p);

	Algorithms::AhoCorasick<> alg(std::istream_iterator<char>(pattern),
		       			std::istream_iterator<char>(),
					'?');

	for (auto index : alg.FindEntries(std::istream_iterator<char>(std::cin),
						std::istream_iterator<char>())) {
		std::cout << index << ' ';
	}

	return 0;
}
