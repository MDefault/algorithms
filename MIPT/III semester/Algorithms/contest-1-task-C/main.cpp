#include <algorithm>
#include <functional>
#include <iostream>
#include <iterator>
#include <memory>
#include <sstream>
#include <vector>

namespace Algorithms {
template <typename HashFunction, uint32_t AlphabetSize = 26>
class AhoCorasick {
public:
	template <typename Iter>
	AhoCorasick(Iter begin, Iter end, HashFunction Hash, char delim = '?') 
       : hash_func_(Hash)
       {
		root_ = std::make_shared<Node>();
		Init(begin, end, delim);
	}

	AhoCorasick(const AhoCorasick&) = delete;
	AhoCorasick(AhoCorasick&&) = delete;
	
	AhoCorasick& operator=(const AhoCorasick&) = delete;
	AhoCorasick& operator=(AhoCorasick&&) = delete;

	template <typename Iter>
	std::vector<uint32_t> FindEntries(Iter begin, Iter end);
public:	
	struct Node {
		std::shared_ptr<Node> _parent;

		std::size_t _parent_letter; 

		std::array<std::shared_ptr<Node>, AlphabetSize> _children; 

		std::array<std::shared_ptr<Node>, AlphabetSize> _delta; 

		std::shared_ptr<Node> _compressed_link;

		std::shared_ptr<Node> _suffix_link;

		bool _is_terminal; 

		std::vector<std::size_t> _index; 
	};
private:
	std::shared_ptr<Node> root_;
	std::size_t pattern_count_;
	std::size_t pattern_len_;
	HashFunction hash_func_;

	void BuildTrie(const std::string&,
			std::size_t);

	std::shared_ptr<Node> CalcDelta(std::shared_ptr<Node>,
			std::size_t);

	std::shared_ptr<Node> CalcSuffLink(std::shared_ptr<Node>);

	std::shared_ptr<Node> CalcCompLink(std::shared_ptr<Node>);

	template <typename Iter>
	void Init(Iter, Iter, char);
};
}

namespace Algorithms {
template <typename HashFunction, uint32_t AlphabetSize>
void AhoCorasick<HashFunction, AlphabetSize>::BuildTrie(const std::string& pattern, 
				std::size_t position) 
{
	std::size_t index = 0;
	std::shared_ptr<Node> leaf;
	std::shared_ptr<Node> start_pos = root_;

	for (std::size_t i = 0; i < pattern.size(); i++) {
		index = hash_func_(pattern[i]);

		if (start_pos->_children[index]) {
			start_pos = start_pos->_children[index];
		} else {
			leaf = (start_pos->_children[index] = std::make_shared<Node>());
			leaf->_parent = start_pos;
			leaf->_parent_letter = index;
			start_pos = leaf;
		}
	}

	start_pos->_index.push_back(position);
	start_pos->_is_terminal = true;
}

template <typename HashFunction, uint32_t AlphabetSize>
std::shared_ptr<typename AhoCorasick<HashFunction, AlphabetSize>::Node> 
	AhoCorasick<HashFunction, AlphabetSize>::CalcSuffLink(std::shared_ptr<typename AhoCorasick<HashFunction, AlphabetSize>::Node> leaf) 
{
	if (leaf->_suffix_link) {
		return leaf->_suffix_link;
	}
	
	if (leaf == root_ || leaf->_parent == root_) {
		leaf->_suffix_link = root_;
	} else {
		leaf->_suffix_link = CalcDelta(CalcSuffLink(leaf->_parent), 
						leaf->_parent_letter);
	}

	return leaf->_suffix_link;
}

template <typename HashFunction, uint32_t AlphabetSize>
std::shared_ptr<typename AhoCorasick<HashFunction, AlphabetSize>::Node> 
	AhoCorasick<HashFunction, AlphabetSize>::CalcDelta(std::shared_ptr<typename AhoCorasick<HashFunction, AlphabetSize>::Node> leaf, 
				std::size_t letter) 
{
	if (leaf->_delta[letter]) {
		return leaf->_delta[letter];
	}

	if (leaf->_children[letter]) {
		leaf->_delta[letter] = leaf->_children[letter];
	} else {
		if (leaf == root_) {
			leaf->_delta[letter] = root_;
		} else {
			leaf->_delta[letter] = CalcDelta(CalcSuffLink(leaf), 
							letter);
		}
	}

	return leaf->_delta[letter];
}

template <typename HashFunction, uint32_t AlphabetSize>
std::shared_ptr<typename AhoCorasick<HashFunction, AlphabetSize>::Node> 
	AhoCorasick<HashFunction, AlphabetSize>::CalcCompLink(std::shared_ptr<typename AhoCorasick<HashFunction, AlphabetSize>::Node> leaf)
{
	if (leaf->_compressed_link) {
		return leaf->_compressed_link;
	}
	
	if (CalcSuffLink(leaf) == root_) {
		leaf->_compressed_link = root_; 
	} else if (CalcSuffLink(leaf)->_is_terminal) {
		leaf->_compressed_link = CalcSuffLink(leaf);
	} else {
		leaf->_compressed_link = CalcCompLink(CalcSuffLink(leaf));
	}

	return leaf->_compressed_link;
}

template <typename HashFunction, uint32_t AlphabetSize>
template <typename Iter>
void AhoCorasick<HashFunction, AlphabetSize>::Init(Iter begin, Iter end, char delim) {
	std::string current;
	std::size_t i = 0;

	pattern_count_ = 0;

	for (i = 0; begin != end; i++, begin++) {
		if (*begin != delim) {
			current.push_back(*begin);
		} else {
			if (!current.empty()) {
				BuildTrie(current, i - 1);
				++pattern_count_;
			}

			current.clear();
		}
	}

	if (!current.empty()) {
		BuildTrie(current, i - 1);
		++pattern_count_;
	}

	pattern_len_ = i;
}

template <typename HashFunction, uint32_t AlphabetSize>
template <typename Iter>
std::vector<uint32_t> AhoCorasick<HashFunction, AlphabetSize>::FindEntries(Iter begin, Iter end) {
	std::string text;
	std::vector<uint16_t> num_of_templates;
	std::vector<uint32_t> result;
	std::shared_ptr<Node> temp;
	std::shared_ptr<Node> current = root_;
	std::size_t text_size = 0;
	
	if (pattern_count_ == 0) {
		while (begin++ != end) {
			++text_size;
		}

		if (pattern_len_ <= text_size) {
			for (std::size_t i = 0; i + pattern_len_ <= text_size; i++) {
				result.push_back(i);
			}
		}

		return result;
	}
	
	for (; begin != end; text_size++, begin++) {
		current = CalcDelta(current, hash_func_(*begin));
		temp = current;
		
		while (temp != root_) {
			if (temp->_is_terminal) {
				for (auto index : temp->_index) {
					if (text_size >= index)
					{
						if (text_size - index >= num_of_templates.size()) {
							num_of_templates.resize(text_size - index + 1);
						}

						if (++num_of_templates[text_size - index] == pattern_count_) {
							result.push_back(text_size - index);
						}
					}
				}
			}

			temp = CalcCompLink(temp);
		}
	}
	
	result.erase(std::remove_if(result.begin(),
					result.end(),
					[text_size, this] (uint32_t value) { 
						return value + pattern_len_ > text_size; 
					}
				), result.end());

	return result;
}
}

int main() {
	std::string p;

	std::cin >> p;

	std::istringstream pattern(p);
	
	auto hash_function = [] (char x) -> uint32_t { return x - 'a'; };


	Algorithms::AhoCorasick<decltype(hash_function), 26> alg(std::istream_iterator<char>(pattern),
		       			std::istream_iterator<char>(),
					hash_function,
					'?');

	for (auto index : alg.FindEntries(std::istream_iterator<char>(std::cin),
						std::istream_iterator<char>())) {
		std::cout << index << ' ';
	}

	return 0;
}
