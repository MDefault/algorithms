﻿#include <cmath>
#include <iostream>
#include <map>
#include <string>
#include <string.h>
#include <vector>

template <int64_t MaxX, int64_t MaxY>
class Point {
public:
	Point(int64_t x = 0, int64_t y = 0)
		: x_(x),
		y_(y)
	{
		if (x_ > MaxX || y_ > MaxY) {
			throw std::out_of_range("");
		}
	}

	const int64_t& operator[](int64_t index) const {
		if (index >= 2) {
			throw std::out_of_range("");
		}

		if (index == 0) {
			return x_;
		} else {
			return y_;
		}
	}

	int64_t& operator[](int64_t index) {
		if (index >= 2) {
			throw std::out_of_range("");
		}

		if (index == 0) {
			return x_;
		} else {
			return y_;
		}
	}
private:
	int64_t x_;
	int64_t y_;
};

enum class WinnerType {
	Terminator,
	Runner,
	None
};

template <int64_t MaxX, int64_t MaxY>
struct State {
	int turn;
	Point<MaxX, MaxY> p1;
	Point<MaxX, MaxY> p2;
public:
	State(int trn, Point<MaxX, MaxY> pnt1, Point<MaxX, MaxY> pnt2)
		: turn(trn),
		p1(pnt1),
		p2(pnt2)
	{
	}
};

template <int64_t MaxX, int64_t MaxY>
class GameBoard {
public:
	GameBoard(const std::vector<std::string>& board);
	WinnerType Winner() const;
private:
	int64_t HashFunc(Point<MaxX, MaxY> p) {
                if (p[1] < 0 || p[1] >= MaxY || p[0] < 0 || p[0] >= MaxX) {
                        throw std::invalid_argument("");
                }

		return p[1] * MaxX + p[0];
	}

	Point<MaxX, MaxY> HashFunc(int64_t index) {
        return {index % MaxX, index / MaxX};
	}

	void InitGraph(const std::vector<std::string>& begin_board);
        bool CanFire(Point<MaxX, MaxY> p1, Point<MaxX, MaxY> p2, const std::vector<std::string>& board);
        bool CanKill(size_t terminator, size_t runner, const std::vector<std::string>& board, bool force);
	void ConnectWith(Point<MaxX, MaxY> from, Point<MaxX, MaxY> to, State<MaxX, MaxY> st,
		const std::vector<std::string>& board);
	void DFS(State<MaxX, MaxY> st);
	void CalculatePositions();
private:
	// 0 for terminator 1 else
	std::vector<std::vector<bool>> win_[2];
	std::vector<std::vector<bool>> lose_[2];
	std::vector<State<MaxX, MaxY>> graph_[MaxX * MaxY][MaxX * MaxY][2];
	int deg_[MaxX * MaxY][MaxX * MaxY][2];
        bool used[MaxX * MaxY][MaxX * MaxY][2];
	int64_t terminator_pos_;
	int64_t runner_pos_;
};

template <int64_t MaxX, int64_t MaxY>
GameBoard<MaxX, MaxY>::GameBoard(const std::vector<std::string>& board) {
	if (board.size() != MaxY || board.front().size() != MaxX) {
		throw std::invalid_argument("");
	}

        memset(used, 0, sizeof(used));
        memset(deg_, 0, sizeof(deg_));

	InitGraph(board);
	CalculatePositions();

	for (int64_t i = 0; i < board.size(); i++) {
		for (int64_t j = 0; j < board[i].size(); j++) {
			if (board[i][j] == '3') {
				terminator_pos_ = HashFunc({j, i});
			} else if (board[i][j] == '2') {
				runner_pos_ = HashFunc({j, i});
			}
		}
	}
}

template <int64_t MaxX, int64_t MaxY>
WinnerType GameBoard<MaxX, MaxY>::Winner() const {
	if (win_[1][terminator_pos_][runner_pos_]) {
		return WinnerType::Runner;
	} else if (lose_[1][terminator_pos_][runner_pos_]) {
		return WinnerType::Terminator;
	}

	return WinnerType::None;
}

template <int64_t MaxX, int64_t MaxY>
void GameBoard<MaxX, MaxY>::CalculatePositions() {
	for (int64_t i = 0; i < MaxX * MaxY; i++) {
		for (int64_t j = 0; j < MaxX * MaxY; j++) {
			for (int turn = 0; turn < 2; turn++) {
                                if (!used[i][j][turn] && (lose_[turn][i][j] || win_[turn][i][j]))
				{
                                        DFS(State<MaxX, MaxY>(turn, HashFunc(i), HashFunc(j)));
				}
			}
		}
	}
}

template <int64_t MaxX, int64_t MaxY>
void GameBoard<MaxX, MaxY>::DFS(State<MaxX, MaxY> cur_st) {	
	int64_t terminator = HashFunc(cur_st.p1);
	int64_t runner = HashFunc(cur_st.p2);
	int turn = cur_st.turn;
        used[terminator][runner][turn] = true;
	
        for (auto st : graph_[terminator][runner][turn]) {
                int64_t p1 = HashFunc(st.p1);
		int64_t p2 = HashFunc(st.p2);

                if (!used[p1][p2][st.turn]) {
		        if (lose_[turn][terminator][runner]) {
			        win_[st.turn][p1][p2] = 1;
                        } else if (--deg_[p1][p2][st.turn] == 0) {
			        lose_[st.turn][p1][p2] = 1;
                        } else {
                                continue;
                        }

                        DFS(State<MaxX, MaxY>(st.turn, st.p1, st.p2));
                }
	}
}

template <int64_t MaxX, int64_t MaxY>
bool GameBoard<MaxX, MaxY>::CanFire(Point<MaxX, MaxY> p1, Point<MaxX, MaxY> p2,
	const std::vector<std::string> &board)
{
        if (p1[0] < 0 || p2[0] < 0 || p1[1] < 0 || p2[1] < 0 ||
                    p1[0] >= MaxX || p1[1] >= MaxY || p2[0] >= MaxX || p2[1] >= MaxY)
        {
                return false;
        }

	if (p1[0] == p2[0] || p1[1] == p2[1] ||
		std::abs(p1[1] - p2[1]) == std::abs(p1[0] - p2[0]))
	{
		if (p1[0] > p2[0]) {
			std::swap(p1, p2);
		}

		int inc_x = (p1[0] == p2[0]) ? 0 : 1;
		int inc_y = (p1[1] == p2[1]) ? 0 : (p1[1] < p2[1] ? 1 : -1);
		int step_count = (p1[0] != p2[0] && p1[1] != p2[1]) ?
			std::abs(p1[0] - p2[0]) + 1 : std::abs(p1[0] - p2[0]) + std::abs(p1[1] - p2[1]) + 1;

		int y = p1[1];
		int x = p1[0];

		while (step_count--) {
			if (board[y][x] == '1') {
				return false;
			}

			x += inc_x;
			y += inc_y;
		}

		return true;
	}

	return false;
}

template <int64_t MaxX, int64_t MaxY>
void GameBoard<MaxX, MaxY>::ConnectWith(Point<MaxX, MaxY> from, Point<MaxX, MaxY> to, State<MaxX, MaxY> st,
	const std::vector<std::string>& board)
{
	if (from[0] >= MaxX || from[1] >= MaxY || from[0] < 0 || from[1] < 0 ||
                to[0] >= MaxX || to[1] >= MaxY || to[0] < 0 || to[1] < 0)
        {
		return;
	}

	if (board[from[1]][from[0]] == '1' || board[to[1]][to[0]] == '1') {
		return;
	}

	graph_[HashFunc(from)][HashFunc(to)][1 - st.turn].push_back(st);
	++deg_[HashFunc(st.p1)][HashFunc(st.p2)][st.turn];
}

template <int64_t MaxX, int64_t MaxY>
bool GameBoard<MaxX, MaxY>::CanKill(size_t terminator, size_t runner, const std::vector<std::string>& board, bool force) {
    int delta_x[] = { 0, 1, 1, 0, -1, -1, -1,  0,  1 };
    int delta_y[] = { 0, 0, 1, 1,  1,  0, -1, -1, -1 };

    if (force && CanFire(HashFunc(terminator), HashFunc(runner), board)) {
        return true;
    }

    bool ans = force;

    for (int k = force; k < 9; k++) {
        Point<MaxX, MaxY> p1 = HashFunc(terminator);
        Point<MaxX, MaxY> p2 = HashFunc(runner);

        if (!force) {
            p1[0] += delta_x[k];
            p1[1] += delta_y[k];
            ans |= CanFire(p1, p2, board);
        } else {
            p2[0] += delta_x[k];
            p2[1] += delta_y[k];

            if (p2[0] >= 0 && p2[1] >= 0 && p2[1] < MaxY && p2[0] < MaxX) {
                ans &= CanKill(HashFunc(p1), HashFunc(p2), board, false);
            }
        }
    }

    return ans;
}

template <int64_t MaxX, int64_t MaxY>
void GameBoard<MaxX, MaxY>::InitGraph(const std::vector<std::string>& begin_board) {
	for (int i = 0; i < 2; i++) {
		win_[i].resize(MaxX * MaxY);
		lose_[i].resize(MaxX * MaxY);

		for (int64_t j = 0; j < MaxX * MaxY; j++) {
			win_[i][j].resize(MaxX * MaxY);
			lose_[i][j].resize(MaxX * MaxY);
		}
	}

	for (int64_t i = 0; i < MaxX * MaxY; i++) {
		for (int64_t j = 0; j < MaxX * MaxY; j++) {
			for (int step = 0; step < 2; step++) {
                                Point<MaxX, MaxY> p1 = HashFunc(i);
                                Point<MaxX, MaxY> p2 = HashFunc(j);

				if (begin_board[p1[1]][p1[0]] == '1' || begin_board[p2[1]][p2[0]] == '1') {
					continue;
				}

				if (step == 0) {
                                        win_[step][i][j] = CanKill(i, j, begin_board, false) ? 1 : 0;
                                        lose_[step][i][j] = (!CanKill(i, j, begin_board, false) &&
					HashFunc(j)[1] + 1 == MaxY) ? 1 : 0;
				} else {
                                        win_[step][i][j] = (!CanKill(i, j, begin_board, true) &&
						HashFunc(j)[1] + 1 == MaxY) ? 1 : 0;
                                        lose_[step][i][j] = !win_[step][i][j] && CanKill(i, j, begin_board, true) ? 1 : 0;
				}

				if (win_[step][i][j] || lose_[step][i][j]) {
					continue;
				}

				int delta_x[] = { 1, 1, 0, -1, -1, -1,  0,  1 };
				int delta_y[] = { 0, 1, 1,  1,  0, -1, -1, -1 };

				for (int k = 0; k < 8; k++) {
                                        Point<MaxX, MaxY> p1_from = p1;
                                        Point<MaxX, MaxY> p2_from = p2;

					if (step == 0) {
						p1_from[0] += delta_x[k];
						p1_from[1] += delta_y[k];
					} else {
						p2_from[0] += delta_x[k];
						p2_from[1] += delta_y[k];
					}

					ConnectWith(p1_from, p2_from, State<MaxX, MaxY>(step, p1, p2), begin_board);
				}
			}
		}
	}
}

int main() {
        std::vector<std::string> board(8);

        for (int i = 0; i < 8; i++) {
                std::cin >> board[i];
	}

        GameBoard<8, 8> gb(board);

	std::cout << ((gb.Winner() == WinnerType::Runner) ? 1 : -1) << std::endl;

	return 0;
}
