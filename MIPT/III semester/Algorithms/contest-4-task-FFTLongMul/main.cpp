#include <iostream>
#include "biginteger.h"

int main() {
        CBigInteger a;
        CBigInteger b;

        std::cin >> a >> b;
        std::cout << a * b << std::endl;

        return 0;
}
