#include <algorithm>
#include <cassert>
#include <cmath>
#include <complex>
#include <iostream>
#include <math.h>
#include <numeric>
#include <string>
#include <vector>

#define M_PI   3.14159265358979323846264338327950288

namespace FFT {
/* implementation dependency: wrong answer for ifft */
template <bool Inverse>
std::vector<std::complex<long double>> native_fft(
                const std::vector<std::complex<long double>>& vec)
{
        std::vector<std::complex<long double>> result(vec.size());
        long double constant = -2 * M_PI;

        if constexpr (Inverse) {
                constant = -constant;
        }

        for (size_t k = 0; k < result.size(); k++) {
                result[k] = { 0, 0 };

                for (size_t i = 0; i < result.size(); i++) {
                        std::complex<long double> tmp = { constant * i * k / result.size(), 0 };

                        result[k] += vec[i] * std::exp(tmp * std::complex<long double>(0, 1));
                }
        }

        return result;
}

/* implementation dependency: wrong answer for ifft */
template <bool Inverse>
std::vector<std::complex<long double>> fft_spec(const std::vector<std::complex<long double>>& vec) {
        if (vec.size() < 10) {
                return native_fft<Inverse>(vec);
        } else if (vec.size() % 2 != 0) {
                throw std::invalid_argument("size of vector must be a power of 2");
        }
        
        std::vector<std::complex<long double>> factor(vec.size() / 2);
        long double constant = -2 * M_PI;

        if constexpr (Inverse) {
                constant = -constant;
        }

        for (size_t i = 0; i < factor.size(); i++) {
                factor[i] = std::exp(std::complex<long double>(0, i * constant / vec.size()));
        }

        std::vector<std::complex<long double>> first_part(vec.size() / 2);
        std::vector<std::complex<long double>> second_part(vec.size() / 2);

        for (size_t i = 0; i < first_part.size(); i++) {
                first_part[i] = vec[i * 2];
                second_part[i] = vec[i * 2 + 1];
        }

        auto even = fft_spec<Inverse>(first_part);
        auto odd = fft_spec<Inverse>(second_part);
        std::vector<std::complex<long double>> result(vec.size());

        for (size_t i = 0; i < even.size(); i++) {
                result[i] = (even[i] + factor[i] * odd[i]);
                result[i + even.size()] = (even[i] - factor[i] * odd[i]);
        }

        return result;
}

/* correct fft function */
template <bool Inverse>
auto fft(const std::vector<std::complex<long double>>& vec) {
        auto result = fft_spec<Inverse>(vec);
        
        if constexpr (Inverse) {
                for (size_t i = 0; i < vec.size(); i++) {
                        result[i] *= std::complex<long double>(1.0 / vec.size(), 0.0);
                }
        }

        return result;
}
}

class CBigInteger {

public:
    CBigInteger();
    CBigInteger( int );
    explicit CBigInteger( long long );
    CBigInteger( const CBigInteger& );
    ~CBigInteger();

    CBigInteger& operator=( const CBigInteger& );

    CBigInteger& operator+=( const CBigInteger& );
    CBigInteger& operator-=( const CBigInteger& );
    CBigInteger& operator*=( const CBigInteger& );
    CBigInteger& operator*=( int );
    CBigInteger& operator/=( const CBigInteger& );
    CBigInteger& operator/=( int );
    CBigInteger& operator%=( const CBigInteger& );
    CBigInteger& operator%=( int );

    CBigInteger& operator++();
    CBigInteger operator++( int );
    CBigInteger& operator--();
    CBigInteger operator--( int );

    CBigInteger operator-() const;

    bool operator==( const CBigInteger& ) const;
    bool operator!=( const CBigInteger& ) const;
    bool operator<=( const CBigInteger& ) const;
    bool operator>=( const CBigInteger& ) const;
    bool operator<( const CBigInteger& ) const;
    bool operator>( const CBigInteger& ) const;

    operator bool() const;

    std::string toString() const;

    friend std::ostream& operator<<( std::ostream&, const CBigInteger& );
    friend std::istream& operator>>( std::istream&, CBigInteger& );

private:
    using BaseType = long long;
    static const size_t BaseLen = 6;
    static const BaseType Base = 1000000;
    static const size_t KaratsubaMinLen = 4;
    static const size_t FFTMinLen = 2;

    std::vector<BaseType> Digits;
    bool IsNegative;

    static CBigInteger PositivePlus( const CBigInteger&, const CBigInteger& ); // |a| >= |b|
    static CBigInteger PositiveMinus( const CBigInteger&, const CBigInteger& ); // |a| >= |b|

    static CBigInteger PositiveShortMul( const CBigInteger&, int );
    static CBigInteger PositiveNativeMul( const CBigInteger&, const CBigInteger& );
    static CBigInteger PositiveKaratsubaMul( const CBigInteger&, const CBigInteger& );
    static CBigInteger PositiveFFTMul( const CBigInteger&, const CBigInteger& );
    static void ShiftInteger( CBigInteger&, size_t );
    static void Copy( const CBigInteger&, CBigInteger&, size_t, size_t ); // [,)

    static std::pair<CBigInteger, CBigInteger> PositiveNativeDiv( const CBigInteger&, int );
    static std::pair<CBigInteger, CBigInteger> PositiveNativeDiv(const CBigInteger&, const CBigInteger&);

    static bool AbsEqual( const CBigInteger&, const CBigInteger& );
    static bool AbsLess( const CBigInteger&, const CBigInteger& );
    static void Normalize( CBigInteger& );

    static std::string itoa(BaseType , size_t Precision);
};

CBigInteger operator+( const CBigInteger&, const CBigInteger& );
CBigInteger operator+( const CBigInteger&, int );
CBigInteger operator+( int, const CBigInteger& );

CBigInteger operator-( const CBigInteger&, const CBigInteger& );
CBigInteger operator-( const CBigInteger&, int );
CBigInteger operator-( int, const CBigInteger& );

CBigInteger operator*( const CBigInteger&, const CBigInteger& );
CBigInteger operator*( const CBigInteger&, int );
CBigInteger operator*( int, const CBigInteger& );

CBigInteger operator/( const CBigInteger&, const CBigInteger& );
CBigInteger operator/( const CBigInteger&, int );
CBigInteger operator/( int, const CBigInteger& );

CBigInteger operator%( const CBigInteger&, const CBigInteger& );
CBigInteger operator%( const CBigInteger&, int );
CBigInteger operator%( int, const CBigInteger& );

bool operator==( const CBigInteger&, int );
bool operator==( int, const CBigInteger& );

bool operator!=( const CBigInteger&, int );
bool operator!=( int, const CBigInteger& );

bool operator<=( const CBigInteger&, int );
bool operator<=( int, const CBigInteger& );

bool operator>=( const CBigInteger&, int );
bool operator>=( int, const CBigInteger& );

bool operator<( const CBigInteger&, int );
bool operator<( int, const CBigInteger& );

bool operator>( const CBigInteger&, int );
bool operator>( int, const CBigInteger& );

CBigInteger::CBigInteger() :
    Digits(1, BaseType()), 
    IsNegative(false)
{
}

CBigInteger::CBigInteger( int Value ) :
    CBigInteger( static_cast<long long>(Value) )
{
}

CBigInteger::CBigInteger( long long Value ) 
{
    IsNegative = false;

    if( Value == 0 ) {
        *this = CBigInteger();
        return;
    } else if( Value < 0 ) {
        Value = -Value;
        IsNegative = true;
    }

    while( Value ) {
        Digits.push_back( Value % Base );
        Value /= Base;
    }
}

CBigInteger::CBigInteger( const CBigInteger& Int ) :
    Digits(Int.Digits), 
    IsNegative(Int.IsNegative)
{

}

CBigInteger::~CBigInteger() 
{
}

CBigInteger& CBigInteger::operator=( const CBigInteger & Int ) 
{
    if(this == &Int ) {
        return *this;
    }

    IsNegative = Int.IsNegative;
    Digits = Int.Digits;

    return *this;
}

CBigInteger& CBigInteger::operator+=( const CBigInteger& Int ) 
{
    if( IsNegative && Int.IsNegative ) {
        if( AbsLess(*this, Int) ) {
            *this = PositivePlus(-Int, -*this);
        } else {
            *this = PositivePlus(-*this, -Int);
        }
        IsNegative = true;
    } else if( !IsNegative && !Int.IsNegative ) {
        if( AbsLess(*this, Int) ) {
            *this = PositivePlus(Int, *this);
        } else {
            *this = PositivePlus(*this, Int);
        }
        IsNegative = false;
    } else if( IsNegative && !Int.IsNegative ) {
        if( AbsLess(-*this, Int) ) {
            *this = PositiveMinus(Int, -*this);
            IsNegative = false;
        } else {
            *this = PositiveMinus(-*this, Int);
            IsNegative = true;
        }
    } else {
        if( AbsLess(*this, Int) ) {
            *this = PositiveMinus(-Int, *this);
            IsNegative = true;
        } else {
            *this = PositiveMinus(*this, -Int);
            IsNegative = false;
        }
    }

    Normalize(*this);

    return *this;
}

CBigInteger& CBigInteger::operator-=( const CBigInteger& Int ) 
{
    *this = -*this;
    *this += Int;
    *this = -*this;

    return *this;
}

CBigInteger& CBigInteger::operator*=( const CBigInteger& Int ) 
{
    if( IsNegative && Int.IsNegative ) {
        *this = PositiveFFTMul(*this, -Int);
        IsNegative = false;
    } else if( IsNegative && !Int.IsNegative ) {
        *this = PositiveFFTMul(*this, Int);
        IsNegative = true;
    } else if( !IsNegative && Int.IsNegative ) {
        *this = PositiveFFTMul(*this, -Int);
        IsNegative = true;
    } else {
        *this = PositiveFFTMul(*this, Int);
        IsNegative = false;
    }

    Normalize(*this);

    return *this;
}

CBigInteger& CBigInteger::operator*=( int Value ) 
{
    if( IsNegative && Value < 0 ) {
        *this = PositiveShortMul( *this, -Value );
        IsNegative = false;
    } else if( IsNegative && Value >= 0 ) {
        *this = PositiveShortMul( *this, Value );
        IsNegative = true;
    } else if( !IsNegative && Value < 0 ) {
        *this = PositiveShortMul( *this, -Value );
        IsNegative = true;
    } else {
        *this = PositiveShortMul( *this, Value );
        IsNegative = false;
    }

    Normalize( *this );

    return *this;
}

CBigInteger& CBigInteger::operator/=( const CBigInteger& Int ) 
{
    if( Int == CBigInteger() ) {
        throw "Divide by zero";
    }

    if( IsNegative && Int.IsNegative ) {
        std::pair<CBigInteger, CBigInteger> P = PositiveNativeDiv( -*this, -Int );

        *this = P.first;
        IsNegative = false;
    } else if( IsNegative && !Int.IsNegative ) {
        std::pair<CBigInteger, CBigInteger> P = PositiveNativeDiv( -*this, Int );

        *this = P.first;
        IsNegative = true;
    } else if( !IsNegative && Int.IsNegative ) {
        std::pair<CBigInteger, CBigInteger> P = PositiveNativeDiv( *this, -Int );

        *this = P.first;
        IsNegative = true;
    } else {
        std::pair<CBigInteger, CBigInteger> P = PositiveNativeDiv( *this, Int );

        *this = P.first;
        IsNegative = false;
    }

    Normalize( *this );

    return *this;
}

CBigInteger& CBigInteger::operator/=( int Value ) 
{
    if( Value == 0 ) {
        throw "Divide by zero";
    }

    if( IsNegative && Value < 0 ) {
        std::pair<CBigInteger, CBigInteger> P = PositiveNativeDiv( -*this, -Value );

        *this = P.first;
        IsNegative = false;
    } else if( IsNegative && Value >= 0 ) {
        std::pair<CBigInteger, CBigInteger> P = PositiveNativeDiv( -*this, Value );

        *this = P.first;
        IsNegative = true;
    } else if( !IsNegative && Value < 0 ) {
        std::pair<CBigInteger, CBigInteger> P = PositiveNativeDiv( *this, -Value );

        *this = P.first;
        IsNegative = true;
    } else {
        std::pair<CBigInteger, CBigInteger> P = PositiveNativeDiv( *this, Value );

        *this = P.first;
        IsNegative = false;
    }

    Normalize( *this );

    return *this;
}

CBigInteger& CBigInteger::operator%=( const CBigInteger& Int ) 
{
    if( Int == CBigInteger() ) {
        throw "Divide by zero";
    }

    if( IsNegative && Int.IsNegative ) {
        std::pair<CBigInteger, CBigInteger> P = PositiveNativeDiv(-*this, -Int);

        *this = P.second;
        IsNegative = true;
    } else if( IsNegative && !Int.IsNegative ) {
        std::pair<CBigInteger, CBigInteger> P = PositiveNativeDiv(-*this, Int);

        *this = P.second;
        IsNegative = true;
    } else if( !IsNegative && Int.IsNegative ) {
        std::pair<CBigInteger, CBigInteger> P = PositiveNativeDiv(*this, -Int);

        *this = P.second;
        IsNegative = false;
    } else {
        std::pair<CBigInteger, CBigInteger> P = PositiveNativeDiv(*this, Int);

        *this = P.second;
        IsNegative = false;
    }

    Normalize( *this );

    return *this;
}

CBigInteger& CBigInteger::operator%=( int Value ) 
{
    if( Value == 0 ) {
        throw "Divide by zero";
    }

    if( IsNegative && Value < 0 ) {
        std::pair<CBigInteger, CBigInteger> P = PositiveNativeDiv(-*this, -Value);

        *this = P.second;
        IsNegative = true;
    } else if( IsNegative && Value >= 0 ) {
        std::pair<CBigInteger, CBigInteger> P = PositiveNativeDiv(-*this, Value);

        *this = P.second;
        IsNegative = true;
    } else if( !IsNegative && Value < 0 ) {
        std::pair<CBigInteger, CBigInteger> P = PositiveNativeDiv(*this, -Value);

        *this = P.second;
        IsNegative = false;
    } else {
        std::pair<CBigInteger, CBigInteger> P = PositiveNativeDiv(*this, Value);

        *this = P.second;
        IsNegative = false;
    }

    Normalize( *this );

    return *this;
}

CBigInteger& CBigInteger::operator++() 
{
    *this += 1;
    return *this;
}

CBigInteger CBigInteger::operator++( int ) 
{
    CBigInteger temp = *this;
    *this += 1;
    return temp;
}

CBigInteger& CBigInteger::operator--() 
{
    *this -= 1;
    return *this;
}

CBigInteger CBigInteger::operator--( int ) 
{
    CBigInteger temp = *this;
    *this -= 1;
    return temp;
}

CBigInteger CBigInteger::operator-() const 
{
    CBigInteger result = *this;

    result.IsNegative = 1 - result.IsNegative;

    Normalize(result);

    return result;
}

bool CBigInteger::operator==( const CBigInteger& Int ) const 
{
    return IsNegative == Int.IsNegative && AbsEqual(*this, Int);
}

bool CBigInteger::operator!=( const CBigInteger& Int ) const
{
    return !(*this == Int);
}

bool CBigInteger::operator<=( const CBigInteger& Int ) const 
{
    return !(*this > Int);
}

bool CBigInteger::operator>=( const CBigInteger& Int ) const 
{
    return !(*this < Int);
}

bool CBigInteger::operator<( const CBigInteger& Int ) const 
{
    if( IsNegative && !Int.IsNegative ) {
        return true;
    } else if( !IsNegative && Int.IsNegative ) {
        return false;
    } else if( IsNegative && Int.IsNegative ) {
        return AbsLess(Int, *this);
    }
    return AbsLess(*this, Int);
}

bool CBigInteger::operator>( const CBigInteger& Int ) const 
{
    return !(*this < Int || *this == Int);
}

CBigInteger::operator bool() const 
{
    return *this != CBigInteger();
}

std::string CBigInteger::toString() const 
{
    std::string result;

    if( IsNegative ) {
        result.push_back('-');
    }

    result += itoa( Digits.back(), 0 );

    for( int i = int(Digits.size()) - 2; i >= 0; i-- ) {
        result += itoa( Digits[i], BaseLen );
    }

    return result;
}

std::ostream& operator<<( std::ostream& ostr, const CBigInteger& Int ) 
{
    if( Int.IsNegative ) {
        ostr << "-";
    }

    ostr << Int.Digits.back();

    for( int i = int(Int.Digits.size()) - 2; i >= 0; i-- ) {
        int NumCount = -1;
        CBigInteger::BaseType Value = Int.Digits[i];

        if( Value == 0 ) {
            NumCount = CBigInteger::BaseLen - 1;
        } else {
            while( Value < CBigInteger::Base ) {
                Value *= 10;
                ++NumCount;
            }
        }

        for( int j = 0; j < NumCount; j++ ) {
            ostr << "0";
        }

        ostr << Int.Digits[i];
    }

    return ostr;
}

std::istream& operator>>( std::istream& istr, CBigInteger& Int ) 
{
    std::string str;
    istr >> str;

    Int.IsNegative = false;

    if( str[0] == '-' ) {
        str = str.substr(1);
        Int.IsNegative = true;
    }

    Int.Digits.resize( str.size() / CBigInteger::BaseLen + 1 );
    int len = 0;

    for( int i = int(str.size()) - 1; i >= 0; i -= CBigInteger::BaseLen ) {
        int index = ( i - int(CBigInteger::BaseLen) + 1 > 0 ) ? (i - int(CBigInteger::BaseLen) + 1) : 0;
        std::string sub = str.substr( index, i - index + 1 );
        Int.Digits[len++] = atoi(sub.c_str());
    }

    CBigInteger::Normalize(Int);

    return istr;
}

CBigInteger CBigInteger::PositivePlus( const CBigInteger& a, const CBigInteger& b ) 
{
    CBigInteger result;
    result.IsNegative = false;
    result.Digits = a.Digits;
    result.Digits.resize( a.Digits.size() + 1, BaseType() );

    for( size_t i = 0; i < b.Digits.size(); i++ ) {
        result.Digits[i] += b.Digits[i];

        if( result.Digits[i] >= Base ) {
            result.Digits[i] -= Base;
            result.Digits[i + 1] += 1;
        }
    }

    for( size_t i = b.Digits.size(); i < a.Digits.size(); i++ ) {
        if( result.Digits[i] >= Base ) {
            result.Digits[i] -= Base;
            result.Digits[i + 1] += 1;
        } else {
            break;
        }
    }

    Normalize( result );

    return result;
}

CBigInteger CBigInteger::PositiveMinus( const CBigInteger& a, const CBigInteger& b ) 
{
    CBigInteger result;
    result.Digits = a.Digits;

    for( size_t i = 0; i < b.Digits.size(); i++ ) {
        result.Digits[i] -= b.Digits[i];

        if( result.Digits[i] < 0 ) {
            result.Digits[i] += Base;
            result.Digits[i + 1] -= 1;
        }
    }

    for( size_t i = b.Digits.size(); i < a.Digits.size(); i++ ) {
        if( result.Digits[i] < 0 ) {
            result.Digits[i] += Base;
            result.Digits[i + 1] -= 1;
        } else {
            break;
        }
    }

    Normalize( result );

    return result;
}

CBigInteger CBigInteger::PositiveShortMul( const CBigInteger& a, int b ) 
{
    CBigInteger result = a;
    result.IsNegative = false;
    result.Digits.resize( a.Digits.size() + 1, BaseType() );

    BaseType R = 0;

    for( size_t i = 0; i < a.Digits.size(); i++ ) {
        result.Digits[i] = result.Digits[i] * b + R;
        if( result.Digits[i] >= Base ) {
            R = result.Digits[i] / Base;
            result.Digits[i] -= Base * R;
        } else {
            R = 0;
        }
    }

    result.Digits.back() += R;

    Normalize( result );

    return result;
}

CBigInteger CBigInteger::PositiveNativeMul( const CBigInteger& a, const CBigInteger& b ) 
{
    CBigInteger result;
    result.IsNegative = false;
    result.Digits.resize( a.Digits.size() + b.Digits.size() + 1, BaseType() );

    for( size_t i = 0; i < a.Digits.size(); i++ ) {
        for( size_t j = 0; j < b.Digits.size(); j++ ) {
            result.Digits[i + j] += a.Digits[i] * b.Digits[j];
            if( result.Digits[i + j] >= Base ) {
                BaseType R = result.Digits[i + j] / Base;
                result.Digits[i + j] -= Base * R;
                result.Digits[i + j + 1] += R;
            }
        }
    }

    Normalize( result );

    return result;
}

CBigInteger CBigInteger::PositiveKaratsubaMul( const CBigInteger& a, const CBigInteger& b ) 
{
    if( a.Digits.size() <= KaratsubaMinLen && b.Digits.size() <= KaratsubaMinLen ) {
        return PositiveNativeMul(a, b);
    }

    std::size_t M = (std::max( a.Digits.size(), b.Digits.size() ) + 1 ) / 2;

    CBigInteger AFirstPart;
    Copy( a, AFirstPart, 0, std::min(M, a.Digits.size()) );

    CBigInteger ASecondPart;
    Copy( a, ASecondPart, std::min(M, a.Digits.size()), a.Digits.size() );

    CBigInteger BFirstPart;
    Copy( b, BFirstPart, 0, std::min(M, b.Digits.size()) );

    CBigInteger BSecondPart;
    Copy( b, BSecondPart, std::min(M, b.Digits.size()), b.Digits.size() );

    CBigInteger APartsSum = AFirstPart + ASecondPart;
    CBigInteger BPartsSum = BFirstPart + BSecondPart;
    CBigInteger ABPartsMul = PositiveKaratsubaMul( APartsSum, BPartsSum );
    CBigInteger ABFirstPartsMul = PositiveKaratsubaMul( AFirstPart, BFirstPart );
    CBigInteger ABSecondPartsMul = PositiveKaratsubaMul( ASecondPart, BSecondPart );

    CBigInteger MiddlePart = ABPartsMul - ABFirstPartsMul - ABSecondPartsMul;

    ShiftInteger( MiddlePart, M );
    ShiftInteger(ABSecondPartsMul, M * 2);

    return ABFirstPartsMul + MiddlePart + ABSecondPartsMul;
}

CBigInteger CBigInteger::PositiveFFTMul( const CBigInteger& a, const CBigInteger& b) {
    if (a.Digits.size() + b.Digits.size() < FFTMinLen) {
        return PositiveNativeMul(a, b);
    }
    
    std::vector<std::complex<long double>> a_data;
    std::vector<std::complex<long double>> b_data;

    std::transform(a.Digits.begin(), a.Digits.end(), std::back_inserter(a_data),
                    [] (BaseType value) { return std::complex<long double>(value, 0); });
    std::transform(b.Digits.begin(), b.Digits.end(), std::back_inserter(b_data),
                    [] (BaseType value) { return std::complex<long double>(value, 0); });

    size_t max_size = std::max(a_data.size(), b_data.size());
    size_t l = 0;

    while ((1ll << l) < max_size) {
        ++l;
    }
    ++l;

    for (size_t i = a_data.size(); i < (1ll << l); i++) {
        a_data.push_back({0, 0});
    }

    for (size_t j = b_data.size(); j < (1ll << l); j++) {
        b_data.push_back({0, 0});
    }

    auto a_fft = FFT::fft<false>(a_data);
    auto b_fft = FFT::fft<false>(b_data);

    for (size_t i = 0; i < a_fft.size(); i++) {
        a_fft[i] *= b_fft[i];
    }

    auto result_ifft = FFT::fft<true>(a_fft);
    std::vector<BaseType> result;

    std::transform(result_ifft.begin(), result_ifft.end(), std::back_inserter(result),
                    [] (std::complex<long double> value) -> BaseType { return std::round(value.real()); });

    BaseType c = 0;
    for (size_t i = 0; i < result.size(); i++) {
        result[i] += c;
        c = result[i] / Base;
        result[i] %= Base;
    }

    CBigInteger answer;
    answer.Digits = result;

    Normalize(answer);
    
    return answer;
}

void CBigInteger::ShiftInteger( CBigInteger& a, size_t len ) 
{
    CBigInteger result;
    result.IsNegative = a.IsNegative;
    result.Digits.resize(a.Digits.size() + len, BaseType());

    for( size_t i = 0; i < a.Digits.size(); i++ ) {
        result.Digits[i + len] = a.Digits[i];
    }

    Normalize( result );

    a = result;
}

void CBigInteger::Copy( const CBigInteger& source, CBigInteger& dist, size_t from, size_t to ) 
{
    if( to == from ) {
        dist = CBigInteger();
        return;
    }

    CBigInteger result;
    result.IsNegative = dist.IsNegative;
    result.Digits.resize( to - from, BaseType() );

    for( size_t i = 0; i < to - from; i++ ) {
        result.Digits[i] = source.Digits[i + from];
    }

    Normalize( result );

    dist = result;
}

std::pair<CBigInteger, CBigInteger> CBigInteger::PositiveNativeDiv( const CBigInteger& a, int b ) 
{
    CBigInteger result;
    result.IsNegative = false;
    result.Digits.resize( a.Digits.size() );

    BaseType Ost = 0;

    for( int i = int(a.Digits.size()) - 1; i >= 0; i-- ) {
        BaseType Cur = Ost * Base + a.Digits[i];
        result.Digits[i] = Cur / b;
        Ost = Cur % b;
    }

    Normalize(result);

    return std::make_pair(result, Ost);
}

std::pair<CBigInteger, CBigInteger> CBigInteger::PositiveNativeDiv( const CBigInteger& a, const CBigInteger& b ) 
{
    CBigInteger result;
    CBigInteger Current;

    result.IsNegative = false;
    result.Digits.resize( a.Digits.size(), BaseType() );

    for( int i = int(a.Digits.size()) - 1; i >= 0; i-- ) {
        Current *= Base;
        Current.Digits[0] = a.Digits[i];

        int MaxValue = 0;
        int LeftBorder = 0;
        int RightBorder = Base;

        while( LeftBorder <= RightBorder ) {
            int Middle = (LeftBorder + RightBorder) / 2;
            CBigInteger Temp = b * Middle;

            if( Temp <= Current ) {
                MaxValue = Middle;
                LeftBorder = Middle + 1;
            } else {
                RightBorder = Middle - 1;
            }
        }

        result.Digits[i] = MaxValue;
        Current = Current - b * MaxValue;
    }

    Normalize( result );

    return std::make_pair(result, Current);
}

bool CBigInteger::AbsEqual( const CBigInteger& a, const CBigInteger& b ) 
{
    if( a.Digits.size() != b.Digits.size() ) {
        return false;
    }

    for( size_t i = 0; i < a.Digits.size(); i++ ) {
        if( a.Digits[i] != b.Digits[i] ) {
            return false;
        }
    }

    return true;
}

bool CBigInteger::AbsLess( const CBigInteger& a, const CBigInteger& b ) 
{
    if( a.Digits.size() < b.Digits.size() ) {
        return true;
    } else if( a.Digits.size() > b.Digits.size() ) {
        return false;
    }

    for( int i = int(a.Digits.size()) - 1; i >= 0; i-- ) {
        if( a.Digits[i] < b.Digits[i] ) {
            return true;
        } else if( a.Digits[i] > b.Digits[i] ) {
            return false;
        }
    }

    return false;
}

void CBigInteger::Normalize( CBigInteger& Int ) 
{
    if( Int.Digits.size() == 0 ) {
        Int = CBigInteger();
    }

    size_t Len = Int.Digits.size();

    while( Len > 1 && Int.Digits[Len - 1] == 0 ) {
        --Len;
    }

    Int.Digits.resize(Len);

    if( Len == 1 && Int.Digits[0] == 0 ) {
        Int.IsNegative = false;
    }
}

std::string CBigInteger::itoa( BaseType Value, size_t Precision ) 
{
    std::string result;

    if( Value == 0 ) {
        result = "0";
    } else {
        while( Value ) {
            result.push_back( Value % 10 + '0' );
            Value /= 10;
        }
    }

    if( Precision != 0 ) {
        while( result.size() < Precision ) {
            result.push_back('0');
        }
    }

    for( size_t i = 0; i < result.size() / 2; i++ ) {
        char c = result[result.size() - 1 - i];
        result[result.size() - 1 - i] = result[i];
        result[i] = c;
    }

    return result;
}

CBigInteger operator+( const CBigInteger& a, const CBigInteger& b ) 
{
    CBigInteger result = a;
    result += b;
    return result;
}

CBigInteger operator+( const CBigInteger& a, int b ) 
{
    CBigInteger result = a;
    result += CBigInteger(b);
    return result;
}

CBigInteger operator+( int a, const CBigInteger& b ) 
{
    CBigInteger result = CBigInteger(a);
    result += b;
    return result;
}

CBigInteger operator-( const CBigInteger& a, const CBigInteger& b ) 
{
    CBigInteger result = a;
    result -= b;
    return result;
}

CBigInteger operator-( const CBigInteger& a, int b ) 
{
    CBigInteger result = a;
    result -= CBigInteger(b);
    return result;
}

CBigInteger operator-( int a, const CBigInteger& b ) 
{
    CBigInteger result = CBigInteger(a);
    result -= b;
    return result;
}

CBigInteger operator*( const CBigInteger& a, const CBigInteger& b ) 
{
    CBigInteger result = a;
    result *= b;
    return result;
}

CBigInteger operator*( const CBigInteger& a, int b ) 
{
    CBigInteger result = a;
    result *= b;
    return result;
}

CBigInteger operator*( int a, const CBigInteger& b ) 
{
    CBigInteger result = CBigInteger(a);
    result *= b;
    return result;
}

CBigInteger operator/( const CBigInteger& a, const CBigInteger& b ) 
{
    CBigInteger result = a;
    result /= b;
    return result;
}

CBigInteger operator/( const CBigInteger& a, int b ) 
{
    CBigInteger result = a;
    result /= b;
    return result;
}

CBigInteger operator/( int a, const CBigInteger& b ) 
{
    CBigInteger result = CBigInteger(a);
    result /= b;
    return result;
}

CBigInteger operator%( const CBigInteger& a, const CBigInteger& b ) 
{
    CBigInteger result = a;
    result %= b;
    return result;
}

CBigInteger operator%( const CBigInteger& a, int b ) 
{
    CBigInteger result = a;
    result %= b;
    return result;
}

CBigInteger operator%( int a, const CBigInteger& b ) 
{
    CBigInteger result = CBigInteger(a);
    result %= b;
    return result;
}

bool operator==( const CBigInteger& a, int b ) 
{
    return a == CBigInteger(b);
}

bool operator==( int a, const CBigInteger& b ) 
{
    return CBigInteger(a) == b;
}

bool operator!=( const CBigInteger& a, int b ) 
{
    return a != CBigInteger(b);
}

bool operator!=( int a, const CBigInteger& b ) 
{
    return CBigInteger(a) != b;
}

bool operator<=( const CBigInteger& a, int b ) 
{
    return a <= CBigInteger(b);
}

bool operator<=( int a, const CBigInteger& b ) 
{
    return CBigInteger(a) <= b;
}

bool operator>=( const CBigInteger& a, int b ) 
{
    return a >= CBigInteger(b);
}

bool operator>=( int a, const CBigInteger& b ) 
{
    return CBigInteger(a) >= b;
}

bool operator<( const CBigInteger& a, int b ) 
{
    return a < CBigInteger(b);
}

bool operator<( int a, const CBigInteger& b ) 
{
    return CBigInteger(a) < b;
}

bool operator>( const CBigInteger& a, int b ) 
{
    return a > CBigInteger(b);
}

bool operator>( int a, const CBigInteger& b ) 
{
    return CBigInteger(a) > b;
}

int main() {
        CBigInteger a;
        CBigInteger b;

        std::cin >> a >> b;
        std::cout << a * b << std::endl;

        return 0;
}
