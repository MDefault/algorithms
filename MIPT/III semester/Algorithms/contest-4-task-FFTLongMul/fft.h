#include <cassert>
#include <complex>
#include <iostream>
#include <numeric>
#include <vector>

namespace FFT {
/* implementation dependency: wrong answer for ifft */
template <bool Inverse>
std::vector<std::complex<long double>> native_fft(
                const std::vector<std::complex<long double>>& vec)
{
        std::vector<std::complex<long double>> result(vec.size());
        long double constant = -2 * M_PI;

        if constexpr (Inverse) {
                constant = -constant;
        }

        for (size_t k = 0; k < result.size(); k++) {
                result[k] = { 0, 0 };

                for (size_t i = 0; i < result.size(); i++) {
                        std::complex<long double> tmp = { constant * i * k / result.size(), 0 };

                        result[k] += vec[i] * std::exp(tmp * std::complex<long double>(0, 1));
                }
        }

        return result;
}

/* implementation dependency: wrong answer for ifft */
template <bool Inverse>
std::vector<std::complex<long double>> fft_spec(const std::vector<std::complex<long double>>& vec) {
        if (vec.size() < 10) {
                return native_fft<Inverse>(vec);
        } else if (vec.size() % 2 != 0) {
                throw std::invalid_argument("size of vector must be a power of 2");
        }
        
        std::vector<std::complex<long double>> factor(vec.size() / 2);
        long double constant = -2 * M_PI;

        if constexpr (Inverse) {
                constant = -constant;
        }

        for (size_t i = 0; i < factor.size(); i++) {
                factor[i] = std::exp(std::complex<long double>(0, i * constant / vec.size()));
        }

        std::vector<std::complex<long double>> first_part(vec.size() / 2);
        std::vector<std::complex<long double>> second_part(vec.size() / 2);

        for (size_t i = 0; i < first_part.size(); i++) {
                first_part[i] = vec[i * 2];
                second_part[i] = vec[i * 2 + 1];
        }

        auto even = fft_spec<Inverse>(first_part);
        auto odd = fft_spec<Inverse>(second_part);
        std::vector<std::complex<long double>> result(vec.size());

        for (size_t i = 0; i < even.size(); i++) {
                result[i] = (even[i] + factor[i] * odd[i]);
                result[i + even.size()] = (even[i] - factor[i] * odd[i]);
        }

        return result;
}

/* correct fft function */
template <bool Inverse>
auto fft(const std::vector<std::complex<long double>>& vec) {
        auto result = fft_spec<Inverse>(vec);
        
        if constexpr (Inverse) {
                for (size_t i = 0; i < vec.size(); i++) {
                        result[i] *= std::complex<long double>(1.0 / vec.size(), 0.0);
                }
        }

        return result;
}
}
