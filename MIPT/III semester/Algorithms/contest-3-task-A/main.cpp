#include <cassert>
#include <algorithm>
#include <cmath>
#include <exception>
#include <functional>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <limits>
#include <optional>
#include <set>
#include <vector>

namespace Algorithms {
namespace Geometry {

// @desc		| Point or Vector implementation in D dimenstion
// @par[D]		| Dimension of space
template <uint8_t Dimension>
class Vector {
public:
	Vector() = default;
	Vector(const std::array<int64_t, Dimension>&);

	const int64_t& operator[](std::size_t index) const;
	int64_t& operator[](std::size_t index);

	Vector& operator+=(const Vector<Dimension>& v);
	Vector& operator-=(const Vector<Dimension>& v);
	Vector& operator*=(int64_t sc);
	Vector& operator/=(int64_t sc);

	// @desc	| evaluate the square of vector length
	// @return	| square of vector length
	int64_t LenSquare() const;
private:
	template <typename... Args>
	void Init(std::size_t, Args...);
private:
	std::array<int64_t, Dimension> coordinates_;
};

// @desc	| implementation of line segment
// @par[D]	| dimension of space
// @par[int64_t]	| the space's field
template <uint8_t Dimension>
class LineSegment {
public:
	LineSegment()
	{
	}

	// @desc 	| Line segment with ends at A and B points
	// @par[A]	| First segment point
	// @par[B]	| Second segment point
	// @par[index]	| Segment's index in data
	LineSegment(const Vector<Dimension>& A, const Vector<Dimension>& B);

	// @desc 	| return point with lowest coordinate:
	// 		| (a1, a2, ..., an) <= (b1, b2, ..., bn)
	// 		| <=> a1 <= b1 && a2 <= b2 && ... && an <= bn
	// @return	| point with lowest coordinate
	const Vector<Dimension>& First() const;

	// @desc	| return point with highest coordinate
	// 		| look at @desc of LineSegment::First
	// @return	| point with highest coordinate
	const Vector<Dimension>& Second() const;
private:
	Vector<Dimension> first_;
	Vector<Dimension> second_;
};

// @desc 		| calculate oriented area of vectors<2, int64_t>
// @par[1, 2, 3] 	| vectors<2, int64_t>
// @return 		| 2*(oriented area of given vectors)
// 			| where oriented area are positive when
// 			| points A, B, C are counterclock-wise and negative else
template <typename int64_t>
int64_t OrientedArea(const Vector<2>& A, 
		const Vector<2>& B, 
		const Vector<2>& C);

// @desc	| Check if two line segments are intersec in 2d
// @par[1, 2]	| line segment of int64_t type in 2d
// return	| int64_true if given line segments are intersec and False else
bool IsIntersec(const LineSegment<2>& first,
		const LineSegment<2>& second);

// @desc	| Check if two line segments are intersec in 1d
// @par[1, 2]	| line segment of int64_t type in 1d
// return	| int64_true if given line segments are intersec and False else
bool IsIntersec(const LineSegment<1>& first,
		const LineSegment<1>& second);

template <uint8_t Dimension>
Vector<Dimension>::Vector(const std::array<int64_t, Dimension>& data)
	: coordinates_(data)
{
}

template <uint8_t Dimension>
const int64_t& Vector<Dimension>::operator[](std::size_t index) const {
	return coordinates_.at(index);
}

template <uint8_t Dimension>
int64_t& Vector<Dimension>::operator[](std::size_t index) {
	return coordinates_.at(index);
}

template <uint8_t Dimension>
Vector<Dimension>& Vector<Dimension>::operator+=(const Vector<Dimension>& v) {
	for (int i = 0; i < Dimension; i++) {
		coordinates_[i] += v.coordinates_[i];
	}

	return *this;
}

template <uint8_t Dimension>
Vector<Dimension>& Vector<Dimension>::operator-=(const Vector<Dimension>& v) {
	for (int i = 0; i < Dimension; i++) {
		coordinates_[i] -= v.coordinates_[i];
	}

	return *this;
}

template <uint8_t Dimension>
Vector<Dimension>& Vector<Dimension>::operator*=(int64_t sc) {
	for (int i = 0; i < Dimension; i++) {
		coordinates_[i] *= sc;
	}

	return *this;
}

template <uint8_t Dimension>
Vector<Dimension>& Vector<Dimension>::operator/=(int64_t sc) {
	if (sc == 0) {
		return std::invalid_argument("Division by zero");
	}

	for (int i = 0; i < Dimension; i++) {
		coordinates_[i] /= sc;
	}

	return *this;
}

template <uint8_t Dimension>
int64_t Vector<Dimension>::LenSquare() const {
	int64_t result = 0;

	for (int i = 0; i < Dimension; i++) {
		result += coordinates_[i] * coordinates_[i];
	}

	return result;
}

template <uint8_t Dimension>
Vector<Dimension> operator+(const Vector<Dimension>& v1,
		const Vector<Dimension>& v2)
{
	Vector<Dimension> result = v1;
	result += v2;
	return result;
}

template <uint8_t Dimension>
Vector<Dimension> operator-(const Vector<Dimension>& v1,
		const Vector<Dimension>& v2)
{
	Vector<Dimension> result = v1;
	result -= v2;
	return result;
}

template <uint8_t Dimension>
Vector<Dimension> operator*(const Vector<Dimension>& v1,
		int64_t sc)
{
	Vector<Dimension> result = v1;
	result *= sc;
	return result;
}

template <uint8_t Dimension>
Vector<Dimension> operator/(const Vector<Dimension>& v1,
		int64_t sc)
{
	Vector<Dimension> result = v1;
	result /= sc;
	return result;
}

template <uint8_t Dimension>
Vector<Dimension> operator*(int64_t sc,
		const Vector<Dimension>& v1)
{
	Vector<Dimension> result = v1;
	result *= sc;
	return result;
}

template <uint8_t Dimension>
Vector<Dimension> operator/(int64_t sc,
		const Vector<Dimension>& v1)
{
	Vector<Dimension> result = v1;
	result /= sc;
	return result;
}

template <uint8_t Dimension>
LineSegment<Dimension>::LineSegment(const Vector<Dimension>& A, 
		const Vector<Dimension>& B)
	: first_(A),
	second_(B)
{
	for (int i = 0; i < Dimension; i++) {
		if (first_[i] > second_[i]) {
			std::swap(first_, second_);
			break;
		} else if (first_[i] < second_[i]) {
			break;
		}
	}
}

template <uint8_t Dimension>
const Vector<Dimension>& LineSegment<Dimension>::First() const {
	return first_;
}

template <uint8_t Dimension>
const Vector<Dimension>& LineSegment<Dimension>::Second() const {
	return second_;
}

// @desc		| calculate oriented area of triangle,
// 			| consisting of par[1], par[2], par[3]
// @par[1, 2, 3]	| 2d vectors
// @return		| 2 * oriented area of (par[1], par[2], par[3])
// 			| >0 if (par[1], par[2], par[3]) are counterclock-wise
// 			| <0 else
int64_t OrientedArea(const Vector<2>& a,
		const Vector<2>& b,
		const Vector<2>& c)
{
	return a[0] * (b[1] - c[1]) + b[0] * (c[1] - a[1]) + c[0] * (a[1] - b[1]);
}

bool IsIntersec(const LineSegment<2>& first,
		const LineSegment<2>& second)
{
	LineSegment<1> first_x(Vector<1>({first.First()[0]}),
			Vector<1>({first.Second()[0]}));
	LineSegment<1> first_y(Vector<1>({first.First()[1]}),
			Vector<1>({first.Second()[1]}));
	LineSegment<1> second_x(Vector<1>({second.First()[0]}),
			Vector<1>({second.Second()[0]}));
	LineSegment<1> second_y(Vector<1>({second.First()[1]}),
			Vector<1>({second.Second()[1]}));

	return IsIntersec(first_x, second_x) &&
		IsIntersec(first_y, second_y) &&
		OrientedArea(first.First(), first.Second(), second.First()) *
				OrientedArea(first.First(), first.Second(), second.Second()) <= 0 &&
		OrientedArea(second.First(), second.Second(), first.First()) *
				OrientedArea(second.First(), second.Second(), first.Second()) <= 0;
}

bool IsIntersec(const LineSegment<1>& first,
		const LineSegment<1>& second)
{
	Vector<1> max_1 = first.First();
	if (max_1[0] <= second.First()[0]) {
		max_1 = second.First();
	}

	Vector<1> min_1 = first.Second();
	if (min_1[0] >= second.Second()[0]) {
		min_1 = second.Second();
	}

	return max_1[0] <= min_1[0];
}

}
}

namespace Algorithms {
namespace Solution {

using namespace Algorithms::Geometry;

template <typename Iter>
int Solveint64_task(Iter& istr) {
	Vector<2> s_start;
	Vector<2> s_end;

	istr >> s_start[0] >> s_start[1] >> s_end[0] >> s_end[1];

	LineSegment<2> Line1(s_start, s_end);

	int num_of_rivers = 0;
	int intersect_amount = 0;

	istr >> num_of_rivers;

	for (int i = 0; i < num_of_rivers; i++) {
		Vector<2> from;
		Vector<2> to;

		istr >> from[0] >> from[1] >> to[0] >> to[1];

		if (IsIntersec(Line1, LineSegment<2>(from, to))) {
			++intersect_amount;
		}
	}

	return intersect_amount;
}

}
}

int main() {
	std::cout << Algorithms::Solution::Solveint64_task(std::cin) << std::endl;

	return 0;
}
