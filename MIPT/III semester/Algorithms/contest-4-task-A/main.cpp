#include <cassert>
#include <complex>
#include <iostream>
#include <numeric>
#include <vector>
#include "wav.h"

namespace FFT {
/* implementation dependency: wrong answer for ifft */
template <bool Inverse>
std::vector<std::complex<double>> native_fft(
                const std::vector<std::complex<double>>& vec)
{
        std::vector<std::complex<double>> result(vec.size());
        double constant = -2 * M_PI;

        if constexpr (Inverse) {
                constant = -constant;
        }

        for (size_t k = 0; k < result.size(); k++) {
                result[k] = { 0, 0 };

                for (size_t i = 0; i < result.size(); i++) {
                        std::complex<double> tmp = { constant * i * k / result.size(), 0 };

                        result[k] += vec[i] * std::exp(tmp * std::complex<double>(0, 1));
                }
        }

        return result;
}

/* implementation dependency: wrong answer for ifft */
template <bool Inverse>
std::vector<std::complex<double>> fft_spec(const std::vector<std::complex<double>>& vec) {
        if (vec.size() < 10) {
                return native_fft<Inverse>(vec);
        } else if (vec.size() % 2 != 0) {
                throw std::invalid_argument("size of vector must be a power of 2");
        }
        
        std::vector<std::complex<double>> factor(vec.size() / 2);
        double constant = -2 * M_PI;

        if constexpr (Inverse) {
                constant = -constant;
        }

        for (size_t i = 0; i < factor.size(); i++) {
                factor[i] = std::exp(std::complex<double>(0, i * constant / vec.size()));
        }

        std::vector<std::complex<double>> first_part(vec.size() / 2);
        std::vector<std::complex<double>> second_part(vec.size() / 2);

        for (size_t i = 0; i < first_part.size(); i++) {
                first_part[i] = vec[i * 2];
                second_part[i] = vec[i * 2 + 1];
        }

        auto even = fft_spec<Inverse>(first_part);
        auto odd = fft_spec<Inverse>(second_part);
        std::vector<std::complex<double>> result(vec.size());

        for (size_t i = 0; i < even.size(); i++) {
                result[i] = (even[i] + factor[i] * odd[i]);
                result[i + even.size()] = (even[i] - factor[i] * odd[i]);
        }

        return result;
}

/* correct fft function */
template <bool Inverse>
auto fft(const std::vector<std::complex<double>>& vec) {
        auto result = fft_spec<Inverse>(vec);
        
        if constexpr (Inverse) {
                for (size_t i = 0; i < vec.size(); i++) {
                        result[i] *= std::complex<double>(1.0 / vec.size(), 0.0);
                }
        }

        return result;
}
}

void ProcessData(const std::string& input_filename, const std::string& output_filename, double percent) {
        if (percent < 0 || percent > 1) {
                throw std::invalid_argument("Expected percent as 0 <= x <= 1");
        }
        
        WAV::WavFile file(input_filename.c_str());
        std::vector<std::complex<double>> complex_data(file.GetData().size());

        for (size_t i = 0; i < complex_data.size(); i++) {
                complex_data[i] = { file.GetData()[i], 0 };
        }

        size_t len = 0;

        while ((1ll << len) < complex_data.size()) {
                ++len;
        }

        for (size_t i = complex_data.size(); i < (1ll << len); i++) {
                complex_data.push_back({0, 0});
        }

        auto fft_data = FFT::fft<false>(complex_data);
        
        for (size_t i = fft_data.size() * (1 - percent); i < fft_data.size(); i++) {
                fft_data[i] = { 255, 0 };
        }

        auto ifft_data = FFT::fft<true>(fft_data);

        std::vector<char> new_data(ifft_data.size());

        for (size_t i = 0; i < new_data.size(); i++) {
                new_data[i] = (char) ifft_data[i].real();
        }

        new_data.resize(file.GetData().size());
        file.SetData(new_data);
        file.WriteToFile(output_filename.c_str());
}

int main(int argc, char* argv[]) {
        ProcessData("speech.wav", "result_1_0.wav", 1.0);
        
        return 0;
}
