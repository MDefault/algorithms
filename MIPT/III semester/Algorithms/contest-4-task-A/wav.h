#include <cmath>
#include <cstdio>
#include <fstream>
#include <iostream>

namespace WAV {
struct WAVHEADER
{
        char chunkId[4];
        unsigned int chunkSize;
        char format[4];
        char subchunk1Id[4];
        unsigned int subchunk1Size;
        unsigned short audioFormat;
        unsigned short numChannels;
        unsigned int sampleRate;
        unsigned int byteRate;
        unsigned short blockAlign;
        unsigned short bitsPerSample;
        char subchunk2Id[4];
        unsigned int subchunk2Size;
};

class WavFile {
public:
        WavFile(const std::string& filename);

        const WAVHEADER& GetHeader() const {
                return header_;
        }

        void SetData(const std::vector<char>& d) {
                if (d.size() != data_.size()) {
                        throw std::invalid_argument("Expected a data of the same size");
                }

                data_ = d;
        }

        const std::vector<char>& GetData() const {
                return data_;
        }

        void WriteToFile(const std::string& filename);
private:
        void ReadFromFile(const std::string& filename);
private:
        WAVHEADER header_;
        std::vector<char> data_;
};

WavFile::WavFile(const std::string& filename) {
        ReadFromFile(filename);
}

void WavFile::ReadFromFile(const std::string& filename) {
        std::ifstream istr(filename.c_str(), std::ios_base::binary);

        if (!istr.is_open()) {
                throw std::runtime_error("Can't open file");
        }

        istr.read(reinterpret_cast<char*>(&header_), sizeof(header_));
        data_.resize(header_.subchunk2Size);
        istr.read(data_.data(), data_.size());
}

void WavFile::WriteToFile(const std::string& filename) {
        std::ofstream ostr(filename.c_str(), std::ios_base::binary);

        if (!ostr.is_open()) {
                throw std::runtime_error("Can't open file");
        }

        ostr.write(reinterpret_cast<char*>(&header_), sizeof(header_));
        ostr.write(data_.data(), data_.size());
}
}
