#include <iostream>
#include <string>
#include <vector>

namespace Algorithms {
	class Manacher {
	public:
		Manacher() = delete;
		
		Manacher(const std::string& str) 
			: _str(str)
		{
		}

		Manacher(const Manacher&) = delete;
		Manacher(Manacher&&) = delete;

		Manacher& operator=(const Manacher&) = delete;
		Manacher& operator=(Manacher&&) = delete;

		uint64_t CalculatePalindromNumber() const;
	private:
		std::string _str;
	private:
		uint64_t ManacherOddEven(bool even) const;
	};
}

namespace Algorithms {
	uint64_t Manacher::ManacherOddEven(bool even) const {
		int right = -1, left = 0;
		uint64_t ans = 0;
		std::vector<int> lens(_str.size());

		for (int i = 0; i < _str.size(); i++) {
			if (i < right) {
				lens[i] = std::min(lens[left + right - i + even], right - i + even);
			}
		
			while (i + lens[i] + 1 - even < _str.size() &&
					i >= lens[i] + 1 &&
					_str[i + lens[i] + 1 - even] == _str[i - lens[i] - 1]) 
			{
				++lens[i];
			}

			if (i + lens[i] > right + even) {
				right = i + lens[i] - even;
				left = i - lens[i];
			}

			ans += lens[i];
		}

		return ans;
	}

	uint64_t Manacher::CalculatePalindromNumber() const {
		return ManacherOddEven(true) + ManacherOddEven(false);
	}
}

int main() {
	std::string source_str;
	std::cin >> source_str;
	
	Algorithms::Manacher obj(source_str);

	std::cout << obj.CalculatePalindromNumber() << std::endl;

	return 0;
}